import React from 'react'
import { toastHeartsStyles } from '../../assets/styles/toast'
import Icon from 'react-native-vector-icons/FontAwesome'
import { View, Text } from 'react-native'


class ToastHearts extends React.Component{
  render() {
    return (
      <View style={ toastHeartsStyles.container }>
        <Icon style={ toastHeartsStyles.iconPlus } name='plus'></Icon>
        <Text style={ toastHeartsStyles.text }>{ this.props.heartsCount }</Text>
        <Icon style={ toastHeartsStyles.iconHeart } name='heart'></Icon>
      </View>
    )
  }
}

export default ToastHearts
