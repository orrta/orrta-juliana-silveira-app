import React from 'react'
import { Text, View, TouchableWithoutFeedback } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { headerMenuStyles } from '../../assets/styles/header'

class HeaderMenu extends React.Component {

  render() {
    return (
      <TouchableWithoutFeedback onPress={ this.props.onIconClick }>
        {
          this.props.iconText ? (
            <View style={ headerMenuStyles.container }>
              <Text style={ headerMenuStyles.text }>{ this.props.iconText }</Text>
              <Icon style={ headerMenuStyles.iconWithText } name={ this.props.iconName }></Icon>
            </View>
          ) : (
            <View style={ headerMenuStyles.container }>
              <Icon style={ headerMenuStyles.icon } name={ this.props.iconName }></Icon>
            </View>
          )
        }
      </TouchableWithoutFeedback>
    )
  }
}

export default HeaderMenu
