import React from 'react'
import { headerStyles } from '../../assets/styles/header'

import { View, Text, StyleSheet } from 'react-native'

class Header extends React.Component{
  render() {
    return (
      <View style={ headerStyles.container }>
        <View style={ headerStyles.header }>
          {
            this.props.leftTitle ? (
              <Text style={ headerStyles.text }>
                { this.props.leftTitle }
              </Text>
            ) : this.props.left && this.props.left()
          }
          {
            this.props.rightTitle ? (
              <Text style={ headerStyles.text }>
                { this.props.rightTitle }
              </Text>
            ) : this.props.right && this.props.right()
          }
        </View>
      </View>
    )
  }
}

export default Header
