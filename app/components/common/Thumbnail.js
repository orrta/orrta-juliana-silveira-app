import React from 'react'
import { CachedImage as Image } from 'react-native-cached-image'
import { thumbnailStyles } from '../../assets/styles/common'

const Thumbnail = props => {
  return (
  <Image style={[ thumbnailStyles.thumbnail, props.style ]}
         source={{uri: props.image || 'https://res.cloudinary.com/marbamedia/image/instagram_name/w_120,h_120,c_fill/julianasilveiraatriz.jpg'}}
  />
)}

export default Thumbnail
