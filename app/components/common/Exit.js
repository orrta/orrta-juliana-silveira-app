import React from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'
import { exitButtonStyles } from '../../assets/styles/common'
import Icon from 'react-native-vector-icons/Ionicons'

class Exit extends React.Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={ this.props.onExitPress }>
        <View style={ exitButtonStyles.container }>
          <Icon style={ exitButtonStyles.icon } name='md-close-circle' />
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export default Exit
