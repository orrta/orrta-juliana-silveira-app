import React from 'react'
import { View, TouchableWithoutFeedback, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { textInputStyles } from '../../assets/styles/common'


class OrrtaTextInput extends React.Component {

  render() {
    return (
      <View style={ [ textInputStyles.container, this.props.containerStyle ]  }>
        {
          <TextInput
            { ...this.props }
            style={ [ textInputStyles.input, this.props.inputStyle ] }
            onFocus={ this.props.onFocus }
            onBlur={ this.props.onBlur }
            placeholderTextColor={ this.props.placeholderTextColor || '#666' }
            underlineColorAndroid='transparent'
          />
        }
        {
          this.props.icon && (
            <TouchableWithoutFeedback onPress={ this.props.onPress || (() => {}) }>
              <View>
                <Icon name={ this.props.iconName || 'ios-arrow-dropright-circle-outline' } style={ [ textInputStyles.icon, this.props.iconStyle ]  } />
              </View>
            </TouchableWithoutFeedback>
          )
        }
      </View>
    )
  }
}

export default OrrtaTextInput
