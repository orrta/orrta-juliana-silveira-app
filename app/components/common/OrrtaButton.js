import React from 'react'
import { View, TouchableWithoutFeedback, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { buttonStyles } from '../../assets/styles/common'

const containerStyle = outline => outline ? buttonStyles.containerOutline : buttonStyles.container
const textStyle = outline => outline ? buttonStyles.textOutline : buttonStyles.text
const iconStyle = outline => outline ? buttonStyles.iconOutline : buttonStyles.icon

const OrrtaButton = props => (
  <TouchableWithoutFeedback onPress={ props.onPress || (() => {}) }>
    <View style={[ containerStyle(props.outline), props.containerStyle ]}>
      {
        props.text !== false && props.text !== undefined && props.text !== null && (
          <Text style={[ textStyle(props.outline), props.textStyle ]}>{ props.text }</Text>
        )
      }
      {
        props.iconName && (
          <Icon name={ props.iconName } style={[ iconStyle(props.outline), props.iconStyle ]} />
        )
      }
    </View>
  </TouchableWithoutFeedback>
)

export default OrrtaButton
