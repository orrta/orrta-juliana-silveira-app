import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { loaderStyles } from '../../assets/styles/common'
import layoutSettings from '../../common/settings/layoutSettings'

const Loader = props => (
  <View style={[ loaderStyles.loader, props.style ]}>
    <ActivityIndicator size="large" color={ props.color || layoutSettings.colors.TERTIARY } />
  </View>
)

export default Loader
