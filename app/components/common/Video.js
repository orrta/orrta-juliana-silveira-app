import React from 'react'

import { View, Dimensions } from 'react-native'

import Loader from './Loader'
import VideoPlayer from 'react-native-video-player'

class Video extends React.Component {

  state = { loading: true, videoLoading: false }

  async componentDidMount() {
    const response = await fetch(`https://player.vimeo.com/video/${this.props.vimeoId}/config`)
    const responseJson = await response.json()

    const width = Dimensions.get('window').width
    const height = Math.floor((width * responseJson.video.height) / responseJson.video.width)

    this.setState({
      loading: false,
      url: responseJson.request.files.hls.cdns[responseJson.request.files.hls.default_cdn].url,
      thumbnail: responseJson.video.thumbs['640'],
      duration: responseJson.video.duration,
      width,
      height
    })
  }

  setVideoLoading = params => {
    this.setState({ videoLoading: params.isBuffering })
  }

  render() {
    if (this.state.loading) {
      return <Loader color={ this.props.loadingColor } />
    }

    const width = this.props.width || this.state.width
    const height = this.props.height || this.state.height

    return (
      <View style={[{
        width,
        height,
        backgroundColor: '#000'
      }, this.props.style]}>

        {
          this.state.videoLoading && (
            <View style={{
              position: 'absolute',
              top: (height / 2) - 40,
              left: (width / 2) - 40,
              height: 80,
              width: 80,
              borderRadius: 40,
              backgroundColor: 'rgba(0, 0, 0, 0.3)',
              zIndex: 999
            }}>
              <Loader color='#fff' style={{
                marginBottom: -3,
                marginLeft: 3,
              }} />
            </View>
          )
        }

        <VideoPlayer
          ref={r => this.questionVideoPlayer = r}
          onBuffer={ this.setVideoLoading }
          ignoreSilentSwitch='ignore'
          video={{ uri: this.state.url }}
          videoWidth={ this.state.width }
          thumbnail={{ uri: this.state.thumbnail }}
          duration={ this.state.duration }
          autoplay={ false }
          disableFullscreen={ false }
          style={{
            width: width,
            height: height
          }}
          { ...this.props }
        />
      </View>
    )
  }
}

export default Video
