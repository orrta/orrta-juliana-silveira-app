import React from 'react'

import { Dimensions, Keyboard, Text, ScrollView, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Toast from 'react-native-easy-toast'

import { commentsPageStyles } from '../../assets/styles/comments/commentsPage'
import { multiContentStyles } from '../../assets/styles/timelinePost'
import { toastStyles } from '../../assets/styles/toast'
import layoutSettings from '../../common/settings/layoutSettings'

import ToastHearts from '../common/ToastHearts'
import OrrtaButton from '../common/OrrtaButton'
import Loader from '../common/Loader'
import CommentInput from './CommentInput'
import TimelineComment from './TimelineComment'
import Header from '../common/Header'
import HeaderMenu from '../common/HeaderMenu'


class PageCommentsView extends React.Component {

  state = { marginButton: layoutSettings.bottomByDevice, pageHeight: Dimensions.get('window').height }

  fixedInputHeight = 70

  renderExitButton = () => <HeaderMenu iconName='md-close' onIconClick={ () => this.props.navigation.goBack() } />

  _keyboardWillShow = (e) => {
    this.setState({
      marginButton: e.endCoordinates.height + layoutSettings.bottomByDevice,
      pageHeight: Dimensions.get('window').height - e.endCoordinates.height
    })
    this.scrollToLastComment()
  }

  _keyboardWillHide = () => {
    this.setState({
      marginButton: layoutSettings.bottomByDevice,
      pageHeight: Dimensions.get('window').height
    })
    this.scrollToLastComment()
  }

  scrollToLastComment = () => {
    setTimeout(() => {
      this.scroll && this.scroll.scrollToEnd()
    }, 100);
  }

  keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardWillShow)
  keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this._keyboardWillHide)

  saveComment = (comment) => {
    this.props.saveComment(comment, () => {
      this.refs.toast && this.refs.toast.show(<ToastHearts heartsCount={ 5 } />, 3000)
    })
  }

  render() {
    return (
      <View style={ commentsPageStyles.container }>
        <Toast
          ref="toast"
          style={ toastStyles.container }
          position='top'
          positionValue={ 70 }
          fadeInDuration={ 750 }
          fadeOutDuration={ 1000 }/>
        <Header leftTitle='COMENTÁRIOS' right={ this.renderExitButton } />
        <View style={ commentsPageStyles.body }>

          {
            (!this.props.post.loadingComments && this.props.post.comments.length === 0) ? (
              <View style={ commentsPageStyles.noContentBody }>
                <View style={ commentsPageStyles.noContentContainer }>
                  <Text style={ commentsPageStyles.noContentText }>
                    SEJA @
                  </Text>
                  <Text style={ commentsPageStyles.noContentText }>
                    PRIMEIR@ A
                  </Text>
                  <Text style={ commentsPageStyles.noContentText }>
                    ENVIAR
                  </Text>
                  <Text style={ commentsPageStyles.noContentText }>
                    COMENTÁRIOS
                  </Text>
                </View>
              </View>
            ) : (
              <View style={[
                commentsPageStyles.container,
                { marginBottom: this.state.marginButton + this.fixedInputHeight }
              ]}>
                <ScrollView ref={ (scroll) => { this.scroll = scroll } }>
                  {
                    !this.props.post.loadingComments &&
                    this.props.post.currentPost.comments_count > this.props.post.comments.length && (
                      <OrrtaButton
                        text='VER MAIS COMENTÁRIOS'
                        containerStyle={ multiContentStyles.viewMoreCommentsButton }
                        onPress={ this.props.viewMoreComments }/>
                    )
                  }

                  { this.props.post.loadingComments
                    && <Loader /> }

                  {
                    (!this.props.post.loadingComments || this.props.post.comments.length > 0)
                    && this.props.renderComments(
                      this.props.post.comments,
                      this.props.auth.loggedUser.id,
                      this.props.deleteComment
                    )
                  }
                </ScrollView>
              </View>
            )
          }

          <View style={[
            multiContentStyles.commentInput,
            commentsPageStyles.fixedCommentInput,
            { bottom: this.state.marginButton, height: this.fixedInputHeight }
          ]}>
            <CommentInput
              onComment={ this.saveComment }
              anonimousUser={ this.props.auth.anonimous }
              navigation={ this.props.navigation }
              openModal={ this.props.actions.openModal } />
          </View>

        </View>
      </View>
    )
  }
}

export default PageCommentsView
