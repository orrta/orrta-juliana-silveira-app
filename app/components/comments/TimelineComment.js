import React from 'react'
import { Text, View, TouchableWithoutFeedback } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import Loader from '../common/Loader'
import layoutSettings from '../../common/settings/layoutSettings'
import Thumbnail from '../common/Thumbnail'
import { timelineCommentStyles } from '../../assets/styles/timelineActions'
import { dateStringToOrrtaLabel } from '../../common/helpers/date'

const renderDeleteButton = (comment, onDelete) => {
  if (onDelete) {
    return (
      <TouchableWithoutFeedback onPress={ () => onDelete(comment.id) }>
        <View style={ timelineCommentStyles.deleteContainer }>
          <Icon name='close' style={ timelineCommentStyles.deleteIcon } />
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const TimelineComment = props => (
  props.inverted ? (
    props.comment.deleting ?
      <Loader/> :
      (
        <View style={ timelineCommentStyles.container }>
          <View style={ timelineCommentStyles.commentsItem }>
            <View style={ timelineCommentStyles.commentsText }>
              <View style={ timelineCommentStyles.commentsUserNameContainer }>
                { renderDeleteButton(props.comment, props.onDelete) }
                <Text style={ timelineCommentStyles.commentsUserNameInverted }>
                  { props.comment.userName || 'Juliana Silveira' }
                </Text>
              </View>
              <Text style={ timelineCommentStyles.textInverted }>{ props.comment.content }</Text>
            </View>
            <Thumbnail image={ props.comment.imageUrl } />
          </View>
          <Text style={ timelineCommentStyles.dateTextLeft }>{ dateStringToOrrtaLabel(props.comment.date_time, false) }</Text>
        </View>
      )
  ) : (
    <View style={ timelineCommentStyles.container }>
      <View style={ timelineCommentStyles.commentsItem }>
        <Thumbnail image={ props.comment.imageUrl } />
        <View style={ timelineCommentStyles.commentsText }>
          <Text style={ timelineCommentStyles.commentsUserName }>{ props.comment.userName || 'Juliana Silveira' }</Text>
          <Text style={ timelineCommentStyles.text }>{ props.comment.content }</Text>
        </View>
      </View>
      <Text style={ timelineCommentStyles.dateTextRight }>{ dateStringToOrrtaLabel(props.comment.date_time, false) }</Text>
    </View>
  )
)

export default TimelineComment
