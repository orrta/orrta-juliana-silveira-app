import React from 'react'
import { Keyboard, View, TouchableWithoutFeedback, Text } from 'react-native'
import OrrtaTextInput from '../common/OrrtaTextInput'
import { timelineCommentInputStyles } from '../../assets/styles/timelineActions'
import layoutSettings from '../../common/settings/layoutSettings'
import Icon from 'react-native-vector-icons/Ionicons'
import { textInputStyles } from '../../assets/styles/common'

class CommentInput extends React.Component {

  state = { comment: '' }

  sendComment = () => {
    if(this.state.comment.trim()) {
      this.props.onComment(this.state.comment)
      this.setState({ comment: '' })
      Keyboard.dismiss()
    }
  }

  unauthorized = () => {
    this.props.openModal({
      title: `Ops...`,
      subtitle: 'Você não está logado no app. Precisamos te conhecer para podermos liberar algumas funcionalidades, como envio de comentários. Para liberar essas funcionalidades, basta fazer login!',
      buttons: [{
        text: 'Fazer Login',
        onPress: () => this.props.navigation.navigate('Login')
      }, {
        text: 'Continuar sem login',
        onPress: () => { },
        outline: true
      }]
    })
  }

  handleCommentInput = comment => { this.setState({ comment }) }

  render() {
    if (this.props.anonimousUser) {
      return (
        <TouchableWithoutFeedback onPress={ this.unauthorized }>
          <View style={ [ textInputStyles.container, timelineCommentInputStyles.container ]  }>
            <Text style={[ textInputStyles.input, timelineCommentInputStyles.input ]}>Converse aqui sobre a matéria...</Text>
            <View>
              <Icon name='ios-arrow-dropright-circle-outline' style={ [ textInputStyles.icon, timelineCommentInputStyles.icon ]  } />
            </View>
          </View>
        </TouchableWithoutFeedback>
      )
    }

    return (
      <OrrtaTextInput
        returnKeyType='send'
        icon={ true }
        onFocus={ this.props.onFocus }
        onBlur={ this.props.onBlur }
        placeholderTextColor={ layoutSettings.colors.TERTIARY }
        inputStyle={ timelineCommentInputStyles.input }
        containerStyle={ timelineCommentInputStyles.container }
        iconStyle={ timelineCommentInputStyles.icon }
        onPress={ this.sendComment }
        placeholder='Converse aqui sobre a matéria...'
        onChangeText={ this.handleCommentInput }
        value={ this.state.comment }
        onSubmitEditing={ this.sendComment }
        underlineColorAndroid='transparent'
      />
    )
  }
}

export default CommentInput
