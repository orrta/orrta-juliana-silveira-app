import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { Text, View, TouchableWithoutFeedback } from 'react-native'
import { multiContentStyles } from '../../assets/styles/timelinePost'
import layoutSettings from '../../common/settings/layoutSettings'

import OrrtaButton from '../common/OrrtaButton'
import Loader from '../common/Loader'
import CommentInput from './CommentInput'
import TimelineComment from './TimelineComment'
import InlineCommentsView from './InlineCommentsView'
import PageCommentsView from './PageCommentsView'


const renderComments = (comments, loggedUserId, onDelete) => {
  if (comments.length === 0) {

    let comment = {
      content: 'Ainda não temos nenhum comentário por aqui. Seja o primeiro! ❤'
    }

    return (<TimelineComment comment={ comment }/>)
  } else {

    const commentsToRender = comments.slice().reverse()

    return commentsToRender.map((comment, index) => {
      if (comment.saving) {
        const loadingComment = { content: 'Estamos publicando... 😍' }
        return (<TimelineComment key={ index } comment={ loadingComment }/>)
      } else if (comment.errorToSave) {
        const loadingComment = { content: 'Não foi possível salvar seu comentário, confira se sua internet está conectada e tente novamente! ☺' }
        return (<TimelineComment key={ index } comment={ loadingComment }/>)
      }
      const inverted = loggedUserId == comment.userId
      return (<TimelineComment inverted={ inverted } key={ index } comment={ comment } onDelete={ onDelete }/>)
    })
  }
}


class Comments extends React.Component {

  state = { commentsPage: 0 }

  viewMoreComments = () => {
    page = this.state.commentsPage + 1
    this.props.actions.loadLastComments(this.props.post.currentPost.id, page)
    this.setState({ commentsPage: page })
  }

  saveComment = (commentContent, callback) => {
    const comment = {
      content: commentContent,
      userId: this.props.auth.loggedUser.id,
      postId: this.props.post.currentPost.id,
      imageUrl: this.props.auth.loggedUser.imageUrl,
      userName: this.props.auth.loggedUser.name,
      section: this.props.post.currentPost.section
    }

    if (this.props.onSaveComment) {
      const _callback = callback

      callback = () => {
        this.props.onSaveComment()
        _callback && _callback()
      }
    }

    this.props.actions.saveComment(comment, callback)
  }

  deleteComment = commentId => {
    this.props.actions.openModal({
      title: 'Apagar Comentário',
      subtitle: 'Deseja realmente apagar seu comentário?',
      buttons: [
        { text: 'Não' },
        {
          text: 'Sim',
          outline: true,
          onPress: () => this.props.actions.deleteComment({
            id: commentId,
            userId: this.props.auth.loggedUser.id,
            postId: this.props.post.currentPost.id,
            section: this.props.post.currentPost.section
          })
        },

      ]
    })
  }

  render() {
    if (this.props.inline) {
      return (
        <InlineCommentsView
          actions={ this.props.actions }
          post={ this.props.post }
          auth={ this.props.auth }
          deleteComment={ this.deleteComment }
          navigation={ this.props.navigation }
          viewMoreComments={ this.viewMoreComments }
          renderComments={ renderComments }
          saveComment={ this.saveComment }/>
      )
    } else {
      return (
        <PageCommentsView
          actions={ this.props.actions }
          post={ this.props.post }
          auth={ this.props.auth }
          navigation={ this.props.navigation }
          deleteComment={ this.deleteComment }
          viewMoreComments={ this.viewMoreComments }
          renderComments={ renderComments }
          saveComment={ this.saveComment }/>
      )
    }
  }
}

const App = ({ post, actions, auth, navigation, onSaveComment, inline }) => (
  <Comments
    inline={ inline }
    auth={ auth }
    post={ post }
    actions={ actions }
    navigation={ navigation }
    onSaveComment={ onSaveComment } />
)

App.propTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  onSaveComment: PropTypes.func
}

const mapStateToProps = state => ({
  auth: state.auth,
  post: state.post
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
