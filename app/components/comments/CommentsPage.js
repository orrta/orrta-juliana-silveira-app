import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { View } from 'react-native'

import { commentsPageStyles } from '../../assets/styles/comments/commentsPage'
import Comments from './Comments'

class CommentsPage extends React.Component {

  render() {
    return (
      <View style={ commentsPageStyles.container }>
        <Comments navigation={ this.props.navigation } />
      </View>
    )
  }
}

const App = ({ actions, navigation }) => (
  <CommentsPage actions={ actions } navigation={ navigation } />
)

App.propTypes = {
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({ })

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
