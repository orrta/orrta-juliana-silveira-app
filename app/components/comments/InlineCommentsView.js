import React from 'react'

import { View } from 'react-native'
import { multiContentStyles } from '../../assets/styles/timelinePost'
import layoutSettings from '../../common/settings/layoutSettings'

import OrrtaButton from '../common/OrrtaButton'
import Loader from '../common/Loader'
import CommentInput from './CommentInput'
import TimelineComment from './TimelineComment'


const InlineCommentsView = (props) => (
  <View>
    {
      !props.post.loadingComments &&
      props.post.currentPost.comments_count > props.post.comments.length && (
        <OrrtaButton
          text='VER MAIS COMENTÁRIOS'
          containerStyle={ multiContentStyles.viewMoreCommentsButton }
          onPress={ props.viewMoreComments }/>
      )
    }

    { props.post.loadingComments && <Loader /> }

    {
      props.renderComments(
        props.post.comments,
        props.auth.loggedUser.id,
        props.deleteComment
      )
    }

    <View style={ multiContentStyles.commentInput }>
      <CommentInput
        onComment={ props.saveComment }
        anonimousUser={ props.auth.anonimous }
        navigation={ props.navigation }
        openModal={ props.actions.openModal } />
    </View>
  </View>
)

export default InlineCommentsView
