import React from 'react'

import { View, Text, Image } from 'react-native'
import { errorPageStyles } from '../../assets/styles/errorPage/errorPage'
import OrrtaButton from '../common/OrrtaButton'


class ErrorPage extends React.Component {

  goToMainPage = () => this.props.navigation.navigate('MainTab')

  render() {
    return (
      <View style={ errorPageStyles.container }>

        <Image source={ require('../../assets/images/login-background.jpg') } style={ errorPageStyles.image } />

        <View style={ errorPageStyles.textContainer }>
          <Text style={ errorPageStyles.text }>Estamos fazendo uma manutenção para lhe entregar uma experiência ainda melhor.</Text>
          <Text style={ errorPageStyles.text }>Em pouco tempo você poderá usar o aplicativo novamente.</Text>
          <Text style={ errorPageStyles.text }>Muito obrigado pela compreensão!</Text>
        </View>

        <OrrtaButton
          text='VOLTAR PARA A TELA DE LOGIN'
          containerStyle={ errorPageStyles.button }
          onPress={ () => { this.props.navigation.navigate('Login') } }/>

      </View>
    )
  }
}

export default ErrorPage
