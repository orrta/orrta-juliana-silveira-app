import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { Modal, View, Text, Image } from 'react-native'
import { modalStyles } from '../../assets/styles/modal/modal'
import OrrtaButton from '../common/OrrtaButton'

const imagesSource = {
  sendingHeartsImage: require('../../assets/images/tutorials/sending-hearts.gif'),
  discountCouponImage: require('../../assets/images/tutorials/discount-coupon.gif'),
  profileButtonImage: require('../../assets/images/tutorials/profile-button.gif')
}

const images = {
  'sending-hearts': {
    image: imagesSource.sendingHeartsImage,
    styles: { height: 252, width: 200 }
  },
  'discount-coupon': {
    image: imagesSource.discountCouponImage,
    styles: { height: 165, width: 250 }
  },
  'profile-button': {
    image: imagesSource.profileButtonImage,
    styles: { height: 120, width: 250 }
  }
}


class OrrtaModal extends React.Component {

  render() {
    return (
      <Modal
        visible={ this.props.modal.open }
        presentationStyle="overFullScreen"
        animationType="fade"
        transparent={ true }
        onRequestClose={ () => {} }
        >
        <View style={ modalStyles.container }>
          <View style={ modalStyles.cardContainer }>
            {
              this.props.modal.title &&
              <Text style={ modalStyles.title }>{ this.props.modal.title }</Text>
            }
            {
              this.props.modal.image &&
              <Image
                style={[ modalStyles.image, images[this.props.modal.image].styles ]}
                source={ images[this.props.modal.image].image } />
            }
            {
              (this.props.modal.subtitle || this.props.modal.body) &&
              <Text style={ modalStyles.subtitle }>{ this.props.modal.subtitle || this.props.modal.body }</Text>
            }
            {
              this.props.modal.buttons.length == 0 ? (
                <OrrtaButton onPress={ this.props.actions.closeModal } text="OK"/>
              ) : (
                this.props.modal.buttons.map((button, index) => (
                  <OrrtaButton outline={ button.outline } key={ index } onPress={ () => {
                    this.props.actions.closeModal()
                    button.onPress && button.onPress()
                  } } text={ button.text }/>
                ))
              )
            }
          </View>
        </View>
      </Modal>
    )
  }
}


const OrrtaModalApp = ({ modal, actions }) => (
  <OrrtaModal modal={ modal } actions={ actions } />
)

OrrtaModalApp.propTypes = { modal: PropTypes.object.isRequired }

const mapStateToProps = state => ({ modal: state.modal })
const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(Actions, dispatch) })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrrtaModalApp)
