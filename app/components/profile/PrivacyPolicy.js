import React from 'react'

import { Text, View, ScrollView } from 'react-native'
import Header from '../common/Header'
import HeaderMenu from '../common/HeaderMenu'
import { profileStyles } from '../../assets/styles/profile/profile'

class PrivacyPolicy extends React.Component {

  renderExitButton = () => <HeaderMenu iconName='md-close' onIconClick={ () => this.props.navigation.goBack() } />

  render() {
    return (
      <View style={ profileStyles.termsContainer } >
        <Header leftTitle='Políticas de Privacidade' right={ this.renderExitButton } />
        <ScrollView style={ profileStyles.defaultContainer } >
          <Text style={ profileStyles.defaultText } >
            Política de Privacidade
          </Text>
          <Text style={ profileStyles.defaultText } >
            Em vigor a partir de 20 de setembro de 2018.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Introdução
          </Text>
          <Text style={ profileStyles.defaultText } >
            Esta política de privacidade divulga nossas práticas de privacidade, que possui e opera vários sites na World Wide Web, incluindo, sem limitação, o  (coletivamente, nossos “Websites”). Seu acesso e uso dos nossos Websites e de todos os serviços fornecidos pelos Websites do  (coletivamente, nossos “Serviços”) estão sujeitos aos nossos Termos de Uso e a todas as leis aplicáveis.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Esta política de privacidade descreve como coletamos, usa e compartilha informações sobre você obtidas em conexão com o fornecimento de nossos serviços para você.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Valorizamos seus comentários sobre nossa declaração de privacidade e convidamos você a nos enviar um e-mail com perguntas, sugestões e reclamações em: contato@mobimedia.com.br.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Quais informações são coletadas
          </Text>
          <Text style={ profileStyles.defaultText } >
            Para melhorar a sua experiência e melhorar os Serviços, coletamos informações sobre você de duas fontes: (i) informações que você afirma de forma afirmativa; e (ii) informações coletadas automaticamente quando você visitar qualquer um dos nossos Sites ou usar qualquer um dos nossos Serviços. Terceiros também podem coletar suas informações relacionadas ao uso dos nossos Serviços.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Informações que você nos dá
          </Text>
          <Text style={ profileStyles.defaultText } >
            Ao se inscrever nos nossos Serviços, podemos solicitar que você forneça determinadas informações sobre você, como seu nome, endereço de e-mail, endereço de faturamento e nome da empresa ou afiliação. Você pode visualizar todas as informações coletadas durante o processo de registro visitando a página de registro. Você pode remover qualquer uma de suas informações pessoais a qualquer momento, acessando sua conta.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Depois de fornecer essas informações e configurar sua conta, podemos solicitar que você forneça informações adicionais sobre você de maneira opcional. Podemos solicitar certas informações financeiras, incluindo dados de contas de cartão de crédito ou outros dados de método de pagamento, para processar pagamentos de alguns de nossos serviços. Podemos coletar qualquer comunicação que tenha consigo por qualquer meio, incluindo e-mail e telefone.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Ao optar por fornecer qualquer informação para nós, você está dando permissão para usar e armazenar essas informações de maneira consistente com esta política de privacidade.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Informações que coletamos automaticamente
          </Text>
          <Text style={ profileStyles.defaultText } >
            Além disso, coletamos automaticamente informações sobre suas interações com os  Websites e Serviços. Por exemplo, podemos registrar informações do seu computador e navegador, como seu endereço IP, informações de perfil, dados agregados, tipo de navegador, atributos de software e hardware, páginas solicitadas e informações de cookies. Ao usar os nossos Serviços, você está dando permissão para usarmos e armazenarmos essas informações de maneira consistente com esta política de privacidade.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Cookies
          </Text>
          <Text style={ profileStyles.defaultText } >
            Você pode se recusar a aceitar cookies ativando a configuração no seu navegador, o que lhe permite recusar a configuração de cookies. No entanto, se você selecionar essa configuração, talvez não consiga acessar determinadas partes do site. A menos que você tenha ajustado a configuração do seu navegador para recusar cookies, nosso sistema emitirá cookies quando você acessar o nosso site.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Informações coletadas por terceiros
          </Text>
          <Text style={ profileStyles.defaultText } >
            Podemos usar serviços que coletam dados remotamente usando “pixel tags”, “web beacons”, “clear GIFs” ou tecnologias similares. Essas tecnologias podem reconhecer determinados tipos de informações em seu navegador da Web, verificar se você visualizou uma determinada página da Web ou mensagem de e-mail e determinar, entre outras coisas, a hora e a data em que você as visualizou e o endereço IP do computador que você as viu.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Os nossos serviços podem ser vinculados ou fornecidos através de sites de terceiros. Não é responsável pelas políticas de privacidade e / ou práticas em sites de terceiros. Ao vincular ou acessar esses sites, você deve revisar cuidadosamente a política de privacidade de tal terceiro para determinar como ele lida com informações coletadas separadamente de você.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Como a informação é usada
          </Text>
          <Text style={ profileStyles.defaultText } >
            Usamos as informações que coletamos de você para entregar os nossos Serviços a você e aprimorar continuamente a sua experiência. Em geral, usamos as informações coletadas para nos comunicar com você, atender suas solicitações, personalizar o conteúdo, aprimorar nossos produtos e serviços, proteger nossos direitos e respeitar as leis e os regulamentos.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Podemos usar suas informações pessoais para enviar informações promocionais e atualizações sobre seus produtos e serviços.
          </Text>
          <Text style={ profileStyles.defaultText } >
            A informação que nós coletamos podem ser usadas de forma agregada de várias maneiras para otimizar e melhorar os nossos Serviços. Embora essas informações possam ser baseadas em informações sobre você, elas não o identificarão pessoalmente. Podemos usar essas informações para as seguintes finalidades: gerenciamento, administração e segurança de sites, atividades promocionais, pesquisa e análise.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Com quem as informações são compartilhadas
          </Text>
          <Text style={ profileStyles.defaultText } >
            Informações Publicas
          </Text>
          <Text style={ profileStyles.defaultText } >
            Podemos compartilhar as informações que você divulgar voluntariamente, incluindo informações postadas por você. Quaisquer blogs, fóruns de discussão, salas de bate-papo ou outros fóruns semelhantes, sejam ou não esses fóruns de nossa propriedade. Uma vez que tais informações públicas podem ser acessadas pelo público e usadas por qualquer membro do público, tal uso por terceiros está além do nosso controle.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Informações Pessoais
          </Text>
          <Text style={ profileStyles.defaultText } >
            Podemos compartilhar suas informações pessoais que coletamos (i) com seus agentes, representantes, contratados e provedores de serviços para fornecer serviços de suporte ao  para operar os nossos Serviços, incluindo empresas que auxiliam no processamento de pagamentos, análise de negócios, processamento de dados, gerenciamento de contas e outros serviços, (ii) se exigido por lei ou para proteger nossos direitos legais ou suas afiliadas, e cada um de seus respectivos investidores, diretores, executivos, funcionários, agentes e fornecedores, (iii) para cumprir um procedimento judicial, ordem judicial ou processo legal, (iv) para proteger a proteção e segurança dos nossos Serviços ou para aplicar nossos Termos de Uso ou (v) para proteger contra fraudes ou para fins de gerenciamento de risco. No caso de  compartilharmos suas informações pessoais de acordo com a subseção (i) acima, ele exigirá que tais agentes, representantes, contratados e / ou provedores de serviços não usem suas informações para qualquer finalidade além de nos fornecer serviços específicos que solicitamos. Note que, se você se inscrever para o serviço de uma de nossas afiliadas, poderemos solicitar o seu consentimento para compartilhar com tais parceiros terceirizados certas informações pessoais. Embora não sejamos responsáveis pelos atos de tais parceiros, incentivamos todos os nossos parceiros a adotar políticas de privacidade que protejam contra a divulgação de informações pessoais a terceiros.
          </Text>
          <Text style={ profileStyles.defaultText } >
            As informações não pessoais e os dado agregados
          </Text>
          <Text style={ profileStyles.defaultText } >
            Podemos compartilhar com terceiros suas informações pessoais e dados agregados relacionados. Por exemplo, podemos compartilhar endereços IP para fornecer dados agregados anônimos a parceiros de negócios sobre o volume de uso nos nossos Serviços. Terceiros não poderão entrar em contato com você exclusivamente com base nesses dados.
          </Text>
          <Text style={ profileStyles.defaultText } >
            A alteração do controle
          </Text>
          <Text style={ profileStyles.defaultText } >
            Se vendermos todo ou parte de nossos negócios, ou fazermos uma venda ou transferência de ativos, ou é Caso contrário envolvido em uma fusão ou transferência de negócios, ou no caso improvável de falência, podemos transferir suas informações pessoais e não pessoais para um ou mais terceiros como parte de tal transação.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Como você pode controlar as informações que coletamos, usamos e compartilhamos
          </Text>
          <Text style={ profileStyles.defaultText } >
            Você tem a opção de divulgar suas informações pessoais ou outras informações sobre você para nós. Independentemente das suas escolhas em relação a comunicações promocionais e atualizações relacionadas ao conteúdo, podemos enviar mensagens administrativas, anúncios de serviço, termos e condições de sua conta ou outras comunicações semelhantes, sem oferecer a você a oportunidade de optar por não recebê-los. Você pode excluir sua conta  de acordo com nossos Termos de Uso. Observe que, se
          </Text>
          <Text style={ profileStyles.defaultText } >
            você cancelar sua conta, poderemos reter algumas informações sobre você, mas interromperá a publicação dessas informações nos Websites do nosso Serviço.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Como as informações são protegidas
          </Text>
          <Text style={ profileStyles.defaultText } >
            Suas contas podem ser protegidas por senhas criadas por membros, caso em que são tomadas precauções razoáveis para garantir que as informações da sua conta sejam mantidas em sigilo. Você é responsável por manter suas senhas confidenciais. Pedimos que você não compartilhe sua senha com ninguém. Usamos medidas razoáveis ​​para proteger suas informações armazenadas em nossos bancos de dados, e restringimos o acesso a essas informações para os funcionários que precisam acessar para executar
          </Text>
          <Text style={ profileStyles.defaultText } >
            suas funções, como nosso pessoal de atendimento ao cliente e equipe técnica. Qualquer pagamento transações serão criptografadas usando a tecnologia SSL. Apesar desses esforços, observe que não podemos garantir ou garantir a segurança de suas informações. Entrada ou uso não autorizado, falha de hardware ou software e outros fatores podem comprometer a segurança de suas informações a qualquer momento.
          </Text>
          <Text style={ profileStyles.defaultText } >
            Alterações a esta política de privacidade
          </Text>
          <Text style={ profileStyles.defaultText } >
            De tempos em tempos, podemos fazer alterações a esta política de privacidade. Para garantir que você esteja familiarizado com nossas políticas e práticas mais atualizadas, verifique a data de vigência indicada acima desta política de privacidade sempre que usar os nosso Serviços. Se fizermos alguma alteração, notificaremos você sobre essas alterações publicando a política de privacidade atualizada nos nossos sites. Também podemos enviar um e-mail alertando você sobre essas mudanças. Você estará sujeito a quaisquer alterações na política quando usar os nossos Serviços após essas alterações terem sido postadas. Se fizermos revisões materiais referentes a práticas nas quais representamos que obteremos seu consentimento para usar ou divulgar suas informações pessoais, obteremos seu consentimento antes de tomar qualquer ação inconsistente com essas representações.
          </Text>
        </ScrollView>
      </View>
    )
  }
}

export default PrivacyPolicy
