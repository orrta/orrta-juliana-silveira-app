import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import Icon from 'react-native-vector-icons/Entypo'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import { Text, View, ScrollView, TouchableWithoutFeedback, Linking } from 'react-native'

import Loader from '../common/Loader'
import Thumbnail from '../common/Thumbnail'
import InviteFriendsCard from '../timeline/card/InviteFriendsCard'
import AdvertisingCard from '../timeline/card/AdvertisingCard'
import OrrtaButton from '../common/OrrtaButton'
import OrrtaTextInput from '../common/OrrtaTextInput'
import layoutSettings from '../../common/settings/layoutSettings'
import appSettings from '../../common/settings/appSettings'
import { profileStyles } from '../../assets/styles/profile/profile'


class Profile extends React.Component {

  state = { sendingCoupon: false, code: '' }

  handleCodeInput = code => { this.setState({ ...this.state, code }) }

  sendDiscountCoupon = () => {

    if (!this.state.code) {
      this.props.actions.openModal({
        title: `Ops...`,
        subtitle: 'Código inválido!'
      })
      return
    }

    this.setState({ ...this.state, sendingCoupon: true })

    this.props.actions.sendDiscountCoupon({
      user_id: this.props.auth.loggedUser.id,
      code: this.state.code
    }, () => {
      this.setState({ sendingCoupon: false, code: '' })
      this.props.actions.openModal({
        title: 'Parabéns',
        subtitle: `Código enviado com sucesso. Você ganhou ${appSettings.SENDER_USER_HEARTS_COUNT} corações!`,
      })
    }, message => {
      this.setState({ sendingCoupon: false })
      this.props.actions.openModal({
        title: 'Ops...',
        subtitle: message,
      })
    })
  }

  goToLoginPage = () => this.props.navigation.navigate('Login')

  logout = () => {
    this.props.actions.openModal({
      title: '😢',
      subtitle: 'Deseja realmente sair?',
      buttons: [
        { text: 'Não' },
        { outline: true, text: 'Sim', onPress: () => this.props.actions.logout(this.props.auth.loggedUser.authType, this.goToLoginPage) }
      ],
    })
  }

  componentDidMount() {
    if (this.props.auth.loggedUser.newUser) {
      const tutorialShown = this.props.auth.loggedUser.tutorialsShown.find(t => t === 'discount-coupon')

      if (!tutorialShown) {
        setTimeout(() => {
          this.props.actions.openModal({
            title: 'Cupom de Desconto',
            image: 'discount-coupon',
            body: 'Inserindo um cupom de desconto você pode ganhar corações.',
            buttons: [{
              text: 'Entendido!',
              onPress: () => this.props.actions.setTutorialAsShown('discount-coupon')
            }]
          })
        }, 1000)
      }
    }
  }

  render() {

    if (this.props.auth.anonimous) {
      return (
        <View style={ profileStyles.card }>
          <View style={ profileStyles.titleContainer }>
            <Text style={ profileStyles.title }>APROVEITE MELHOR O APP</Text>
          </View>
          <View style={ profileStyles.contentContainer }>
            <Text style={ profileStyles.content }>
              Faça seu cadastro no aplicativo da atriz Juliana Silveira para ter uma experiência ainda mais divertida! Com o seu cadastro você irá poder usar o aplicativo como uma rede social, podendo comentar e curtir postagens, além de participar do quiz interativo da Ju!
            </Text>
            <OrrtaButton containerStyle={ profileStyles.cardButton } text='FAZER LOGIN' onPress={ () => this.props.navigation.navigate('Login') } />
            <OrrtaButton containerStyle={ profileStyles.exitButton } textStyle={ profileStyles.exitButtonText } text='POLÍTICA DE PRIVACIDADE' onPress={ () => this.props.navigation.navigate('ProfilePrivacyPolicy', { tabBarVisible: false }) } />
            <OrrtaButton containerStyle={ profileStyles.exitButton } textStyle={ profileStyles.exitButtonText } text='TERMOS E CONDIÇÕES' onPress={ () => this.props.navigation.navigate('ProfileTermsAndConditions', { tabBarVisible: false }) } />
          </View>
        </View>
      )
    }

    const user = this.props.auth.loggedUser

    return user.name ? (
      <ScrollView>

        <View style={ profileStyles.thumbnailContainer }>
          <View style={ profileStyles.thumbnail }>
            <Thumbnail style={ profileStyles.thumbnailImage } image={ user.imageUrl } />
          </View>
        </View>

        <View style={ profileStyles.userContainer }>
          <Text style={ profileStyles.userName }>{ user.name }</Text>

          {
            this.state.sendingCoupon ? <Loader /> : (
              <OrrtaTextInput
                icon={ true }
                onPress={ this.sendDiscountCoupon }
                placeholderTextColor={ layoutSettings.colors.TERTIARY }
                returnKeyType='send'
                placeholder='Digite aqui seu cupom'
                onChangeText={ this.handleCodeInput }
                onSubmitEditing={ this.sendDiscountCoupon }
                autoCapitalize="characters"
                value={ this.state.code }/>
            )
          }
        </View>

        <View style={ profileStyles.heartsInfoSection }>
          <View style={ profileStyles.heartsInfoCard }>
            <View style={ profileStyles.heartsInfoTitleContainer }>
              <Text style={ profileStyles.heartsInfoTitle }>VOCÊ POSSUI</Text>
            </View>
            <View style={ profileStyles.heartsInfoCardBody }>
              <Text style={ profileStyles.heartsInfoText }>{ user.available_hearts }</Text>
              <FontAwesomeIcon name='heart' style={ profileStyles.heartsInfoIcon } />
            </View>
          </View>
          <View style={ profileStyles.heartsInfoCard }>
            <View style={ profileStyles.heartsInfoTitleContainer }>
              <Text style={ profileStyles.heartsInfoTitle }>VOCÊ ENVIOU</Text>
            </View>
            <View style={ profileStyles.heartsInfoCardBody }>
              <Text style={ profileStyles.heartsInfoText }>{ user.sent_hearts }</Text>
              <FontAwesomeIcon name='heart' style={ profileStyles.heartsInfoIcon } />
            </View>
          </View>
        </View>

        <InviteFriendsCard navigation={ this.props.navigation } profile={ true } />

        <AdvertisingCard navigation={ this.props.navigation } profile={ true } />

        <View style={ profileStyles.card }>
          <View style={ profileStyles.titleContainer }>
            <Text style={ profileStyles.title }>MAIS OPÇÕES</Text>
          </View>
          <View style={ profileStyles.contentContainer }>
            <OrrtaButton containerStyle={ profileStyles.cardButton } text='POLÍTICA DE PRIVACIDADE' onPress={ () => this.props.navigation.navigate('ProfilePrivacyPolicy', { tabBarVisible: false }) } />
            <OrrtaButton containerStyle={ profileStyles.cardButton } text='TERMOS E CONDIÇÕES' onPress={ () => this.props.navigation.navigate('ProfileTermsAndConditions', { tabBarVisible: false }) } />
            <OrrtaButton containerStyle={ profileStyles.cardButton } text='ENTRAR EM CONTATO' onPress={ () => Linking.openURL('mailto://contato@mobimedia.com.br?subject=Sobre%20o%20App%20da%20Juliana%20Silveira') } />
            <OrrtaButton containerStyle={ profileStyles.exitButton } textStyle={ profileStyles.exitButtonText } text='SAIR DA CONTA' onPress={ this.logout } />
          </View>
        </View>

      </ScrollView>
    ) : (<Loader />)
  }
}

const App = ({ auth, actions, navigation }) => (
  <Profile auth={ auth } actions={ actions } navigation={ navigation } />
)

App.propTypes = {
  auth: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
