import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import Loader from '../common/Loader'
import apisSettings from '../../common/settings/apisSettings'
import { Platform, View, Text, TouchableWithoutFeedback, Dimensions, Share } from 'react-native'
import { subscribeStyles } from '../../assets/styles/subscribe/subscribe'
import { videoStyles } from '../../assets/styles/common'
import { CachedImage as Image } from 'react-native-cached-image'
import OrrtaButton from '../common/OrrtaButton'
import VideoPlayer from 'react-native-video-player'
import Exit from '../common/Exit'

import * as RNIap from 'react-native-iap'

class Subscribe extends React.Component {

  state = { loading: false }

  buy = async (subscriptionName) => {
    this.setState({ loading: true })

    try {
      await RNIap.prepare()
      const purchase = await RNIap.buySubscription(subscriptionName)

      let url = `${apisSettings.USER_ITERATION.URL}/subscription/`

      let response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify({ purchase, user_id: this.props.auth.loggedUser.id }),
        headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY }
      })

      if (response.status != 200) {
        this.props.actions.openModal({
          title: 'Ops...',
          subtitle: 'Falha ao registrar seu pagamento! Favor entrar em contato com suporte@marbamedia.com.br'
        })
      } else if (response.status == 200) {
        this.props.actions.openModal({
          title: 'UHUUUL!',
          subtitle: 'Assinatura realizada com sucesso!'
        })
        this.props.actions.setUserAsSubscriber()
        this.props.navigation.goBack()
      }
    } catch (e) {
      this.props.actions.openModal({
        title: 'Ops...',
        subtitle: 'Assinatura não realizada!'
      })
    }

    this.setState({ loading: false })
  }

  render() {

    if (this.state.loading) {
      return <Loader />
    }

    if (this.props.auth.anonimous || this.props.auth.loggedUser.subscriber) {
      return (
        <View style={ subscribeStyles.wellcomeContainer }>
          <Exit onExitPress={ () => { this.props.navigation.goBack() } } />
          <Image source={{ uri: this.props.appSettings.subscription.background_image }} style={ subscribeStyles.wellcomeImage } />

          <View style={ subscribeStyles.wellcomeContainer }>
            <View style={ subscribeStyles.wellcomeTextContainer }>
              <Text style={ subscribeStyles.wellcomeDescription }>
                VOCÊ JÁ É ASSINANTE! MUITO OBRIGADO! ESPERO QUE TENHA UMA BOA EXPERIÊNCIA!
              </Text>
            </View>
            <OrrtaButton
              text='VOLTAR PARA A TIMELINE'
              onPress={ () => { this.props.navigation.goBack() } }
              containerStyle={ subscribeStyles.yearly } />
          </View>

          <View style={ subscribeStyles.terms }>
            <TouchableWithoutFeedback onPress={ () => this.props.navigation.navigate(this.props.navigation.state.params.termsAndConditionsNav, { tabBarVisible: false }) }>
              <View>
                <Text style={ subscribeStyles.serviceTerms }>Termos do Serviço</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={ () => this.props.navigation.navigate(this.props.navigation.state.params.privacyPolicyNav, { tabBarVisible: false }) }>
              <View>
                <Text style={ subscribeStyles.privacyPolicy }>Política de Privacidade</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      )
    }

    return (
      <View style={ subscribeStyles.wellcomeContainer }>
        <Exit onExitPress={ () => { this.props.navigation.goBack() } } />
        <Image source={{ uri: this.props.appSettings.subscription.background_image }} style={ subscribeStyles.wellcomeImage } />

        <View style={ subscribeStyles.wellcomeContainer }>
          <View style={ subscribeStyles.subscriptionContainer }>
            <View style={ subscribeStyles.wellcomeTextContainer }>
              <Text style={ subscribeStyles.wellcomeStarName }>
                JULIANA SILVEIRA
              </Text>
              <Text style={ subscribeStyles.wellcomeBeSubscriber }>
                { this.props.appSettings.subscription.title }
              </Text>
              <Text style={ subscribeStyles.wellcomeDescription }>
                { this.props.appSettings.subscription.subtitle }
              </Text>
            </View>

            {
              this.props.appSettings.subscription.plans.map((plan, i) => (
                <OrrtaButton text={ plan.name } onPress={ () => { this.buy(plan.id) } } key={ i } />
              ))
            }
          </View>
        </View>

        <View style={ subscribeStyles.terms }>
          <TouchableWithoutFeedback onPress={ () => this.props.navigation.navigate(this.props.navigation.state.params.termsAndConditionsNav, { tabBarVisible: false }) }>
            <View>
              <Text style={ subscribeStyles.serviceTerms }>Termos do Serviço</Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={ () => this.props.navigation.navigate(this.props.navigation.state.params.privacyPolicyNav, { tabBarVisible: false }) }>
            <View>
              <Text style={ subscribeStyles.privacyPolicy }>Política de Privacidade</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const App = ({ actions, navigation, auth, appSettings }) => (
  <Subscribe actions={ actions } navigation={ navigation } auth={ auth } appSettings={ appSettings } />
)

App.propTypes = {
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  appSettings: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  appSettings: state.appSettings
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
