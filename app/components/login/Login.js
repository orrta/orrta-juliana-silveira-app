import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { BackHandler, View, Text } from 'react-native'
import { CachedImage as Image } from 'react-native-cached-image'
import { loginStyles } from '../../assets/styles/login/login'
import OrrtaButton from '../common/OrrtaButton'
import Loader from '../common/Loader'


class Login extends React.Component {

  goToMainPage = () => {
    this.backHandler.remove()
    this.props.navigation.navigate('MainTab')
  }

  goToErrorPage = () => {
    this.backHandler.remove()
    this.props.navigation.navigate('ErrorPage')
  }

  goToSubscribePage = () => {
    this.backHandler.remove()
      this.props.navigation.navigate('Subscribe', {
      tabBarVisible: false,
      termsAndConditionsNav: 'TermsAndConditions',
      privacyPolicyNav: 'PrivacyPolicy'
    })
  }

  firstLoginCallback = (user) => {
    this.props.actions.openModal({
      title: `Seja bem-vind@, ${user.name}!`,
      subtitle: `Estou muito feliz de ter você aqui! Espero que sua experiência seja ótima!`,
      buttons: [{
        text: 'Continuar',
        onPress: this.goToMainPage,
        outline: true
      }]
    })
  }

  loginCallback = (user) => {
    this.goToMainPage()
  }

  showError = error => {
    if (error && error.controlled) {
      this.goToErrorPage()
    } else {
      this.props.actions.openModal({
        title: `Ops...`,
        subtitle: error.orrtaMessage || 'Tivemos um imprevisto ao fazer o login, tente novamente em alguns minutos'
      })
    }
  }

  componentWillUnmount() {
    this.backHandler.remove()
  }

  componentDidMount = () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp()
      return true
    })

    this.props.actions.loadLoggedUser(this.firstLoginCallback, this.loginCallback, this.showError)
  }

  loginWithFacebook = () => {
    this.props.actions.loginWithFacebook(this.firstLoginCallback, this.loginCallback, this.showError)
  }

  loginWithGoogle = () => {
    this.props.actions.loginWithGoogle(this.firstLoginCallback, this.loginCallback, this.showError)
  }

  enterWithoutLogin = () => {
    this.props.actions.openModal({
      title: `Olá, seja bem vind@!`,
      subtitle: 'Você está entrando no app sem fazer login. Precisamos te conhecer para podermos liberar algumas funcionalidades, como envio de corações e comentários. Para liberar essas funcionalidades, basta fazer login!',
      buttons: [{
        text: 'Fazer Login'
      }, {
        text: 'Entrar sem login',
        outline: true,
        onPress: () => this.props.actions.enterWithoutLogin(this.goToMainPage)
      }]
    })
  }

  render() {
    return (
      <View style={ loginStyles.container }>
        <Image source={ require('../../assets/images/login-background.jpg') } style={ loginStyles.image } />

        {
          this.props.auth.loading ? (
            <View style={ loginStyles.loading }>
              <Loader/>
            </View>
          ) :
          (
            <View style={ loginStyles.container }>
              <View style={ loginStyles.textContainer }>
                <Text style={ loginStyles.title }>JU SILVEIRA</Text>
              </View>

              <OrrtaButton
                text='ENTRE COM O FACEBOOK'
                containerStyle={ loginStyles.loginFB }
                textStyle={ loginStyles.loginFBText }
                onPress={ this.loginWithFacebook }/>

              <OrrtaButton
                text='ENTRE COM O GOOGLE'
                containerStyle={ loginStyles.loginGoogle }
                textStyle={ loginStyles.loginFBText }
                onPress={ this.loginWithGoogle }/>

              <OrrtaButton
                text='PULAR'
                containerStyle={ loginStyles.enterWithoutLogin }
                textStyle={ loginStyles.enterWithoutLoginText }
                onPress={ this.enterWithoutLogin }/>
            </View>
          )
        }
      </View>
    )
  }
}

const App = ({ auth, actions, navigation }) => (
  <Login auth={ auth } actions={ actions } navigation={ navigation } />
)

App.propTypes = {
  auth: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
