import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import layoutSettings from '../../common/settings/layoutSettings'
import RankingThumbnail from './RankingThumbnail'
import Loader from '../common/Loader'
import Header from '../common/Header'
import HeaderMenu from '../common/HeaderMenu'
import { Text, View, ListView, RefreshControl } from 'react-native'
import { rankingStyles } from '../../assets/styles/ranking/ranking'
import Icon from 'react-native-vector-icons/FontAwesome'

import apisSettings from '../../common/settings/apisSettings'

class Ranking extends React.Component {

  currentPage = 0
  size = 20

  state = {
    loading: true,
    total: 0,
    items: [],
    dataSource: new ListView.DataSource({
      rowHasChanged: (r1, r2) => JSON.stringify(r1) !== JSON.stringify(r2),
    })
  }

  searchRanking = async () => {
    try {
      const items = [...this.state.items]
      const postId = this.props.navigation.state.params.postId
      const url = `${apisSettings.AGGREGATION.URL}/search?index=hearts-sending` +
                  `&query_name=ranking-default&post_id=${postId}` +
                  `&from=${this.currentPage * this.size}&size=${this.size}`

      response = await fetch(url, {
        headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY }
      })

      responseJson = await response.json()

      if (responseJson.es && responseJson.es.hits) {
        const newItems = responseJson.es.hits.hits.map(hit => {
          let result = hit._source
          result.id = hit._id
          return result
        })

        if (this.currentPage === 0) {
          items = [...newItems]
        } else {
          items = [...items, ...newItems]
        }

        this.setState({
          loading: false,
          items,
          total: responseJson.es.hits.total,
          error: false,
          dataSource: this.getUpdatedDataSource(items)
        })
        this.props.actions.updateRanking(items, responseJson.es.aggregations.sum.value, postId)
      } else {
        that.setState({
          loading: false,
          items: [],
          error: false,
          dataSource: this.getUpdatedDataSource([])
        })
      }
    } catch (e) {
      that.setState({
        loading: false,
        items: [],
        error: true,
        dataSource: this.getUpdatedDataSource([])
      })
    }
  }

  getUpdatedDataSource = (rows) => {
    let ids = rows.map((obj, index) => index)
    return this.state.dataSource.cloneWithRows(rows, ids)
  }

  renderRefreshControl = () => {
    return (
      <RefreshControl
        tintColor={layoutSettings.colors.TERTIARY}
        refreshing={ this.state.loading }
        onRefresh={ this.refresh }
      />
    )
  }

  refresh = () => {
    this.currentPage = 0
    this.searchRanking()
  }

  loadMoreContent = () => {
    if (
      !this.state.loading &&
      this.state.total > 0 &&
      this.state.total > this.state.items.length
    ) {
      this.currentPage ++
      this.searchRanking()
    }
  }

  renderExitButton = () => <HeaderMenu iconName='md-close' onIconClick={ () => this.props.navigation.goBack() } />

  renderRankingItem = (rankingItem, index) => (
    <View key={ index } style={ rankingStyles.rankingItem }>
      <RankingThumbnail rankingItem={ rankingItem } key={ index } index={ index } />
      <Text style={ rankingStyles.name }>{ (rankingItem.userName || '').toUpperCase() }</Text>
    </View>
  )

  componentDidMount() {
    this.refresh()
  }

  render() {

    if (this.state.loading) {
      return (
        <View style={ rankingStyles.container }>
          <Header leftTitle='RANKING' right={ this.renderExitButton } />
          <View style={ rankingStyles.container }>
            <Loader />
          </View>
        </View>
      )
    }

    if (!this.state.loading && this.state.items.length === 0) {
      return (
        <View style={ rankingStyles.container }>
          <Header leftTitle='RANKING' right={ this.renderExitButton } />
          <View style={ rankingStyles.noContentBody }>
            <View style={ rankingStyles.noContentContainer }>
              <Text style={ rankingStyles.noContentText }>
                SEJA @
              </Text>
              <Text style={ rankingStyles.noContentText }>
                PRIMEIR@ A
              </Text>
              <Text style={ rankingStyles.noContentText }>
                ENVIAR
              </Text>
              <Text style={ rankingStyles.noContentText }>
                CORAÇÕES
              </Text>
            </View>
          </View>
        </View>
      )
    }

    return this.state.loading ? (<Loader />) : (
      <View style={ rankingStyles.container }>
        <Header leftTitle='RANKING' right={ this.renderExitButton } />
        <View style={ rankingStyles.body }>
          <ListView
            dataSource={ this.state.dataSource }
            renderRow={ (row, sectionId, rowId) => this.renderRankingItem(row, rowId) }
            enableEmptySections={true}
            onEndReached={ this.loadMoreContent }
            renderFooter={ () => (this.state.loading && this.currentPage > 0 && <Loader />) }
            refreshControl={ this.renderRefreshControl() } />
        </View>
      </View>
    )
  }
}

const App = ({ actions, navigation }) => (
  <Ranking actions={ actions } navigation={ navigation } />
)

App.propTypes = {
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({ })

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
