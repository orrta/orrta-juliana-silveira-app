import React from 'react'
import RankingThumbnail from './RankingThumbnail'
import { View } from 'react-native'
import { rankingPreviewStyles } from '../../assets/styles/ranking/rankingPreview'

const RankingPreview = props => props.rankingItem.map((r, i) => (
  <View style={ rankingPreviewStyles.listItem }  key={ i }>
    <RankingThumbnail rankingItem={ r } index={ i } />
  </View>
))

export default RankingPreview
