import React from 'react'
import Thumbnail from '../common/Thumbnail'
import { Text, View } from 'react-native'
import { rankingPreviewStyles } from '../../assets/styles/ranking/rankingPreview'
import Icon from 'react-native-vector-icons/FontAwesome'

const RankingThumbnail = props => (
  <View style={ rankingPreviewStyles.rankingThumbnailContainer }>
    <View style={ rankingPreviewStyles.rankingThumbnailImage }>
      <Thumbnail style={ rankingPreviewStyles.rankingThumbnail } image={ props.rankingItem.imageUrl } />
    </View>
    <View style={ rankingPreviewStyles.rankingStarCountContainer }>
      <Text style={ rankingPreviewStyles.rankingStarCountText } >{ props.rankingItem.count }</Text>
      <Icon style={ rankingPreviewStyles.rankingStarCountIcon } name='heart' />
    </View>
    <View style={ rankingPreviewStyles.rankingPositionContainer }>
      <Text style={ rankingPreviewStyles.rankingPositionText } >{ props.index + 1 }</Text>
    </View>
  </View>
)

export default RankingThumbnail
