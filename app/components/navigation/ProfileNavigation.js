import React from 'react'
import { StackNavigator } from 'react-navigation'
import layoutSettings from '../../common/settings/layoutSettings'
import Profile from '../profile/Profile'
import TermsAndConditions from '../profile/TermsAndConditions'
import PrivacyPolicy from '../profile/PrivacyPolicy'
import Subscribe from '../subscribe/Subscribe'

const ProfileNavigation = StackNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    ProfileSubscribe: {
      screen: Subscribe,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    ProfileTermsAndConditions: {
      screen: TermsAndConditions,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    ProfilePrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    }
  }, {
    initialRouteName: 'Profile',
    headerMode: 'none',
    mode: 'modal',
    cardStyle: {
      backgroundColor: layoutSettings.colors.PRIMARY
    }
  }
)

export default ProfileNavigation
