import React from 'react'
import { StackNavigator } from 'react-navigation'

import layoutSettings from '../../common/settings/layoutSettings'

import TermsAndConditions from '../profile/TermsAndConditions'
import CommentsPage from '../comments/CommentsPage'
import PrivacyPolicy from '../profile/PrivacyPolicy'
import Search from '../search/Search'
import Subscribe from '../subscribe/Subscribe'
import TimelinePost from '../timeline/post/TimelinePost'
import Ranking from '../ranking/Ranking'


const SearchNavigation = StackNavigator(
  {
    Search: {
      screen: Search,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    SearchTimelinePost: {
      screen: TimelinePost,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    SearchPostRanking: {
      screen: Ranking,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    SearchComments: {
      screen: CommentsPage,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    SearchSubscribe: {
      screen: Subscribe,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    SearchTermsAndConditions: {
      screen: TermsAndConditions,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    SearchPrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    }
  }, {
    initialRouteName: 'Search',
    headerMode: 'none',
    mode: 'modal',
    cardStyle: {
      backgroundColor: layoutSettings.colors.PRIMARY
    }
  }
)

export default SearchNavigation
