import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { TabNavigator, TabBarTop } from 'react-navigation'
import { Platform, Dimensions, Text, View } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import { buildTitle } from '../../assets/styles/typografy'
import layoutSettings from '../../common/settings/layoutSettings'

import Login from '../login/Login'
import ErrorPage from '../errorPage/ErrorPage'
import ProfileNavigation from './ProfileNavigation'
import SearchNavigation from './SearchNavigation'
import TimelineNavigation from './TimelineNavigation'


const HeartIcon = props => {
  return (
    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={ buildTitle({
          fontSize: 12,
          color: props.color
        }) }>{ ((props.auth || {}).loggedUser || {}).available_hearts || 0 }</Text>
        <Icon style={{ color: props.color, fontSize: 10, marginTop: -2 }} name='heart' />
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center', marginLeft: 5, marginTop: 3}}>
        <Icon style={{ color: props.color, fontSize: 22 }} name='user' />
      </View>
    </View>
  )
}

const HeartIconApp = ({ auth, color }) => (
  <HeartIcon auth={auth} color={color} />
)

HeartIconApp.propTypes = {
  auth: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth
})

const ConnectedHeartIcon = connect(
  mapStateToProps
)(HeartIconApp)

const TabNavigation = TabNavigator(
  {
    MainTab: {
      screen: TimelineNavigation,
      path: '/timeline'
    },
    SearchTab: {
      screen: SearchNavigation,
      path: '/search'
    },
    StarsTab: {
      screen: ProfileNavigation,
      path: '/stars'
    }
  },
  {
    navigationOptions: ({ navigation }) => {

      let tabBarVisible = true

      if (navigation.state.routes && navigation.state.routes.length > 1) {
        tabBarVisible = (navigation.state.routes[1].params || {}).tabBarVisible
      }

      return {
        tabBarVisible,
        tabBarIcon: ({ focused, tintColor }) => {
          const { routeName } = navigation.state;
          const color = focused ? layoutSettings.colors.TERTIARY : layoutSettings.colors.TERTIARY_TRANSPARENT

          if (routeName === 'StarsTab') {
            return <ConnectedHeartIcon color={color} />
          }

          const text = {
            MainTab: 'INÍCIO',
            SearchTab: 'BUSCA',
            ProfileTab: 'PERFIL'
          }[routeName]

          return (<Text style={ buildTitle({
            fontSize: 20,
            color: color
          }) }>{text}</Text>)
        }
      }
    },
    tabBarComponent: TabBarTop,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      style: {
        paddingTop: layoutSettings.topByDevice,
        backgroundColor: layoutSettings.colors.SECONDARY,
        borderBottomColor: 'rgba(255, 255, 255, 0.4)',
        borderBottomWidth: 0,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 1,
        elevation: 2,
        shadowColor: layoutSettings.colors.SHADOW_COLOR
      },
      indicatorStyle: {
        borderBottomColor: layoutSettings.colors.TERTIARY,
        borderBottomWidth: 6
      },
      iconStyle: {
        width: 100
      }
    },
    tabBarPosition: 'top',
    animationEnabled: true,
    swipeEnabled: true,
    initialRouteName: 'MainTab'
  }
)

Navigation = TabNavigator(
  {
    Login: {
      screen: Login,
      path: '/login'
    },
    ErrorPage: {
      screen: ErrorPage,
      path: '/errorPage'
    },
    App: {
      screen: TabNavigation,
      path: '/tabNavigation'
    }
  },
  {
    navigationOptions: {
      tabBarVisible: false
    },
    tabBarOptions: {
      showIcon: false,
      showLabel: false,
    },
    tabBarPosition: 'top',
    animationEnabled: false,
    swipeEnabled: false,
    initialRouteName: 'Login'
  }
)

export default Navigation
