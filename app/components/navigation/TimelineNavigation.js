import React from 'react'

import { StackNavigator } from 'react-navigation'
import Timeline from '../timeline/Timeline'
import Subscribe from '../subscribe/Subscribe'
import TimelinePost from '../timeline/post/TimelinePost'
import Ranking from '../ranking/Ranking'
import CommentsPage from '../comments/CommentsPage'
import TermsAndConditions from '../profile/TermsAndConditions'
import PrivacyPolicy from '../profile/PrivacyPolicy'

import layoutSettings from '../../common/settings/layoutSettings'

const TimelineNavigation = StackNavigator(
  {
    Timeline: {
      screen: Timeline,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Subscribe: {
      screen: Subscribe,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    TimelinePost: {
      screen: TimelinePost,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    PostRanking: {
      screen: Ranking,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    TimelineComments: {
      screen: CommentsPage,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    TermsAndConditions: {
      screen: TermsAndConditions,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    },
    PrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: {
        gesturesEnabled: false,
        swipeEnabled: false
      }
    }
  }, {
    initialRouteName: 'Timeline',
    headerMode: 'none',
    mode: 'modal',
    cardStyle: {
      backgroundColor: layoutSettings.colors.PRIMARY
    }
  }
)

export default TimelineNavigation
