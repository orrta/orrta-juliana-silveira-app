import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { View, ScrollView, Text, TouchableWithoutFeedback } from 'react-native'
import Loader from '../common/Loader'
import { searchStyles } from '../../assets/styles/search/search'
import HorizontalTimeline from '../timeline/horizontal-timeline/HorizontalTimeline'
import SearchBar from './SearchBar'
import Icon from 'react-native-vector-icons/Ionicons'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'


import Timeline from '../timeline/Timeline'

const SearchTimeline = ({ auth, timeline, actions, navigation, onSlidingStart, onSlidingComplete }) => (
  <Timeline
    auth={ auth }
    onSlidingStart={ onSlidingStart }
    onSlidingComplete={ onSlidingComplete }
    timeline={ timeline.searchResults }
    actions={ actions }
    navigation={ navigation }
    timelinePostNavigation='SearchTimelinePost'
    timelineCommentsNavigation='SearchComments'
    search={ true } />
)

SearchTimeline.propTypes = {
  auth: PropTypes.object.isRequired,
  timeline: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const _mapStateToProps = state => ({
  timeline: state.timeline,
  auth: state.auth
})

const _mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

ConnectedSearchTimeline = connect(
  _mapStateToProps,
  _mapDispatchToProps
)(SearchTimeline)

class Search extends React.Component {

  state = { activeSearch: false, activeFilter: false, loading: false, scrollEnabled: true }

  search = (queryName, indexName, searchTerm) => {
    this.props.actions.loadTimeline(queryName, indexName, searchTerm)
    this.setState({ activeSearch: true })
  }

  viewMore = (queryName, indexName, searchTerm, filterName) => {
    this.props.actions.loadTimeline(queryName, indexName, searchTerm)
    this.setState({ activeFilter: true, filterName })
  }

  clearSearch = () => {
    this.setState({ ...this.state, loading: true })
    setTimeout(() => {
      this.setState({ activeSearch: false, loading: false })
    }, 1000)
  }

  clearFilter = () => {
    this.setState({ ...this.state, loading: true })
    setTimeout(() => {
      this.setState({ activeFilter: false, loading: false })
    }, 1000)
  }

  componentDidMount() {
    this.props.actions.loadCategories()
  }

  render() {
    return (
      <View style={ searchStyles.container } scrollEnabled={ this.state.scrollEnabled }>
        {
          !this.state.activeFilter && (
            <SearchBar onSearch={ this.search } clearSearch={ this.clearSearch } />
          )
        }

        {
          this.state.activeFilter && !this.state.loading && (
            <View style={ searchStyles.filterTitleContainer }>
              <Text style={ searchStyles.filterTitleText }>
                { this.state.filterName }
              </Text>
              <TouchableWithoutFeedback onPress={ this.clearFilter }>
                <View style={ searchStyles.filterExitContainer }>
                  <FontAwesomeIcon name='close' style={ searchStyles.filterExitIcon } />
                </View>
              </TouchableWithoutFeedback>
            </View>
          )
        }

        {
          (this.state.activeSearch || this.state.activeFilter) && !this.state.loading && (
            <ConnectedSearchTimeline
              { ...this.props }
              search={ true } />
          )
        }

        {
          this.state.loading ? (
            <Loader />
          ) : !this.state.activeSearch && !this.state.activeFilter && (
            <ScrollView>
              <HorizontalTimeline
                viewMore={ this.viewMore }
                title='Mais Adorados'
                navigation={ this.props.navigation }
                timelinePostNavigation={ 'SearchTimelinePost' }
                search={ true }
                mostLoved={ true } />
              {
                this.props.categories.loading ? (
                  <Loader />
                ) : (
                  this.props.categories.items.map((category, index) => (
                    <HorizontalTimeline
                      key={ index }
                      viewMore={ this.viewMore }
                      title={ category.title }
                      navigation={ this.props.navigation }
                      timelinePostNavigation={ 'SearchTimelinePost' }
                      search={ true }
                      index={ category.indexName } />
                  ))
                )
              }

            </ScrollView>
          )
        }

      </View>
    )
  }
}

const App = ({ actions, navigation, categories }) => (
  <Search actions={ actions } navigation={ navigation } categories={ categories } />
)

App.propTypes = {
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  categories: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  categories: state.categories
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
