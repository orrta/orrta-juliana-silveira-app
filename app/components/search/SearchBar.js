import React from 'react'
import { View, TextInput, Text, TouchableWithoutFeedback } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { searchStyles } from '../../assets/styles/search/search'
import layoutSettings from '../../common/settings/layoutSettings'

class SearchBar extends React.Component {

  state = { searchWasExecuted: false }
  inputs = {}

  onClearSearch = () => {
    this.setState({ searchTerm: '', searchWasExecuted: false })
    this.props.clearSearch()
  }

  onExecuteSearch = () => {
    this.props.onSearch('timeline-search', 'timeline--all-content', this.state.searchTerm)
    this.setState({ searchTerm: this.state.searchTerm, searchWasExecuted: true })
  }

  renderClearButton = () => {
    if (this.state.searchWasExecuted) {
      return (
        <TouchableWithoutFeedback onPress={ this.onClearSearch }>
          <Icon style={[ searchStyles.icon, searchStyles.iconRight ]} name='md-close-circle' />
        </TouchableWithoutFeedback>
      )
    }
  }

  renderSearchButton = () => {
    if (!this.state.searchWasExecuted) {
      return (
        <TouchableWithoutFeedback onPress={ () => this.inputs['searchInput'].focus() }>
          <Icon style={ searchStyles.icon } name='md-search' />
        </TouchableWithoutFeedback>
      )
    }
  }

  render() {
    return (
      <View style={ searchStyles.searchContainer }>
        { this.renderSearchButton() }
        <TextInput
          underlineColorAndroid='transparent'
          style={ searchStyles.input }
          placeholder='Buscar Postagem...'
          placeholderTextColor={ layoutSettings.colors.TERTIARY }
          blurOnSubmit={ true }
          returnKeyType={ 'search' }
          onSubmitEditing={ this.onExecuteSearch }
          ref={ input => { this.inputs['searchInput'] = input }}
          onChangeText={ searchTerm => this.setState({ searchTerm }) }
          value={ this.state.searchTerm }/>
        { this.renderClearButton() }
      </View>
    )
  }
}

export default SearchBar
