import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { View, Text } from 'react-native'
import { CachedImage as Image } from 'react-native-cached-image'
import { Slider } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import layoutSettings from '../../common/settings/layoutSettings'

import { dateStringToOrrtaLabel } from '../../common/helpers/date'
import { postCardStyles } from '../../assets/styles/timelineCard'
import { postCoverStyles, postInfoStyles } from '../../assets/styles/timeline/postCover'

import OrrtaButton from '../common/OrrtaButton'
import Loader from '../common/Loader'
import RankingPreview from '../ranking/RankingPreview'


class PostCover extends React.Component {

  state = {
    heartsCount: 1,
    heartSliderOpenned: false,
    heartsSendingLoading: false,
    max: this.props.auth.loggedUser.available_hearts || 0,
    invertedToSlider: {
      heartsCount: -1,
      min: (this.props.auth.loggedUser.available_hearts || 0) * -1,
      max: -1
    }
  }

  hideHeartsLoader = () => this.setState({ ...this.state, heartsSendingLoading: false })

  PostRankingNavigation = () => this.props.search ? 'SearchPostRanking' : 'PostRanking'

  SubscribeNavigation = () => this.props.search ? 'SearchSubscribe' : 'Subscribe'

  onSliderChange = newInvertedValue => this.setState({
    ...this.state,
    heartsCount: parseFloat(newInvertedValue) * -1,
    invertedToSlider: {
      ...this.state.invertedToSlider,
      heartsCount: parseFloat(newInvertedValue),
    }
  })

  openHeartSlider = () => {
    if (this.props.auth.anonimous) {
      this.props.actions.openModal({
        title: 'Ops...',
        subtitle: 'Você precisa estar logado para enviar corações!',
        buttons: [{
          text: 'Fazer Login',
          onPress: () => this.props.navigation.navigate('Login')
        }, {
          outline: true,
          text: 'Cancelar'
        }]
      })
    } else {
      this.setState({ ...this.state, heartSliderOpenned: true })

      if (this.props.auth.loggedUser.newUser) {
        const tutorialShown = this.props.auth.loggedUser.tutorialsShown.find(t => t === 'sending-hearts')

        if (!tutorialShown) {
          setTimeout(() => {
            this.props.actions.openModal({
              title: 'Envio de Corações',
              image: 'sending-hearts',
              body: 'É possível enviar seus corações nas matérias que desejar. Os fãs que enviam mais corações ficam em evidência no ranking!',
              buttons: [{
                text: 'ENTENDIDO!',
                onPress: () => this.props.actions.setTutorialAsShown('sending-hearts')
              }]
            })
          }, 100)
        }
      }
    }
  }

  closeHeartSlider = () => this.setState({ ...this.state, heartSliderOpenned: false })

  sendHearts = () => {
    const post = this.props.post

    this.setState({ ...this.state, heartsSendingLoading: true, heartSliderOpenned: false })

    this.props.actions.sendHearts({
      user_id: this.props.auth.loggedUser.id,
      post_id: post.id,
      imageUrl: this.props.auth.loggedUser.imageUrl,
      userName: this.props.auth.loggedUser.name,
      count: this.props.auth.loggedUser.available_hearts == 0 ? 0 : this.state.heartsCount,
      section: post.section
    }, () => {
      this.props.actions.openModal({
        title: 'Parabéns',
        subtitle: `${this.state.heartsCount == 0 ? 1 : this.state.heartsCount} ${ this.state.heartsCount > 1 ? 'corações enviados' : 'coração enviado' }! ❤`,
        buttons: [{
          text: 'OK',
          onPress: () => this.setState({ ...this.state, heartsSendingLoading: false })
        }]
      })
    }, () => {
      this.props.actions.openModal({
        title: 'Ops...',
        subtitle: `Não conseguimos enviar os corações, tente novamente em alguns minutos`,
        buttons: [{
          text: 'OK',
          onPress: () => this.setState({ ...this.state, heartsSendingLoading: false })
        }]
      })
    })
  }

  viewRanking = () => this.props.navigation.navigate(this.PostRankingNavigation(), {
    tabBarVisible: false,
    postId: this.props.post.id
  })

  locked = () =>
    !this.props.hideLock &&
    this.props.post.exclusive &&
    (!this.props.auth.loggedUser.subscriber || this.props.auth.anonimous)

  get getDateLabel() {
    return dateStringToOrrtaLabel(this.props.post.dateTime)
  }

  componentWillReceiveProps(nextProps) {
    let newState = { ...this.state }

    newState.max = nextProps.auth.loggedUser.available_hearts || 0,
    newState.invertedToSlider.min = (nextProps.auth.loggedUser.available_hearts || 0) * -1

    if (this.state.heartsCount >= (nextProps.auth.loggedUser.available_hearts || 0)) {
      newState.heartsCount = nextProps.auth.loggedUser.available_hearts || 0
      newState.invertedToSlider.heartsCount = (nextProps.auth.loggedUser.available_hearts || 0) * -1
    }

    this.setState(newState)
  }

  render() {
    return (
      <View style={
        this.props.top ? postCardStyles.imageContainerTop : postCardStyles.imageContainer
      } >

        <Image source={ { uri: this.props.post.imageUrl } } style={
          this.props.top ? postCardStyles.imageTop : postCardStyles.image
        } />

        <View style={ postInfoStyles.container }>
          {
            !this.props.hideClock && (
              <View style={ postInfoStyles.content }>
                <Icon style={ postInfoStyles.icon } name='clock' />
                <Text style={ postInfoStyles.text }>{ this.getDateLabel }</Text>
              </View>
            )
          }

          {
            this.props.post.type === 'quiz' && (
              <View style={ postInfoStyles.content }>
                <Icon style={ postInfoStyles.iconGame } name='google-controller' />
                <Text style={ postInfoStyles.text }>INTERATIVO</Text>
              </View>
            )
          }
        </View>

        { this.locked() && (
          <View style={ postCardStyles.locked }>
            <View style={ postCardStyles.lockedContainer }>
              <View style={ postCardStyles.lockedIconContent }>
                <Icon style={ postCardStyles.lockedIcon } name='lock-outline' />
              </View>
            </View>
          </View>
        ) }

        {
          this.state.heartSliderOpenned ? (
            <View style={ postCardStyles.ranking }>
              <OrrtaButton
                iconName='close'
                outline={ true }
                containerStyle={ postCoverStyles.closeSendHeartsButton }
                onPress={ this.closeHeartSlider }/>
              {
                (this.props.auth.loggedUser.available_hearts || 0) > 1 && (
                  <View style={ postCoverStyles.sendHeartsContainer } >

                    <Text style={ postCoverStyles.sliderText }>
                      { this.state.max }
                    </Text>
                    <View style={ postCoverStyles.sendHeartsSliderContainer } >
                      <Slider
                        orientation='vertical'
                        step={1}
                        maximumValue={ this.state.invertedToSlider.max }
                        minimumValue={ this.state.invertedToSlider.min }
                        onValueChange={ this.onSliderChange }
                        value={ this.state.invertedToSlider.heartsCount }
                        style={ postCoverStyles.sendHeartsSlider }
                        trackStyle={ postCoverStyles.sendHeartsSliderTrack }
                        minimumTrackTintColor={ layoutSettings.colors.TERTIARY }
                        maximumTrackTintColor={ layoutSettings.colors.TERTIARY }
                        thumbStyle={ postCoverStyles.sendHeartsSliderThumb }
                        onSlidingStart={ this.props.onSlidingStart }
                        onSlidingComplete={ this.props.onSlidingComplete }
                      />
                    </View>
                    <Text style={ postCoverStyles.sliderText }>1</Text>

                  </View>
                )
              }
              <OrrtaButton
                iconName='check'
                containerStyle={ postCoverStyles.sendHeartsCheckButton }
                text={ `${this.state.heartsCount > 0 ? this.state.heartsCount : 1}` }
                onPress={ this.sendHearts } />
            </View>
          ) : this.state.heartsSendingLoading ? (
            <View style={ postCardStyles.ranking }>
              <View style={ postCoverStyles.sendHeartsLoading }>
                <Loader color={ layoutSettings.colors.PRIMARY } />
              </View>
            </View>
          ) : (
            <View style={ postCardStyles.ranking }>
              <RankingPreview rankingItem={ this.props.post.ranking } />
              <OrrtaButton
                iconName='heart'
                containerStyle={ postCoverStyles.sendHeartsOpenSliderButton }
                onPress={ this.openHeartSlider } />
            </View>
          )
        }

        <View style={ postCardStyles.actions }>
          <OrrtaButton
            iconName='heart'
            text={ this.props.post.hearts_count || 0 }
            containerStyle={{
              flexDirection: 'row',
              paddingVertical: 5,
              paddingTop: 6,
              marginRight: 0,
              backgroundColor: layoutSettings.colors.SECONDARY
            }}
            textStyle={{
              fontSize: 16,
              color: layoutSettings.colors.TERTIARY
            }}
            iconStyle={{
              fontSize: 20,
              marginLeft: 4,
              color: layoutSettings.colors.TERTIARY
            }}
            onPress={ this.viewRanking } />

          <OrrtaButton
            iconName='message-text'
            text={ this.props.post.comments_count || 0 }
            containerStyle={{
              flexDirection: 'row',
              paddingVertical: 5,
              paddingTop: 6,
              marginRight: 0,
              backgroundColor: layoutSettings.colors.SECONDARY
            }}
            textStyle={{
              fontSize: 16,
              color: layoutSettings.colors.TERTIARY
            }}
            iconStyle={{
              fontSize: 20,
              marginLeft: 4,
              color: layoutSettings.colors.TERTIARY
            }}
            onPress={ this.props.onCommentCountClick } />
        </View>

      </View>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostCover)
