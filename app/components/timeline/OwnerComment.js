import React from 'react'
import Thumbnail from '../common/Thumbnail'
import { Text, View } from 'react-native'
import { ownerCommentStyles } from '../../assets/styles/timeline/ownerComment'

const OwnerComment = props => (
  <View style={ ownerCommentStyles.container }>
    <Text style={ ownerCommentStyles.title }>Mensagem da Juliana</Text>
    <View style={ ownerCommentStyles.comment }>
        <Thumbnail />
      <View style={ ownerCommentStyles.ownerCommentContent }>
        {
          props.comment.map((c, i) => {
            return (<Text key={ i } style={ ownerCommentStyles.ownerCommentText }>{ c }</Text>)
          })
        }
      </View>
    </View>
  </View>
)

export default OwnerComment
