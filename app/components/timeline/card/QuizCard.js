import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../../actions'

import { Text, View, TouchableWithoutFeedback } from 'react-native'
import { CachedImage as Image } from 'react-native-cached-image'
import { quizCardStyles } from '../../../assets/styles/timeline/quizCard'
import Icon from 'react-native-vector-icons/FontAwesome'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { dateStringToOrrtaLabel } from '../../../common/helpers/date'

import PostCover from '../PostCover'

class PostCard extends React.Component {

  SubscribeNavigation = () => this.props.search ? 'SearchSubscribe' : 'Subscribe'
  TermsAndConditionsNavigation = () => this.props.search ? 'SearchTermsAndConditions' : 'TermsAndConditions'
  PrivacyPolicyNavigation = () => this.props.search ? 'SearchPrivacyPolicy' : 'PrivacyPolicy'

  viewComments = () => {
    this.props.onCommentsPress(this.props.post)
  }

  viewPost = () => {
    if (this.props.auth.anonimous) {
      this.props.actions.openModal({
        title: 'Ops...',
        subtitle: 'Você precisa estar logado para participar de um QUIZ!',
        buttons: [{
          text: 'Fazer Login',
          onPress: () => this.props.navigation.navigate('Login')
        }, {
          outline: true,
          text: 'Cancelar'
        }]
      })
    } else if (this.props.post.exclusive) {
      this.props.navigation.navigate(this.SubscribeNavigation(), {
        tabBarVisible: false,
        termsAndConditionsNav: this.TermsAndConditionsNavigation(),
        privacyPolicyNav: this.PrivacyPolicyNavigation()
      })
    } else {
      this.props.onCardClick(this.props.post)
    }
  }

  get getDateLabel() {
    return dateStringToOrrtaLabel(this.props.post.dateTime)
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={ this.viewPost }>
        <View style={ quizCardStyles.card }>
          <PostCover
            top={ true }
            onCommentCountClick={ this.viewComments }
            search={ this.props.search }
            navigation={ this.props.navigation }
            onSlidingStart={ this.props.onSlidingStart }
            onSlidingComplete={ this.props.onSlidingComplete }
            post={ this.props.post }/>
          <View style={ quizCardStyles.titleContainer }>
            <View style={ quizCardStyles.subTitleContainer }>
              <Text style={ quizCardStyles.subTitle }>{ this.props.post.content_section }</Text>
            </View>
            <Text style={ quizCardStyles.title }>
              { this.props.post.title }
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostCard)
