import React from 'react'
import AdvertisingCard from './AdvertisingCard'
import InviteFriendsCard from './InviteFriendsCard'
import PostCard from './PostCard'
import QuizCard from './QuizCard'

import { View } from 'react-native'

const TimelineCard = props => ({
  'advertising': <AdvertisingCard { ...props } />,
  'multi-content': <PostCard { ...props } />,
  'inviteFriends': <InviteFriendsCard { ...props } />,
  'quiz': <QuizCard { ...props } />,
}[props.post.type] || <PostCard { ...props } />)

export default TimelineCard
