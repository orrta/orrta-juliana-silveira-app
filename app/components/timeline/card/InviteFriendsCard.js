import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../../actions'

import { Text, View, Share } from 'react-native'
import { inviteFriendsCardStyles } from '../../../assets/styles/timelineCard'
import OrrtaButton from '../../common/OrrtaButton'
import appSettings from '../../../common/settings/appSettings'

const share = sharingCode => {
  Share.share({
    message: `A atriz de Floribella, Juliana Silveira, criou seu próprio aplicativo para compartilhar suas intimidades, rotina e muito mais.\n\n
Use meu cupom: ${sharingCode} para ganhar ${appSettings.SENDER_USER_HEARTS_COUNT} corações no aplicativo e assim ser destaque em cada matéria que ela publicar!\n\n
Baixe agora clicando no link: \n
${appSettings.APP_DOWNLOAD_LINK}`,
    title: 'Você já viu o aplicativo da Juliana Silveira?'
  })
}

const insertCoupon = (navigation, isProfilePage) => {
  navigation.navigate(
    isProfilePage ? 'ProfileInsertDiscountCoupon' : 'InsertDiscountCoupon',
    { tabBarVisible: false })
}

const InviteFriendsCard = props => (
  <View style={  inviteFriendsCardStyles.card }>
    <View style={ inviteFriendsCardStyles.titleContainer }>
      <Text style={ inviteFriendsCardStyles.title }>CONVIDE AMIGOS!</Text>
    </View>
    <View style={ inviteFriendsCardStyles.cardContainer }>
      <View style={ inviteFriendsCardStyles.contentContainer }>
        <Text style={ inviteFriendsCardStyles.content }>
          ACUMULE MAIS CORAÇÕES CONVIDANDO OS SEUS AMIGOS ATRAVÉS DO SEU CUPOM INDIVIDUAL. CADA VEZ QUE UM AMIGO UTILIZAR SEU CUPOM VOCÊ GANHA {appSettings.OWNER_USER_HEARTS_COUNT} CORAÇÕES.
        </Text>
      </View>

      <OrrtaButton
        containerStyle={ inviteFriendsCardStyles.buttonContainer }
        textStyle={ inviteFriendsCardStyles.buttonText }
        text='CONVIDE AMIGOS'
        onPress={ () => share(props.auth.loggedUser.sharingCode) }/>

    </View>
  </View>
)

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InviteFriendsCard)
