import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../../actions'

import { Text, View } from 'react-native'
import { advertisingCardStyles } from '../../../assets/styles/timelineCard'
import OrrtaButton from '../../common/OrrtaButton'


const goToSubscribe = (navigation, isProfilePage, isSearchPage) => {

  let nav = 'Subscribe'
  let termsAndConditionsNav = 'TermsAndConditions'
  let privacyPolicyNav = 'PrivacyPolicy'

  if (isProfilePage) {
    nav = 'ProfileSubscribe'
    termsAndConditionsNav = 'ProfileTermsAndConditions'
    privacyPolicyNav = 'ProfilePrivacyPolicy'
  }

  if (isSearchPage) {
    nav = 'SearchSubscribe'
    termsAndConditionsNav = 'ProfileTermsAndConditions'
    privacyPolicyNav = 'ProfilePrivacyPolicy'
  }

  navigation.navigate(nav, { termsAndConditionsNav, privacyPolicyNav, tabBarVisible: false })
}

const AdvertisingCard = props => (props.auth.anonimous || !props.auth.loggedUser.subscriber) && (
  <View style={  advertisingCardStyles.card }>
    <View style={ advertisingCardStyles.titleContainer }>
      <Text style={ advertisingCardStyles.title }>CONTEÚDO EXCLUSIVO</Text>
    </View>
    <View style={ advertisingCardStyles.advertisingCard }>
      <View style={ advertisingCardStyles.contentContainer }>
        <Text style={ advertisingCardStyles.content }>
          ASSINE HOJE E SAIBA DA MINHA ROTINA ATRAVÉS DE QUADROS INCRÍVEIS COMO O “PERGUNTA QUE EU TE FALO”, “QUERIDO DIÁRIO”, “MAKING OF” E MUITO MAIS! VOU FALAR SOBRE DICAS DE MATERNIDADE, MODA, CARREIRA, SAÚDE E ETC.! AO ASSINAR VOCÊ IRÁ ACUMULAR 200 CORAÇÕES TODO MÊS PARA FICAR EM EVIDÊNCIA NO RANKING DAS POSTAGENS.
        </Text>
      </View>
      <OrrtaButton
        containerStyle={ advertisingCardStyles.buttonContainer }
        textStyle={ advertisingCardStyles.buttonText }
        text='ASSINAR'
        onPress={ () => goToSubscribe(props.navigation, props.profile, props.search) }/>
    </View>
  </View>
)

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvertisingCard)
