import React from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../../actions'

import { Text, View, TouchableWithoutFeedback } from 'react-native'
import { postCardStyles } from '../../../assets/styles/timelineCard'

import PostCover from '../PostCover'

class PostCard extends React.Component {

  SubscribeNavigation = () => this.props.search ? 'SearchSubscribe' : 'Subscribe'
  TermsAndConditionsNavigation = () => this.props.search ? 'SearchTermsAndConditions' : 'TermsAndConditions'
  PrivacyPolicyNavigation = () => this.props.search ? 'SearchPrivacyPolicy' : 'PrivacyPolicy'

  viewPost = () => {
    if (this.props.post.exclusive && this.props.auth.anonimous) {
      this.props.actions.openModal({
        title: `Ops...`,
        subtitle: 'Essa funcionalidade é exclusiva para usuários logados assinantes',
        buttons: [{
          text: 'Fazer Login',
          onPress: () => this.props.navigation.navigate('Login')
        }, {
          outline: true,
          text: 'Cancelar'
        }]
      })
    } else if (this.props.post.exclusive && !this.props.auth.loggedUser.subscriber) {
      this.props.navigation.navigate(this.SubscribeNavigation(), {
        tabBarVisible: false,
        termsAndConditionsNav: this.TermsAndConditionsNavigation(),
        privacyPolicyNav: this.PrivacyPolicyNavigation()
      })
    } else {
      this.props.onCardClick(this.props.post)
    }
  }

  viewComments = () => {
    this.props.onCommentsPress(this.props.post)
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={ this.viewPost }>
        <View style={ postCardStyles.card }>
          <View style={ postCardStyles.titleContainer }>
            <View style={ postCardStyles.subTitleContainer }>
              <Text style={ postCardStyles.subTitle }>{ this.props.post.content_section }</Text>
            </View>
            <Text style={ postCardStyles.title }>{ this.props.post.title }</Text>
          </View>
          <PostCover
            onCommentCountClick={ this.viewComments }
            search={ this.props.search }
            navigation={ this.props.navigation }
            onSlidingStart={ this.props.onSlidingStart }
            onSlidingComplete={ this.props.onSlidingComplete }
            post={ this.props.post }/>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostCard)
