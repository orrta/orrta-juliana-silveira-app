import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../../actions'

import HorizontalTimelineView from './HorizontalTimelineView'
import Loader from '../../common/Loader'

import apisSettings from '../../../common/settings/apisSettings'

import { guid } from '../../../common/helpers/uuid'


class HorizontalTimeline extends React.Component {

  state = { loading: true, items: [] }

  queryName = () => this.props.mostLoved ? 'timeline-most-loved' : 'timeline-default'
  indexName = () => this.props.index || 'timeline--all-content'
  SubscribeNavigation = () => this.props.search ? 'SearchSubscribe' : 'Subscribe'
  TermsAndConditionsNavigation = () => this.props.search ? 'SearchTermsAndConditions' : 'TermsAndConditions'
  PrivacyPolicyNavigation = () => this.props.search ? 'SearchPrivacyPolicy' : 'PrivacyPolicy'

  viewMore = () => {
    this.props.viewMore(this.queryName(), this.indexName(), null, this.props.title)
  }

  onCardPress = post => this.props.actions.openPost(post, () => {
    if (post.exclusive && this.props.auth.anonimous) {
      this.props.actions.openModal({
        title: 'Ops...',
        subtitle: 'Essa funcionalidade é exclusiva para usuários logados assinantes',
        buttons: [{
          text: 'Fazer Login',
          onPress: () => this.props.navigation.navigate('Login')
        }, {
          outline: true,
          text: 'Cancelar'
        }]
      })
    } else if (post.exclusive && !this.props.auth.loggedUser.subscriber) {
      this.props.navigation.navigate(this.SubscribeNavigation(), {
        tabBarVisible: false,
        termsAndConditionsNav: this.TermsAndConditionsNavigation(),
        privacyPolicyNav: this.PrivacyPolicyNavigation()
      })
    } else {
      if (this.props.multiContentCardClickCallback) {
        this.props.multiContentCardClickCallback()
      } else {
        this.props.navigation.navigate(this.props.timelinePostNavigation, {
          tabBarVisible: false,
          search: this.props.search
        })
      }
    }
  })

  componentDidMount() {
    this.id = guid()
    this.props.actions.loadHorizontalTimeline(this.id, this.indexName(), this.queryName())
  }

  render() {

    const timeline = this.props.horizontalTimeline.timelines.find(t => t.id === this.id)

    if (!timeline) {
      return <Loader />
    }

    return timeline.loading ? (
      <Loader />
    ) : (
      <HorizontalTimelineView
        viewMoreEnabled={ !!this.props.viewMore }
        viewMore={ this.viewMore }
        posts={ (timeline.items || []).filter(i => i.id !== this.props.currentPostId) }
        title={ this.props.title.toUpperCase() }
        auth={ this.props.auth }
        onCardPress={ this.onCardPress } />
    )
  }

}

const App = ({
  multiContentCardClickCallback,
  actions,
  navigation,
  timelinePostNavigation,
  title,
  index,
  currentPostId,
  viewMore,
  mostLoved,
  search,
  auth,
  horizontalTimeline
}) => (
  <HorizontalTimeline
    multiContentCardClickCallback={ multiContentCardClickCallback }
    actions={ actions }
    navigation={ navigation }
    timelinePostNavigation={ timelinePostNavigation }
    currentPostId={ currentPostId }
    title={ title }
    index={ index }
    viewMore={ viewMore }
    mostLoved={ mostLoved }
    search={ search }
    auth={ auth }
    horizontalTimeline={ horizontalTimeline }/>
)

App.propTypes = {
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
  timelinePostNavigation: PropTypes.string,
  title: PropTypes.string.isRequired,
  currentPostId: PropTypes.string,
  mostLoved: PropTypes.bool,
  multiContentCardClickCallback: PropTypes.func,
  index: PropTypes.string,
  viewMore: PropTypes.func,
  search: PropTypes.bool,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
  auth: state.auth,
  horizontalTimeline: state.horizontalTimeline
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
