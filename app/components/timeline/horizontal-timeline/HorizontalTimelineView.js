import React from 'react'
import { TouchableWithoutFeedback, Text, View, ScrollView } from 'react-native'
import { CachedImage as Image } from 'react-native-cached-image'
import { horizontalTimelineStyles } from '../../../assets/styles/timeline/horizontalTimeline'
import Icon from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const HorizontalTimelineView = props => (
  <View style={ horizontalTimelineStyles.section } >
    <View style={ horizontalTimelineStyles.header }>
      <Text style={ horizontalTimelineStyles.sectionTitle } >{ props.title }</Text>
      {
        props.viewMoreEnabled && (
          <TouchableWithoutFeedback onPress={ props.viewMore }>
            <View>
              <Text style={ horizontalTimelineStyles.showMore } >
                Todos
                <Icon style={ horizontalTimelineStyles.iconRight } name='chevron-thin-right' />
              </Text>
            </View>
          </TouchableWithoutFeedback>
        )
      }
    </View>

    <ScrollView horizontal={ true } >
      <View style={ horizontalTimelineStyles.list }>
        {
          props.posts.map((post, i) => (
            <TouchableWithoutFeedback key={ i } onPress={ () => props.onCardPress(post) }>
              <View style={ horizontalTimelineStyles.post } >
                <View style={ horizontalTimelineStyles.postTitleConteiner } >
                  <Text style={ horizontalTimelineStyles.postTitleText } numberOfLines={ 1 } >{ post.title }</Text>
                </View>
                <View style={ horizontalTimelineStyles.postImageConteiner } >
                  <Image source={ { uri: post.miniImageUrl } } style={ horizontalTimelineStyles.postImage } />

                  { post.exclusive && (props.auth.anonimous || !props.auth.loggedUser.subscriber) && (
                    <View style={ horizontalTimelineStyles.locked }>
                      <View style={ horizontalTimelineStyles.lockedContainer }>
                        <View style={ horizontalTimelineStyles.lockedIconContent }>
                          <MaterialCommunityIcon style={ horizontalTimelineStyles.lockedIcon } name='lock-outline' />
                        </View>
                      </View>
                    </View>
                  ) }

                </View>
              </View>
            </TouchableWithoutFeedback>
          ))
        }
      </View>
    </ScrollView>
  </View>
)

export default HorizontalTimelineView
