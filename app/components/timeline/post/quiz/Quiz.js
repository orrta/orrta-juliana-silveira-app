import React from 'react'

import { StatusBar, View, Text, Dimensions } from 'react-native'
import VideoPlayer from 'react-native-video-player'
import { CachedImage as Image } from 'react-native-cached-image'
import Toast from 'react-native-easy-toast'

import apisSettings from '../../../../common/settings/apisSettings'
import layoutSettings from '../../../../common/settings/layoutSettings'

import Loader from '../../../common/Loader'
import Video from '../../../common/Video'
import OrrtaButton from '../../../common/OrrtaButton'
import ToastHearts from '../../../common/ToastHearts'
import { quizStyles } from '../../../../assets/styles/quiz/quiz'
import { videoStyles } from '../../../../assets/styles/common'
import { toastStyles } from '../../../../assets/styles/toast'


const width = Dimensions.get('window').width
const height = Dimensions.get('window').height


class Quiz extends React.Component {

  state = {
    quizState: 'showingQuestionVideo',
    loading: false,
    userAnswer: null,
    otherAlternative: null,
    userHasAlreadyAnswered: false
  }

  goToSubscribePage = () => this.props.navigation.navigate((this.props.navigation.state.params.search ? 'SearchSubscribe' : 'Subscribe'), {
    tabBarVisible: false,
    termsAndConditionsNav: this.props.navigation.state.params.search ? 'SearchTermsAndConditions' : 'TermsAndConditions',
    privacyPolicyNav: this.props.navigation.state.params.search ? 'SearchPrivacyPolicy' : 'PrivacyPolicy'
  })

  goToUserHasAlreadyAnsweredScreen = () => this.setState({ quizState: 'userHasAlreadyAnswered' })

  goToAnwserFeedbackVideo = () => this.setState({ quizState: 'showingAnwserFeedbackVideo' })

  goToOtherAlternativeVideo = () => {
    if (this.props.auth.loggedUser.subscriber) {
      this.setState({ quizState: 'showingOtherAlternativeVideo' })
    } else {
      this.props.actions.openModal({
        title: `Olá, ${this.props.auth.loggedUser.name}!`,
        subtitle: `Essa funcionalidade é exclusiva para assinantes!`,
        buttons: [{
          text: 'Assinar',
          onPress: this.goToSubscribePage
        }, {
          outline: true,
          text: 'Cancelar'
        }]
      })
    }
  }

  goToQuestionVideo = () => this.setState({ quizState: 'showingQuestionVideo' })

  goToAnswerScreen = () => this.setState({
    quizState: this.state.userHasAlreadyAnswered ? 'userHasAlreadyAnswered' : 'showingAnwserScreen'
  })

  userHasAlreadyAnsweredThisQuestion = async () => {
    const postId = this.props.post.currentPost.id
    const userId = this.props.auth.loggedUser.id

    try {
      const response =  await fetch(`${apisSettings.AGGREGATION.URL}/quiz-answer/${postId}:${userId}`, {
        headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY },
        'Cache-Control': 'no-cache'
      })

      switch (response.status) {
        case 200:
          const successResponseJson = await response.json()

          this.setState({
            userHasAlreadyAnswered: true,
            userAnswer: successResponseJson.es._source.userAnswer,
            otherAlternative: successResponseJson.es._source.otherAlternative
          })

          break

        case 404:
          break

        default:
          this.props.actions.openModal({ title: 'Ops...', subtitle: 'Falha ao verificar seu voto. Tente novamente em alguns minutos.' })
      }
    } catch (e) {
      console.error(e)
      this.props.actions.openModal({ title: 'Ops...', subtitle: 'Falha ao verificar seu voto. Tente novamente em alguns minutos.' })
    }
  }

  handleUserAnswer = async (userAnswer, otherAlternative) => {
    this.setState({ loading: true })

    const url = `${apisSettings.USER_ITERATION.URL}/quiz-answer`

    const body = JSON.stringify({
      postId: this.props.post.currentPost.id,
      userId: this.props.auth.loggedUser.id,
      userAnswer,
      otherAlternative
    })

    try {
      const response =  await fetch(url, {
        method: 'POST',
        body: body,
        headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY }
      })

      switch (response.status) {
        case 200:
          this.setState({
            userAnswer,
            otherAlternative,
            loading: false,
            quizState: 'showingAnwserFeedbackVideo',
            userHasAlreadyAnswered: true
          })
          this.props.actions.updateUserHearts(5)
          setTimeout(() => {
            this.refs.toast && this.refs.toast.show(<ToastHearts heartsCount={ 5 } />, 3000)
          }, 5000)
          break

        case 400:
          const responseJson = await response.json()
          this.setState({ loading: false })
          this.props.actions.openModal({ title: 'Ops...', subtitle: responseJson.message })
          break

        default:
          this.setState({ loading: false })
          this.props.actions.openModal({ title: 'Ops...', subtitle: 'Falha ao salvar. Tente novamente em alguns minutos.' })
      }
    } catch (e) {
      this.setState({ loading: false })
      this.props.actions.openModal({ title: 'Ops...', subtitle: 'Falha ao salvar. Tente novamente em alguns minutos.' })
    }
  }

  renderAnswerScreen = () => (
    <View style={ quizStyles.container }>
      <Image source={{ uri: this.backGroundImageUrl(this.props.post.currentPost.quizBackground) }} style={ quizStyles.image } />

      <View style={ quizStyles.contentContainer }>
        <View>
          <Text style={ quizStyles.questionTitle }>{ this.props.post.currentPost.questionTitle }</Text>
          <Text style={ quizStyles.description }>{ this.props.post.currentPost.questionDescription }</Text>
        </View>
        {
          this.state.loading ? (<View style={ quizStyles.answerContainer }><Loader/></View>) : (
            <View style={ quizStyles.answerContainer }>
              <OrrtaButton
                text={ this.props.post.currentPost.alternativeA.name.toUpperCase() }
                onPress={ () => this.handleUserAnswer(this.props.post.currentPost.alternativeA, this.props.post.currentPost.alternativeB) }/>

              <OrrtaButton
                text={ this.props.post.currentPost.alternativeB.name.toUpperCase() }
                onPress={ () => this.handleUserAnswer(this.props.post.currentPost.alternativeB, this.props.post.currentPost.alternativeA) }/>
            </View>
          )
        }
      </View>
    </View>
  )

  renderUserHasAlreadyAnsweredScreen = () => {
    return (
      <View style={ quizStyles.container }>
        <Image source={{ uri: this.backGroundImageUrl(this.props.post.currentPost.quizBackground) }} style={ quizStyles.image } />

        <View style={ quizStyles.contentContainer }>
          <View>
            <Text style={ quizStyles.responseTitle }>
              { this.props.post.currentPost.title }
            </Text>
            <View style={ quizStyles.yourResponseContainer }>
              <Text style={ quizStyles.yourResponsePrefix }>SUA RESPOSTA FOI: </Text>
              <Text style={ quizStyles.yourResponseText }>{ this.state.userAnswer.name.toUpperCase() }</Text>
            </View>
          </View>

          {
            this.state.loading && <View style={ quizStyles.answerContainer }><Loader/></View>
          }

          {
            !this.state.loading && this.props.post.currentPost.oneFeedback && (
              <View style={ quizStyles.answerContainer }>
                <OrrtaButton
                  text="REVER VÍDEO DA PERGUNTA"
                  onPress={ this.goToQuestionVideo }/>

                <OrrtaButton
                  text="REVER RESPOSTA DA JULIANA"
                  onPress={ this.goToAnwserFeedbackVideo }/>

              </View>
            )
          }

          {
            !this.state.loading && !this.props.post.currentPost.oneFeedback && (
              <View style={ quizStyles.answerContainer }>
                <OrrtaButton
                  text="REVER VÍDEO DA PERGUNTA"
                  onPress={ this.goToQuestionVideo }/>

                <OrrtaButton
                  text="REVER RESPOSTA DA JULIANA"
                  onPress={ this.goToAnwserFeedbackVideo }/>

                <OrrtaButton
                  outline={ true }
                  text="VER A OUTRA RESPOSTA DA JULIANA"
                  onPress={ this.goToOtherAlternativeVideo }/>
              </View>
            )
          }

        </View>
      </View>
    )
  }

  renderQuestionVideo = () => this.renderFullScreenVideo(this.props.post.currentPost.questionVideo, this.goToAnswerScreen)

  renderAnswerFeedbackVideo = () => this.renderFullScreenVideo(this.state.userAnswer.videoFeedback, this.goToUserHasAlreadyAnsweredScreen)

  renderOtherAlternativeVideo = () => this.renderFullScreenVideo(this.state.otherAlternative.videoFeedback, this.goToUserHasAlreadyAnsweredScreen)

  backGroundImageUrl = imageName => {
    const width = Dimensions.get('window').width
    const cloudinaryUrl = 'https://res.cloudinary.com'
    return `${cloudinaryUrl}/marbamedia/image/upload/c_scale,w_${width + 200}/${imageName}`
  }

  renderFullScreenVideo = (video, endCallback) => {
    if (!video || !!this.state.loading) {
      return <Loader color='#fff' />
    }
    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        <StatusBar hidden={true} />
        <Video
          loadingColor={ layoutSettings.colors.SECONDARY }
          ignoreSilentSwitch='ignore'
          autoplay={ true }
          disableFullscreen={ true }
          onEnd={ endCallback }
          vimeoId={ video.vimeoId }
          width={ width }
          height={ height }
          />
      </View>
    )
  }

  buildVideoFromVimeo = vimeoConfigData => ({
    "vimeo": true,
    "videoWidth": vimeoConfigData.video.width,
    "videoHeight": vimeoConfigData.video.height,
    "videoDuration": vimeoConfigData.video.duration,
    "videoUrl": vimeoConfigData.request.files.hls.cdns[vimeoConfigData.request.files.hls.default_cdn].url
  })

  componentDidMount = async () => {
    this.setState({ quizState: 'showingQuestionVideo' })
    this.userHasAlreadyAnsweredThisQuestion()
  }

  renderQuiz() {
    switch (this.state.quizState) {
      case 'showingQuestionVideo':
        return this.renderQuestionVideo()

      case 'showingAnwserScreen':
        return this.renderAnswerScreen()

      case 'showingAnwserFeedbackVideo':
        return this.renderAnswerFeedbackVideo()

      case 'showingOtherAlternativeVideo':
        return this.renderOtherAlternativeVideo()

      case 'userHasAlreadyAnswered':
        return this.renderUserHasAlreadyAnsweredScreen()
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        <Toast
          ref="toast"
          style={ toastStyles.container }
          position='top'
          positionValue={ 70 }
          fadeInDuration={ 750 }
          fadeOutDuration={ 1000 }/>
        { this.renderQuiz() }
      </View>
    )
  }
}

export default Quiz
