import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../../actions'

import { View } from 'react-native'

import MultiContent from './multi-content/MultiContent'
import Quiz from './quiz/Quiz'
import Exit from '../../common/Exit'

class TimelinePost extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Exit onExitPress={ () => { this.props.navigation.goBack() } } />
        {
          this.props.post.currentPost.type === 'multi-content' &&
          <MultiContent { ...this.props } />
        }
        {
          this.props.post.currentPost.type === 'quiz' &&
          <Quiz { ...this.props } />
        }
      </View>
    )
  }
}

const App = ({ post, actions, auth, navigation }) => (
  <TimelinePost
    auth={ auth }
    post={ post }
    actions={ actions }
    navigation={ navigation } />
)

App.propTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  post: state.post
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
