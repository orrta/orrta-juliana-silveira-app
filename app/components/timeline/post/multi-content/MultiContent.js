import React from 'react'

import { Text, View, TouchableWithoutFeedback, Linking } from 'react-native'
import { multiContentStyles } from '../../../../assets/styles/timelinePost'
import { toastStyles } from '../../../../assets/styles/toast'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Toast from 'react-native-easy-toast'
import Icon from 'react-native-vector-icons/Entypo'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons'

import ToastHearts from '../../../common/ToastHearts'
import Thumbnail from '../../../common/Thumbnail'
import PostCover from '../../PostCover'
import Loader from '../../../common/Loader'
import Comments from '../../../comments/Comments'
import MultiContentBody from './MultiContentBody'
import OwnerComment from '../../OwnerComment'
import HorizontalTimeline from '../../horizontal-timeline/HorizontalTimeline'

class MultiContent extends React.Component {

  TimelinePostNavigation = () => this.props.navigation.state.params.search ? 'SearchTimelinePost' : 'TimelinePost'

  state = { scrollEnabled: true, showScrollToTopButton: false, loadind: false }

  multiContentCardClickCallback = () => {
    this.setState({ ...this.state, loading: true })
    setTimeout(() => {
      this.scroll.scrollToPosition(0, 0)
      this.setState({ ...this.state, loading: false })
    }, 2000)
  }

  goToComments = () => this.scroll.scrollToEnd()

  handleScroll = (event) => {
    if (event.nativeEvent.contentOffset.y > 300 && !this.state.showScrollToTopButton) {
      this.setState({ showScrollToTopButton: true })
    } else if (event.nativeEvent.contentOffset.y <= 300 && this.state.showScrollToTopButton) {
      this.setState({ showScrollToTopButton: false })
    }
  }

  onSaveComment = () => {
    this.refs.toast && this.refs.toast.show(<ToastHearts heartsCount={ 5 } />, 3000)
  }

  render() {
    return (
      <View>
        <Toast
          ref="toast"
          style={ toastStyles.container }
          position='top'
          positionValue={ 70 }
          fadeInDuration={ 750 }
          fadeOutDuration={ 1000 }/>
        {
          this.state.showScrollToTopButton && (
            <TouchableWithoutFeedback onPress={ () => { this.scroll.scrollToPosition(0, 0) } }>
              <View style={ multiContentStyles.scrollToTopContainer }>
                <Icon style={ multiContentStyles.scrollToTopIcon } name='chevron-thin-up' />
              </View>
            </TouchableWithoutFeedback>
          )
        }
        <KeyboardAwareScrollView
          scrollEnabled={ this.state.scrollEnabled }
          onScroll={ this.handleScroll }
          ref={ (scroll) => { this.scroll = scroll } }
          contentContainerStyle={ multiContentStyles.container } >
          {
            this.state.loading ? (
              <Loader />
            ) : (
              <View style={ multiContentStyles.postContainer } >

                <PostCover
                  onCommentCountClick={ this.goToComments }
                  search={ this.props.navigation.state.params.search }
                  navigation={ this.props.navigation }
                  post={ this.props.post.currentPost }
                  onSlidingStart={ () => this.setState({ ...this.state, scrollEnabled: false }) }
                  onSlidingComplete={ () => this.setState({ ...this.state, scrollEnabled: true }) }
                  hideLock={ true }
                  hideClock={ true } />

                <Text style={ multiContentStyles.title }>{ this.props.post.currentPost.title }</Text>

                <View style={ multiContentStyles.divisor }></View>

                {
                  this.props.post.currentPost.ownerComment && (
                    <OwnerComment comment={ this.props.post.currentPost.ownerComment } />
                  )
                }

                <MultiContentBody content={ this.props.post.currentPost.content } />

                {
                  this.props.post.currentPost.credits && this.props.post.currentPost.credits.length > 0 && (
                    <View>
                      <View style={ multiContentStyles.divisorWithMargin }></View>
                      <Text style={ multiContentStyles.sectionTitle }>CRÉDITOS</Text>
                      {
                        this.props.post.currentPost.credits.map((c, i) => (
                          <View style={ multiContentStyles.creditItem } key={ i }>
                            <Thumbnail image={ c.imageUrl } style={ multiContentStyles.creditItemThumbnail } />
                            <View>
                              <Text style={ multiContentStyles.creditNameText }>{ c.name }</Text>
                              <View style={ multiContentStyles.creditLinks }>
                                {
                                  c.fb && (
                                    <TouchableWithoutFeedback onPress={ ()=> Linking.openURL(c.fb) }>
                                      <View style={ multiContentStyles.creditIconContainer }>
                                        <FAIcon name='facebook-square' style={ multiContentStyles.creditIcon }/>
                                      </View>
                                    </TouchableWithoutFeedback>
                                  )
                                }
                                {
                                  c.instagram && (
                                    <TouchableWithoutFeedback onPress={ ()=> Linking.openURL(c.instagram) }>
                                      <View style={ multiContentStyles.creditIconContainer }>
                                        <FAIcon name='instagram' style={ multiContentStyles.creditIcon }/>
                                      </View>
                                    </TouchableWithoutFeedback>
                                  )
                                }
                                {
                                  c.twitter && (
                                    <TouchableWithoutFeedback onPress={ ()=> Linking.openURL(c.twitter) }>
                                      <View style={ multiContentStyles.creditIconContainer }>
                                        <FAIcon name='twitter-square' style={ multiContentStyles.creditIcon }/>
                                      </View>
                                    </TouchableWithoutFeedback>
                                  )
                                }
                                {
                                  c.youtube && (
                                    <TouchableWithoutFeedback onPress={ ()=> Linking.openURL(c.youtube) }>
                                      <View style={ multiContentStyles.creditIconContainer }>
                                        <FAIcon name='youtube-play' style={ multiContentStyles.creditIcon }/>
                                      </View>
                                    </TouchableWithoutFeedback>
                                  )
                                }
                                {
                                  c.webSite && (
                                    <TouchableWithoutFeedback onPress={ ()=> Linking.openURL(c.webSite) }>
                                      <View style={ multiContentStyles.creditIconContainer }>
                                        <MCIcon name='web' style={ multiContentStyles.creditIcon }/>
                                      </View>
                                    </TouchableWithoutFeedback>
                                  )
                                }
                              </View>
                            </View>
                          </View>
                        ))
                      }
                    </View>
                  )
                }

                {
                  this.props.post.currentPost.musics &&
                  this.props.post.currentPost.musics.length > 0 && (
                    <View style={ multiContentStyles.creditsContainer }>
                      <View style={ multiContentStyles.divisorWithMargin }></View>
                      <Text style={ multiContentStyles.sectionTitle }>MÚSICAS</Text>
                      {
                        this.props.post.currentPost.musics.map((m, i) => (
                          <Text style={ multiContentStyles.creditsMusicText } key={ i }>{ m }</Text>
                        ))
                      }
                    </View>
                  )
                }

                <View style={ multiContentStyles.divisorWithMargin }></View>

                <Text style={ multiContentStyles.sectionTitle }>COMENTÁRIOS</Text>

                <Comments
                  inline={ true }
                  navigation={ this.props.navigation }
                  onSaveComment={ this.onSaveComment } />

                <View style={ multiContentStyles.divisorWithMargin }></View>

                <View style={ multiContentStyles.horizontalTimeline }>
                  <HorizontalTimeline
                    title='RELACIONADOS'
                    currentPostId={ this.props.post.currentPost.id }
                    navigation={ this.props.navigation }
                    timelinePostNavigation={ this.TimelinePostNavigation() }
                    multiContentCardClickCallback={ this.multiContentCardClickCallback }
                    index={ this.props.post.currentPost.section }
                    search={ this.props.navigation.state.params.search }/>
                </View>
              </View>
            )
          }
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

export default MultiContent
