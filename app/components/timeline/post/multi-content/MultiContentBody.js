import React from 'react'

import { Text, View, Dimensions } from 'react-native'
import { CachedImage as Image } from 'react-native-cached-image'

import Video from '../../../common/Video'
import layoutSettings from '../../../../common/settings/layoutSettings'
import { multiContentStyles } from '../../../../assets/styles/timelinePost'

import Swiper from 'react-native-swiper'

const width = Dimensions.get('window').width

const PaginationDot = props => (
  <View style={{
    backgroundColor: props.active ? layoutSettings.colors.TERTIARY : layoutSettings.colors.SECONDARY,
    borderWidth: 1,
    borderColor: props.active ? layoutSettings.colors.SECONDARY : layoutSettings.colors.TERTIARY,
    width: props.active ? 11 : 9,
    height: props.active ? 11 : 9,
    borderRadius: props.active ? 6 : 5,
    marginHorizontal: 3
  }} />
)

const MultiContentBody = props => (
  <View style={ multiContentStyles.postbody }>
    {
      props.content.map((c, i) => {
        if (c.type === 'text') {
          return (
            <View key={ i } style={ multiContentStyles.body } >
              <Text style={ multiContentStyles.textBody } >{ c.value }</Text>
            </View>
          )
        }
        if (c.type === 'textArray') {
          return (
            <View key={ i } style={ multiContentStyles.body } >
              { c.value.map((t, i) => <Text style={ multiContentStyles.textBody } key={ i }>{ t }</Text>) }
            </View>
          )
        }
        if (c.type === 'photoAlbum') {
          let biggerPhotoHeight = 200

          for (photo of c.albumPhotos) {
            const height = (width * photo.height) / photo.width

            if (height > biggerPhotoHeight) {
              biggerPhotoHeight = height
            }
          }

          return (
            <Swiper
              key={ i }
              loop={ false }
              showsButtons={ false }
              showsPagination={ true }
              paginationStyle={{ top: 30, bottom: null }}
              dot={ <PaginationDot /> }
              activeDot={ <PaginationDot active={ true } /> }
              style={{ backgroundColor: '#000', height: biggerPhotoHeight, marginTop: 20 }}>
              {
                c.albumPhotos.map((photo, photoIndex) => {
                  const height = (width * photo.height) / photo.width

                  const marginTop = (biggerPhotoHeight - height) / 4

                  const cloudinaryUrl = 'https://res.cloudinary.com'
                  const watherMark = `w_${width + 100},g_south,l_watermark`

                  const url = `${cloudinaryUrl}/marbamedia/image/upload/c_scale,w_${width + 100}/${watherMark}/${photo.name}`

                  return <Image source={ { uri: url } } key={ `${i}-${photoIndex}` } style={{ height, marginTop, width: null }} />
                })
              }
            </Swiper>
          );
        }
        if (c.type === 'image') {
          const height = Math.floor((width * c.imageData.height) / c.imageData.width)

          const cloudinaryUrl = 'https://res.cloudinary.com'
          const watherMark = `w_${width + 100},g_south,l_watermark`

          const url = `${cloudinaryUrl}/marbamedia/image/upload/c_scale,w_${width + 100}/${watherMark}/${c.imageData.name}`

          return <Image source={ { uri: url } } key={ i } style={{ flex: 1, height, width: null }} />
        }
        if (c.type === 'video') {
          return <Video key={ i } vimeoId={ c.videoData.vimeoId } />
        }
      })
    }
  </View>
)

export default MultiContentBody
