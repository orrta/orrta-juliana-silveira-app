import React from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../../actions'

import { Text, View, ListView, RefreshControl } from 'react-native'

import layoutSettings from '../../common/settings/layoutSettings'
import Loader from '../common/Loader'
import TimelineCard from './card/TimelineCard'
import { timelineStyles } from '../../assets/styles/timeline'

class Timeline extends React.Component {

  currentPage = 0

  state = {
    scrollEnabled: true,
    dataSource: new ListView.DataSource({
      rowHasChanged: (r1, r2) => JSON.stringify(r1) !== JSON.stringify(r2),
    })
  }

  componentDidMount() {
    !this.props.search && this.refresh()

    if (this.props.auth.loggedUser.newUser) {
      const tutorialShown = this.props.auth.loggedUser.tutorialsShown.find(t => t === 'profile-button')

      if (!tutorialShown) {
        setTimeout(() => {
          this.props.actions.openModal({
            title: 'Seus Corações',
            image: 'profile-button',
            body: 'Aqui você pode ver os corações que você possui. Clicando sobre eles, você vai para o seu perfil. Comente nas postagens e interaja com o aplicativo para ganhar corações!',
            buttons: [{
              text: 'Entendido!',
              onPress: () => this.props.actions.setTutorialAsShown('profile-button')
            }]
          })
        }, 1000)
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ...this.state,
      dataSource: this.getUpdatedDataSource(nextProps),
    })
  }

  getUpdatedDataSource = (props) => {
    let rows = props.search ? props.timeline.searchResults.items : props.timeline.items
    let ids = rows.map((obj, index) => index)
    return this.state.dataSource.cloneWithRows(rows, ids)
  }

  onSlidingStart = () => {
    this.setState({ ...this.state, scrollEnabled: false })
  }

  onSlidingComplete = () => {
    this.setState({ ...this.state, scrollEnabled: true })
  }

  renderRefreshControl = () => {
    return (
      <RefreshControl
        tintColor={layoutSettings.colors.TERTIARY}
        refreshing={ this.props.timeline.loading }
        onRefresh={ this.refresh }
      />
    )
  }

  refresh = () => {
    this.currentPage = 0
    this.props.actions.loadTimeline()
  }

  loadMoreContent = () => {
    if (
      !this.props.search &&
      !this.props.timeline.loading &&
      this.props.timeline.total > 0 &&
      this.props.timeline.total > this.props.timeline.items.length
    ) {
      this.currentPage ++
      this.props.actions.paginateTimeline(this.currentPage)
    }
  }

  onCardPress = post => this.props.actions.openPost(post, () => {
    this.props.navigation.navigate(this.props.timelinePostNavigation, {
      tabBarVisible: false,
      search: this.props.search
    })
  })

  onCommentsPress = post => this.props.actions.openPost(post, () => {
    this.props.navigation.navigate(this.props.timelineCommentsNavigation, {
      tabBarVisible: false,
      search: this.props.search
    })
  })

  render() {
    return (
      <View style={ timelineStyles.body }>
        {
          this.props.search && this.props.timeline.searchResults.loading ? (
            <Loader />
          ) : (
            <View>
              <ListView
                scrollEnabled={ this.state.scrollEnabled }
                dataSource={ this.state.dataSource }
                renderRow={ row => {
                  return (
                    <TimelineCard
                      onCardClick={ this.onCardPress }
                      onCommentsPress={ this.onCommentsPress }
                      post={ row }
                      navigation={ this.props.navigation }
                      timelinePostNavigation={ this.props.timelinePostNavigation }
                      search={ this.props.search }
                      sendHearts={ this.props.actions.sendHearts }
                      onSlidingStart={ this.onSlidingStart }
                      onSlidingComplete={ this.onSlidingComplete } />
                  )
                }}
                enableEmptySections={true}
                onEndReached={ this.loadMoreContent }
                renderFooter={ () => (!this.props.search && this.props.timeline.loading && <Loader />) }
                refreshControl={ this.renderRefreshControl() }
              />
            </View>
          )
        }
      </View>
    )
  }
}

const TimelineApp = ({ auth, timeline, actions, navigation, timelinePostNavigation, search, onSlidingStart, onSlidingComplete, timelineCommentsNavigation }) => (
  <Timeline
    auth={ auth }
    timeline={ timeline }
    actions={ actions }
    navigation={ navigation }
    search={ search }
    onSlidingStart={ onSlidingStart }
    onSlidingComplete={ onSlidingComplete }
    timelinePostNavigation={ timelinePostNavigation || 'TimelinePost' }
    timelineCommentsNavigation={ timelineCommentsNavigation || 'TimelineComments' } />
)

TimelineApp.propTypes = {
  auth: PropTypes.object.isRequired,
  timeline: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  timeline: state.timeline,
  auth: state.auth
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimelineApp)
