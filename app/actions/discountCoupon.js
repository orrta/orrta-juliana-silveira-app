import {
  SAVING_DISCOUNT_COUPON,
  SUCCESS_TO_SAVE_DISCOUNT_COUPON,
  ERROR_TO_SAVE_DISCOUNT_COUPON
} from '../constants/ActionTypes'

import apisSettings from '../common/settings/apisSettings'

export const sendDiscountCoupon = (discountCoupon, successCallback, errorCallback) => {
  return dispatch => {
    dispatch({ type: SAVING_DISCOUNT_COUPON, discountCoupon })

    const url = `${apisSettings.USER_ITERATION.URL}/discount-coupon/v2`

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(discountCoupon),
      headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY }
    }).then(response => {
      switch (response.status) {
        case 200:
          dispatch({ type: SUCCESS_TO_SAVE_DISCOUNT_COUPON, discountCoupon })
          successCallback()
          break;

        case 400:
          response.json().then(responseJson => {
            dispatch({ type: ERROR_TO_SAVE_DISCOUNT_COUPON, error: responseJson.message, discountCoupon })
            errorCallback(responseJson.message)
          })
          break;

        default:
          dispatch({
            type: ERROR_TO_SAVE_DISCOUNT_COUPON,
            error: 'Falha ao enviar. Tente novamente em alguns minutos.',
            discountCoupon
          })
          errorCallback('Falha ao enviar. Tente novamente em alguns minutos.')
      }
    }, error => {
      dispatch({
        type: ERROR_TO_SAVE_DISCOUNT_COUPON,
        error: 'Falha ao enviar. Tente novamente em alguns minutos.',
        discountCoupon
      })
      errorCallback('Falha ao enviar. Tente novamente em alguns minutos.')
    })
  }
}
