import {
  UPDATE_USER_HEARTS,
  SUCCESS_TO_LOAD_USER,
  SUCCESS_TO_SAVE_USER,
  ERROR_TO_AUTHENTICATE,
  LOGIN_LOADING,
  LOGIN_CANCELED,
  LOGOUT,
  SET_USER_AS_SUBSCRIBER,
  SET_ANONIMOUS_LOGIN,
  UPDATE_MOBILE_SETTINGS
} from '../constants/ActionTypes'

import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from 'react-native-fbsdk'

import { GoogleSignin } from 'react-native-google-signin';

import FCM, {
  FCMEvent,
  NotificationType,
  WillPresentNotificationResult,
  RemoteNotificationResult
} from 'react-native-fcm'

import { Platform, NetInfo } from 'react-native'

import apisSettings from '../common/settings/apisSettings'


GoogleSignin.configure({})


const _listenNotifications = async (dispatch) => {
    FCM.requestPermissions()

    const token = await FCM.getFCMToken()

    FCM.subscribeToTopic('general')

    FCM.on(FCMEvent.Notification, async (notif) => {
      if (Platform.OS === 'ios') {
        switch (notif._notificationType) {
          case NotificationType.Remote:
            notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
            break
          case NotificationType.NotificationResponse:
            notif.finish()
            break
          case NotificationType.WillPresent:
            notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
            break
        }
      }
    })

    return token
}

import { s4 } from '../common/helpers/uuid'

const _getFacebookToken = AccessToken.getCurrentAccessToken

const _getFacebookUser = async (accessToken) => {
  return new Promise((resolve, reject) => {
    const infoRequest = new GraphRequest(
      '/me',
      {
        accessToken: accessToken,
        parameters: {
          fields: {
            string: 'email,name,first_name,middle_name,last_name'
          }
        }
      },
      (error, user) => error ? reject(error) : resolve(user)
    )
    new GraphRequestManager().addRequest(infoRequest).start()
  })
}

const _getOrrtaUser = async (userId) => {
  let response = await fetch(
    `${apisSettings.AGGREGATION.URL}/user/${userId}`,
    { headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY } }
  )

  if (response.status === 404) {
    return null
  }

  if (response.status === 500) {
    json = await response.json()
    throw json
  }

  if (response.ok) {
    return response.json()
  }

  throw { orrtaMessage: 'Houve um probleminha ao fazer o login, tente novamente em alguns minutos' }
}

const _saveOrrtaUser = async (user) => {
  let url = `${apisSettings.USER_ITERATION.URL}/user`

  let response = await fetch(url, {
    method: 'POST',
    headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY },
    body: JSON.stringify(user)
  })

  if (response.ok) {
    return { ...user, available_hearts: 100, sent_hearts: 0 }
  }

  throw { orrtaMessage: 'Houve um probleminha ao fazer o login, tente novamente em alguns minutos' }
}

const _generateUserSharingCode = (firstName, lastName) => {
  return `${firstName.replace(" ", "")}-${lastName.replace(" ", "").slice(0, 1)}-${s4()}`.toUpperCase()
}

const _saveNewUser = async (user, callback, dispatch) => {
  try {
    let sharingCode = _generateUserSharingCode(user.first_name || user.givenName, user.last_name || user.familyName)
    let newUser = await _saveOrrtaUser({ ...user, sharingCode })
    callback(newUser)
    dispatch({ type: SUCCESS_TO_SAVE_USER, user: newUser })
  } catch (e) {
    throw e
  }
}

_haveToUpdateUser = (fbUser, esUser) => {
  const propertiesToCompare = [
    'email',
    'name',
    'photo',
    'fcmToken'
  ]

  for (property of propertiesToCompare) {

    if (fbUser[property] !== esUser[property]) {
      return true
    }
  }

  return false
}

const _updateUser = async (user, responseJson, callback, dispatch) => {
  dispatch({ type: SUCCESS_TO_LOAD_USER, user: responseJson.es._source })

  if (_haveToUpdateUser(user, responseJson.es._source)) {
    await _saveOrrtaUser(user)
  }

  callback({ ...responseJson.es._source, ...user})
}

const _loadLoggedUser = async (dispatch, callbackNewUser, callbackLogin, callbackError) => {
  try {
    const fcmToken = await _listenNotifications(dispatch)

    let fbData = await _getFacebookToken()

    if (fbData) {
      let user = await _getFacebookUser(fbData.accessToken)
      let responseJson = await _getOrrtaUser(user.id)

      dispatch({ type: UPDATE_MOBILE_SETTINGS, settings: responseJson.settings })

      user.fcmToken = fcmToken
      user.authType = 'fb'

      user.photo = user.photo || 'https://res.cloudinary.com/marbamedia/image/upload/c_scale,w_150/v1541460375/App/user.jpg'

      if (responseJson && responseJson.es && responseJson.es.found) {
        await _updateUser(user, responseJson, callbackLogin, dispatch)
      } else {
        await _saveNewUser(user, callbackNewUser, dispatch)
      }
    } else {
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true })

      const isSignedIn = await GoogleSignin.isSignedIn()

      const googleData = null

      if (isSignedIn) {
         googleData = await GoogleSignin.signInSilently()
      }

      if (googleData) {
        let user = googleData.user
        let responseJson = await _getOrrtaUser(user.id)

        dispatch({ type: UPDATE_MOBILE_SETTINGS, settings: responseJson.settings })

        user.fcmToken = fcmToken
        user.authType = 'google'

        user.photo = user.photo || 'https://res.cloudinary.com/marbamedia/image/upload/c_scale,w_150/v1541460375/App/user.jpg'

        if (responseJson && responseJson.es && responseJson.es.found) {
          await _updateUser(user, responseJson, callbackLogin, dispatch)
        } else {
          await _saveNewUser(user, callbackNewUser, dispatch)
        }
      } else {
        dispatch({ type: ERROR_TO_AUTHENTICATE })
      }
    }
  } catch (error) {
    dispatch({ type: ERROR_TO_AUTHENTICATE, error })
    callbackError(error)
  }
}

export const loginWithFacebook = (callbackNewUser, callbackLogin, callbackError) => {
  return dispatch => {
    dispatch({ type: LOGIN_LOADING })

    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        dispatch({ type: LOGIN_CANCELED })
        callbackError({ 'orrtaMessage': 'Você não está conectado com a internet. O aplicativo precisa de internet para ser utilizado!' })
      } else {
        LoginManager.logInWithReadPermissions(['public_profile']).then(result => {
          if (result.isCancelled) {
            dispatch({ type: LOGIN_CANCELED })
          } else {
            return _loadLoggedUser(dispatch, callbackNewUser, callbackLogin, callbackError)
          }
        })
      }
    })
  }
}

const googleLogin = async () => {
  await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true })
  const usuario = await GoogleSignin.signIn()
  return usuario
}

export const loginWithGoogle = (callbackNewUser, callbackLogin, callbackError) => {
  return dispatch => {
    dispatch({ type: LOGIN_LOADING })

    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        dispatch({ type: LOGIN_CANCELED })
        callbackError({ 'orrtaMessage': 'Você não está conectado com a internet. O aplicativo precisa de internet para ser utilizado!' })
      } else {

        GoogleSignin.isSignedIn().then(isSignedIn => {
          if (isSignedIn) {
            GoogleSignin.revokeAccess().then(() => {
              GoogleSignin.signOut().then(() => {
                googleLogin().then(data => {
                  return _loadLoggedUser(dispatch, callbackNewUser, callbackLogin, callbackError)
                }, error => {
                  dispatch({ type: LOGIN_CANCELED })
                })
              })
            })
          } else {
            googleLogin().then(data => {
              return _loadLoggedUser(dispatch, callbackNewUser, callbackLogin, callbackError)
            }, error => {
              dispatch({ type: LOGIN_CANCELED })
            })
          }
        })
      }
    })
  }
}

export const logout = (authType, callback) => {
  return dispatch => {
    dispatch({ type: LOGOUT })

    if (authType === 'google') {
      GoogleSignin.revokeAccess().then(() => {
        GoogleSignin.signOut()
      })
    } else {
      LoginManager.logOut()
    }

    callback()
  }
}

export const setUserAsSubscriber = () => {
  return dispatch => {
    dispatch({ type: SET_USER_AS_SUBSCRIBER })
  }
}

export const loadLoggedUser = (callbackNewUser, callbackLogin, callbackError) => {
  return dispatch => {
    return _loadLoggedUser(dispatch, callbackNewUser, callbackLogin, callbackError)
  }
}

export const enterWithoutLogin = (callbackLogin) => {
  return dispatch => {
    dispatch({ type: SET_ANONIMOUS_LOGIN })
    callbackLogin()
  }
}

export const updateUserHearts = heartsCount => {
  return dispatch => {
    dispatch({ type: UPDATE_USER_HEARTS, heartsCount })
  }
}
