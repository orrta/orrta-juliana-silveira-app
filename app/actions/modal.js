import {
  CLOSE_MODAL,
  OPEN_MODAL,
  SET_TUTORIAL_AS_SHOWN
} from '../constants/ActionTypes'


export const openModal = ({ title, subtitle, body, image, buttons = [] }) => {
  return dispatch => {
    dispatch({ type: OPEN_MODAL, modalData: { title, subtitle, body, image, buttons } })
  }
}

export const setTutorialAsShown = tutorialName => {
  return dispatch => {
    dispatch({ type: SET_TUTORIAL_AS_SHOWN, tutorialName })
  }
}

export const closeModal = () => {
  return dispatch => {
    dispatch({ type: CLOSE_MODAL })
  }
}
