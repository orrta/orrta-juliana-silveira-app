import {
  LOADING_CATEGORIES,
  SUCCESS_TO_LOAD_CATEGORIES,
  ERROR_TO_LOAD_CATEGORIES,
  NO_CATEGORIES
} from '../constants/ActionTypes'

import apisSettings from '../common/settings/apisSettings'

export const loadCategories = () => {
  return dispatch => {
    dispatch({ type: LOADING_CATEGORIES })

    const url = `${apisSettings.AGGREGATION.URL}/content_types/_search`

    fetch(url, {
      method: 'GET',
      headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY }
    }).then(response => {
      response.json().then(responseJson => {
        if (responseJson.es && responseJson.es.hits) {
          categories = responseJson.es.hits.hits.map(hit => {
            let result = hit._source
            result.id = hit._id
            return result
          })
          dispatch({ type: SUCCESS_TO_LOAD_CATEGORIES, categories })
        } else {
          dispatch({ type: NO_CATEGORIES })
        }
      }, error => dispatch({ type: ERROR_TO_LOAD_CATEGORIES, error }))
    }, error => dispatch({ type: ERROR_TO_LOAD_CATEGORIES, error }))
  }
}
