import * as authActions from './auth'
import * as postActions from './post'
import * as modalActions from './modal'
import * as timelineActions from './timeline'
import * as categoriesActions from './categories'
import * as discountCouponActions from './discountCoupon'
import * as horizontalTimelineActions from './horizontalTimeline'


export const setUserAsSubscriber = authActions.setUserAsSubscriber
export const enterWithoutLogin = authActions.enterWithoutLogin
export const loginWithFacebook = authActions.loginWithFacebook
export const loginWithGoogle = authActions.loginWithGoogle
export const updateUserHearts = authActions.updateUserHearts
export const loadLoggedUser = authActions.loadLoggedUser
export const logout = authActions.logout

export const openPost = postActions.openPost
export const loadLastComments = postActions.loadLastComments
export const saveComment = postActions.saveComment
export const deleteComment = postActions.deleteComment

export const sendHearts = timelineActions.sendHearts
export const loadTimeline = timelineActions.loadTimeline
export const paginateTimeline = timelineActions.paginateTimeline
export const updateRanking = timelineActions.updateRanking

export const openModal = modalActions.openModal
export const closeModal = modalActions.closeModal
export const setTutorialAsShown = modalActions.setTutorialAsShown

export const loadCategories = categoriesActions.loadCategories

export const sendDiscountCoupon = discountCouponActions.sendDiscountCoupon

export const loadHorizontalTimeline = horizontalTimelineActions.loadHorizontalTimeline
