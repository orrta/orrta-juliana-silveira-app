import {
  OPEN_POST,
  LOADING_COMMENTS,
  COMMENTS_LOADED,
  NO_COMMENTS,
  ERROR_TO_LOAD_COMMENTS,
  SAVING_COMMENT,
  SUCCESS_TO_SAVE_COMMENT,
  ERROR_TO_SAVE_COMMENT,
  DELETING_COMMENT,
  SUCCESS_TO_DELETE_COMMENT,
  ERROR_TO_DELETE_COMMENT
} from '../constants/ActionTypes'

import { guid } from '../common/helpers/uuid'
import { openModal } from './modal'
import apisSettings from '../common/settings/apisSettings'

const _loadLastComments = async (postId, page = 0, dispatch) => {
  dispatch({ type: LOADING_COMMENTS })

  let size = 4
  let from = page * size

  if (from >= size) {
    from = from - size / 2
    size = size + size / 2
  }

  const url = `${apisSettings.AGGREGATION.URL}/search?query_name=comments-default&index=comments&from=${from}&size=${size}&post_id=${postId}&include=comment-user-picture`

  try {
    let response = await fetch(url, { headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY } })
    let responseJson = await response.json()

    if (responseJson.es && responseJson.es.hits) {
      comments = responseJson.es.hits.hits.map(hit => {
        let result = hit._source
        result.id = hit._id
        return result
      })
      dispatch({ type: COMMENTS_LOADED, comments, page, total: responseJson.es.hits.total, postId })
    } else {
      dispatch({ type: NO_COMMENTS, page })
    }
  } catch (error) {
    dispatch({ type: ERROR_TO_LOAD_COMMENTS, error, page })
  }
}

export const loadLastComments = (postId, page = 0) => {
  return dispatch => {
    _loadLastComments(postId, page, dispatch)
  }
}

export const saveComment = (comment, successCallback) => {
  return dispatch => {
    const alertError = () => {
      dispatch(openModal({
        title: 'Ops...',
        subtitle: 'Não conseguimos salvar o comentário, tente novamente em alguns minutos'
      }))
      dispatch({ type: ERROR_TO_SAVE_COMMENT, comment })
    }

    comment.id = `${comment.postId}:${comment.userId}:${guid()}`

    dispatch({ type: SAVING_COMMENT, comment })

    const url = `${apisSettings.USER_ITERATION.URL}/comment`

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(comment),
      headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY }
    }).then(response => {
      if (response.ok) {
        dispatch({ type: SUCCESS_TO_SAVE_COMMENT, comment })
        successCallback && successCallback()
      } else {
        alertError()
      }
    }, alertError)

  }
}

export const deleteComment = (comment) => {
  return dispatch => {
    dispatch({ type: DELETING_COMMENT, comment })

    const url = `${apisSettings.USER_ITERATION.URL}/comment`

    fetch(url, {
      method: 'DELETE',
      body: JSON.stringify(comment),
      headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY }
    }).then(response => {
      dispatch({ type: SUCCESS_TO_DELETE_COMMENT, comment })
    }, error => dispatch({ type: ERROR_TO_DELETE_COMMENT, error, comment }))
  }
}

export const openPost = (post, callback) => {
  return dispatch => {
    dispatch({ type: OPEN_POST, post })
    callback()
    _loadLastComments(post.id, 0, dispatch)
  }
}
