import {
  SEND_HEARTS_LOADING,
  SUCCESS_TO_SEND_HEARTS,
  ERROR_TO_SEND_HEARTS,
  SUCCESS_TO_LOAD_TIMELINE,
  SUCCESS_TO_PAGINATE_TIMELINE,
  TIMELINE_LOADED_WITHOUT_RESULTS,
  ERROR_TO_LOAD_TIMELINE,
  OPEN_POST,
  LOADING_TIMELINE,
  SUCCESS_TO_UPDATE_RANKING
} from '../constants/ActionTypes'

import apisSettings from '../common/settings/apisSettings'

export const updateRanking = (ranking, heartsSum, postId) => {
  return dispatch => {

    rankingCopy = (ranking || []).slice()

    if ((rankingCopy || []).length > 5) {
      rankingCopy = rankingCopy.slice(0, 5)
    }

    dispatch({ type: SUCCESS_TO_UPDATE_RANKING, ranking: rankingCopy, heartsSum, postId })
  }
}

export const sendHearts = (sendHeartsDto, callback, callbackError) => {
  return dispatch => {
    const alertError = () => {
      callbackError()
      dispatch({ type: ERROR_TO_SEND_HEARTS })
    }

    dispatch({ type: SEND_HEARTS_LOADING })

    const url = `${apisSettings.USER_ITERATION.URL}/send-hearts`

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(sendHeartsDto),
      headers: { Authorization: apisSettings.USER_ITERATION.AUTH_KEY }
    }).then(response => {
      if (response.ok) {
        callback()
        dispatch({ type: SUCCESS_TO_SEND_HEARTS, sendHeartsDto })
      } else {
        alertError()
      }
    }, alertError)
  }
}

export const loadTimeline = (queryName = 'timeline-default', index = 'timeline--all', searchTerm = null, includeRanking = true, page = 0) => {
  const isSearch = index !== 'timeline--all'
  const size = 15

  return dispatch => {
    dispatch({ type: LOADING_TIMELINE, isSearch })

    let url = `${apisSettings.AGGREGATION.URL}/search?index=${index}&query_name=${queryName}&from=${page * size}&size=${size}`

    if (includeRanking) {
      url = `${url}&include=ranking`
    }

    if (searchTerm) {
      url = `${url}&search_term=${searchTerm}`
    }

    fetch(url, { headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY } }).then(
      response => {
        
        response.json().then(
          responseJson => {
            if (responseJson.es && responseJson.es.hits) {
              const items = responseJson.es.hits.hits.map(hit => {
                let result = hit._source
                result.id = hit._id
                result.section = hit._index
                return result
              })
              const total = responseJson.es.hits.total
              if (page > 0) {
                dispatch({ type: SUCCESS_TO_PAGINATE_TIMELINE, items, total, isSearch })
              } else {
                dispatch({ type: SUCCESS_TO_LOAD_TIMELINE, items, total, isSearch })
              }
            } else {
              dispatch({ type: TIMELINE_LOADED_WITHOUT_RESULTS, isSearch })
            }
          },
          error => dispatch({ type: ERROR_TO_LOAD_TIMELINE, error, isSearch })
        )
      },
      error => dispatch({ type: ERROR_TO_LOAD_TIMELINE, error, isSearch })
    )
  }
}

export const paginateTimeline = (page = 0) => {
  return loadTimeline('timeline-default', 'timeline--all', null, true, page)
}
