import {
  LOADING_HORIZONTAL_TIMELINE,
  SUCCESS_TO_LOAD_HORIZONTAL_TIMELINE,
  ERROR_TO_LOAD_HORIZONTAL_TIMELINE
} from '../constants/ActionTypes'

import apisSettings from '../common/settings/apisSettings'

const _loadHorizontalTimeline = async (dispatch, horizontalTimelineId, indexName, queryName) => {
  dispatch({ type: LOADING_HORIZONTAL_TIMELINE, horizontalTimelineId })

  const url = `${apisSettings.AGGREGATION.URL}/search?index=${indexName}&query_name=${queryName}&include=ranking`

  const response = await fetch(url, { headers: { 'X-App-Authorization': apisSettings.AGGREGATION.AUTH_KEY } })

  if (response.ok) {
    const responseJson = await response.json()

    if (responseJson.es && responseJson.es.hits) {
      const items = responseJson.es.hits.hits.map(hit => {
        let result = hit._source
        result.id = hit._id
        result.section = hit._index
        return result
      })
      dispatch({ type: SUCCESS_TO_LOAD_HORIZONTAL_TIMELINE, horizontalTimelineId, items })
    } else {
      dispatch({ type: SUCCESS_TO_LOAD_HORIZONTAL_TIMELINE, horizontalTimelineId, items: [] })
    }
  } else {
    dispatch({ type: ERROR_TO_LOAD_HORIZONTAL_TIMELINE, horizontalTimelineId })
  }
}

export const loadHorizontalTimeline = (horizontalTimelineId, indexName, queryName) => {
  return dispatch => _loadHorizontalTimeline(dispatch, horizontalTimelineId, indexName, queryName)
}
