import { combineReducers } from 'redux'

import appSettings from './appSettings'
import auth from './auth'
import categories from './categories'
import horizontalTimeline from './horizontalTimeline'
import modal from './modal'
import post from './post'
import timeline from './timeline'


const rootReducer = combineReducers({
  appSettings,
  auth,
  categories,
  horizontalTimeline,
  modal,
  post,
  timeline,
})

export default rootReducer
