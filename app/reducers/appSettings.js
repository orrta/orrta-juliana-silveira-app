import {
  UPDATE_MOBILE_SETTINGS
} from '../constants/ActionTypes'


const initialState = {
}


export default modal = (state = initialState, action) => {
  switch (action.type) {

    case UPDATE_MOBILE_SETTINGS:
      console.log(action);
      return { ...action.settings }

    default:
      return state
  }
}
