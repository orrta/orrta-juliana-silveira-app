import {
  CLOSE_MODAL,
  OPEN_MODAL
} from '../constants/ActionTypes'


const initialState = {
  open: false,
  title: null,
  subtitle: null,
  buttons: [],
}


export default modal = (state = initialState, action) => {
  switch (action.type) {

    case CLOSE_MODAL:
      return { ...state, open: false }

    case OPEN_MODAL:
      return { ...state, ...action.modalData, open: true }

    default:
      return state
  }
}
