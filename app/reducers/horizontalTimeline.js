import {
  LOADING_HORIZONTAL_TIMELINE,
  SUCCESS_TO_LOAD_HORIZONTAL_TIMELINE,
  ERROR_TO_LOAD_HORIZONTAL_TIMELINE,
  SUCCESS_TO_DELETE_COMMENT,
  SUCCESS_TO_SAVE_COMMENT
} from '../constants/ActionTypes'


const initialState = {
  timelines: []
}


const getTimeline = (timelineId, timelines) => timelines.find(t => t.id === timelineId)


export default HorizontalTimeline = (state = initialState, action) => {
  switch (action.type) {

    case SUCCESS_TO_SAVE_COMMENT:
      return {
        timelines: state.timelines.map(t => ({
          ...t,
          items: t.items.map(i => ({
            ...i,
            comments_count: (i.comments_count || 0) + 1
          }))
        }))
      }

    case SUCCESS_TO_DELETE_COMMENT:
      return {
        timelines: state.timelines.map(t => ({
          ...t,
          items: t.items.map(i => ({
            ...i,
            comments_count: (i.comments_count || 0) - 1
          }))
        }))
      }

    case LOADING_HORIZONTAL_TIMELINE:
      return {
        timelines: [
          ...state.timelines,
          {
            id: action.horizontalTimelineId,
            loading: true
          }
        ]
      }

    case SUCCESS_TO_LOAD_HORIZONTAL_TIMELINE:
      return {
        timelines: state.timelines.map(timeline => {
          if (timeline.id === action.horizontalTimelineId) {
            return {
              ...timeline,
              loading: false,
              items: action.items
            }
          }
          return timeline
        })
      }

    case ERROR_TO_LOAD_HORIZONTAL_TIMELINE:
      return {
        timelines: state.timelines.map(timeline => {
          if (timeline.id === action.horizontalTimelineId) {
            return {
              ...timeline,
              loading: false,
              error: true
            }
          }
          return timeline
        })
      }

    default:
      return state
  }
}
