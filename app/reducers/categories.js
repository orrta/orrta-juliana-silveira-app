import {
  LOADING_CATEGORIES,
  SUCCESS_TO_LOAD_CATEGORIES,
  ERROR_TO_LOAD_CATEGORIES,
  NO_CATEGORIES
} from '../constants/ActionTypes'

const initialState = {
  loading: true,
  items: [],
}

export default post = (state = initialState, action) => {
  switch (action.type) {

    case LOADING_CATEGORIES:
      return { ...state, loading: true }

    case SUCCESS_TO_LOAD_CATEGORIES:
      return { ...state, loading: false, items: action.categories }

    case ERROR_TO_LOAD_CATEGORIES:
      return { ...state, loading: false, items: [] }

    case NO_CATEGORIES:
      return { ...state, loading: false, items: [] }

    default:
      return state
  }
}
