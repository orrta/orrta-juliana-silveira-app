import {
  UPDATE_USER_HEARTS,
  SUCCESS_TO_LOAD_USER,
  SUCCESS_TO_SAVE_USER,
  ERROR_TO_AUTHENTICATE,
  LOGIN_LOADING,
  LOGIN_CANCELED,
  LOGOUT,
  SUCCESS_TO_SEND_HEARTS,
  SUCCESS_TO_SAVE_DISCOUNT_COUPON,
  SUCCESS_TO_SAVE_COMMENT,
  SET_USER_AS_SUBSCRIBER,
  SET_ANONIMOUS_LOGIN,
  SET_TUTORIAL_AS_SHOWN
} from '../constants/ActionTypes'

import appSettings from '../common/settings/appSettings'

const _buildProfileImageUrl = userId => {
  return `https://res.cloudinary.com/marbamedia/image/facebook/c_scale,h_100/${userId}.jpg`
}

const initialState = {
  loading: true,
  loggedUser: {},
  anonimous: false
}

export default auth = (state = initialState, action) => {
  switch (action.type) {

    case SUCCESS_TO_SAVE_USER:
      return {
        loading: false,
        anonimous: false,
        loggedUser: {
          ...action.user,
          newUser: true,
          tutorialsShown: [],
          imageUrl: action.user.authType == 'google' ? action.user.photo : _buildProfileImageUrl(action.user.id)
        }
      }

    case ERROR_TO_AUTHENTICATE:
      return { loading: false,  }

    case SET_USER_AS_SUBSCRIBER:
      return { ...state, loggedUser: {
        ...state.loggedUser,
        subscriber: true
      }}

    case LOGIN_LOADING:
      return { ...initialState, loading: true }

    case LOGIN_CANCELED:
      return { ...initialState, loading: false }

    case LOGOUT:
      return { loading: false, loggedUser: {}, anonimous: true }

    case SUCCESS_TO_LOAD_USER:
      return {
        ...state,
        loading: false,
        anonimous: false,
        loggedUser: {
          ...action.user,
          newUser: false,
          imageUrl: action.user.authType == 'google' ? action.user.photo : _buildProfileImageUrl(action.user.id)
        }
      }

    case SUCCESS_TO_SEND_HEARTS:
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          available_hearts: state.loggedUser.available_hearts - action.sendHeartsDto.count,
          sent_hearts: state.loggedUser.sent_hearts + (action.sendHeartsDto.count == 0 ? 1 : action.sendHeartsDto.count)
        }
      }

    case SUCCESS_TO_SAVE_DISCOUNT_COUPON:
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          available_hearts: state.loggedUser.available_hearts + appSettings.SENDER_USER_HEARTS_COUNT,
        }
      }

    case SUCCESS_TO_SAVE_COMMENT:
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          available_hearts: state.loggedUser.available_hearts + 5,
        }
      }

    case SET_ANONIMOUS_LOGIN:
      return { loading: false, anonimous: true, loggedUser: {} }

    case UPDATE_USER_HEARTS:
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          available_hearts: state.loggedUser.available_hearts + action.heartsCount,
        }
      }

    case SET_TUTORIAL_AS_SHOWN:
      return {
        ...state,
        loggedUser: {
          ...state.loggedUser,
          tutorialsShown: [...state.loggedUser.tutorialsShown, action.tutorialName],
        }
      }

    default:
      return state
  }
}
