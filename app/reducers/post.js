import {
  SUCCESS_TO_SEND_HEARTS,
  OPEN_POST,
  LOADING_COMMENTS,
  COMMENTS_LOADED,
  NO_COMMENTS,
  ERROR_TO_LOAD_COMMENTS,
  SAVING_COMMENT,
  SUCCESS_TO_SAVE_COMMENT,
  ERROR_TO_SAVE_COMMENT,
  DELETING_COMMENT,
  SUCCESS_TO_DELETE_COMMENT,
  ERROR_TO_DELETE_COMMENT,
  SUCCESS_TO_UPDATE_RANKING
} from '../constants/ActionTypes'

const initialState = {
  loadingComments: true,
  currentPost: {},
  comments: []
}

const updateHeartsCount = (item, action) => {

  let newItem = { ...item }

  if (newItem.ranking) {
    let isLoggedUserInRanking = newItem.ranking.map(r => r.user_id).includes(action.sendHeartsDto.user_id)

    if (isLoggedUserInRanking) {
      newItem.ranking = newItem.ranking.map(r => {
        if (r.user_id === action.sendHeartsDto.user_id) {
          return { ...r, count: r.count + action.sendHeartsDto.count }
        }
        return r
      }).sort((a, b) => a.count - b.count).reverse()
    } else {
      newItem.ranking.push(action.sendHeartsDto)
      newItem.ranking = newItem.ranking.sort((a, b) => a.count - b.count).reverse()
    }

    newItem.ranking = newItem.ranking.slice(0, 5)
  }
  return newItem
}

export default post = (state = initialState, action) => {
  switch (action.type) {

    case SUCCESS_TO_SEND_HEARTS:
      return {
        ...state,
        currentPost: {
          ...state.currentPost,
          ...updateHeartsCount(state.currentPost, action),
          hearts_count: (state.currentPost.hearts_count || 0) + action.sendHeartsDto.count || 1
        }
      }

    case SUCCESS_TO_UPDATE_RANKING:
      return {
        ...state,
        currentPost: {
          ...state.currentPost,
          ranking: action.ranking,
          hearts_count: action.heartsSum
        }
      }

    case OPEN_POST:
      return { ...state, loadingComments: true, currentPost: action.post, comments: [] }

    case LOADING_COMMENTS:
      return { ...state, loadingComments: true }

    case COMMENTS_LOADED:
      let commentsWithDuplicatedValues = state.comments.concat(action.comments)
      let uniqueIds = new Set(commentsWithDuplicatedValues.map(c => c.id))

      let comments = []

      uniqueIds.forEach(id => comments.push(commentsWithDuplicatedValues.find(c => c.id === id)))

      return {
        ...state,
        comments: comments,
        loadingComments: false,
        currentPost: {
          ...state.currentPost,
          comments_count: action.total
        }
      }

    case NO_COMMENTS:
      return { ...state, comments: action.page == 0 ? [] : state.comments, loadingComments: false }

    case ERROR_TO_LOAD_COMMENTS:
      return { ...state, comments: [], loadingComments: false }

    case SAVING_COMMENT:
      state.comments.unshift({
        ...action.comment,
        saving: true
      })

      return { ...state }

    case SUCCESS_TO_SAVE_COMMENT:
      return {
        ...state,
        currentPost: {
          ...state.currentPost,
          comments_count: (state.currentPost.comments_count || 0) + 1
        },
        comments: state.comments.map(comment => {
          if (comment.id === action.comment.id) {
            return { ...comment, saving: false, errorToSave: false }
          } else {
            return comment
          }
        })
      }

    case ERROR_TO_SAVE_COMMENT:
      return {
        ...state,
        comments: state.comments.map(comment => {
          if (comment.id === action.comment.id) {
            return { ...comment, saving: false, errorToSave: true }
          } else {
            return comment
          }
        })
      }

    case DELETING_COMMENT:
      return { ...state, comments: state.comments.map(comment => {
        if (comment.id === action.comment.id) {
          return { ...comment, deleting: true }
        } else {
          return comment
        }
      })}

    case SUCCESS_TO_DELETE_COMMENT:
      return {
        ...state,
        currentPost: {
          ...state.currentPost,
          comments_count: state.currentPost.comments_count - 1
        },
        comments: state.comments.filter(comment => {
          return comment.id !== action.comment.id
        }
      )}

    case ERROR_TO_DELETE_COMMENT:
      return { ...state, comments: state.comments.map(comment => {
        if (comment.id === action.comment.id) {
          return { ...comment, errorToDelete: true }
        } else {
          return comment
        }
      })}

    default:
      return state
  }
}
