import {
  SEND_HEARTS_LOADING,
  SUCCESS_TO_SEND_HEARTS,
  COMMENTS_LOADED,
  ERROR_TO_SEND_HEARTS,
  SUCCESS_TO_LOAD_TIMELINE,
  SUCCESS_TO_PAGINATE_TIMELINE,
  TIMELINE_LOADED_WITHOUT_RESULTS,
  ERROR_TO_LOAD_TIMELINE,
  SUCCESS_TO_SAVE_COMMENT,
  SUCCESS_TO_DELETE_COMMENT,
  LOADING_TIMELINE,
  SUCCESS_TO_UPDATE_RANKING
} from '../constants/ActionTypes'

const initialState = {
  loading: true,
  errorToLoadTimeline: false,
  sendHeartsLoading: false,
  items: [],
  total: 0,
  searchResults: {
    loading: true,
    errorToLoadTimeline: false,
    items: [],
    total: 0
  }
}

const updateStateBySearch = (state, propertiesToUpdate, isSearch) => {
  if (isSearch) {
    return {
      ...state,
      searchResults: { ...state.searchResults, ...propertiesToUpdate }
    }
  }
  return { ...state, ...propertiesToUpdate }
}

const updatePropertyCount = (item, property, postId, count, replace = false) => {
  if (item.id == postId) {
    let result = { ...item }

    if (replace) {
      result[property] =  count
    } else {
      result[property] = (item[property] || 0) + count  
    }

    return result
  }
  return item
}

const updateRanking = (item, action) => {
  if (item.id == action.postId) {
    return { ...item, ranking: action.ranking, hearts_count: action.heartsSum }
  }
  return item
}

const updateHeartsCount = (item, action) => {
  action.sendHeartsDto.count = action.sendHeartsDto.count || 1

  let result = updatePropertyCount(
    item, 'hearts_count', action.sendHeartsDto.post_id, action.sendHeartsDto.count
  )

  if (item.id == action.sendHeartsDto.post_id) {
    let isLoggedUserInRanking = result.ranking.map(r => r.user_id).includes(action.sendHeartsDto.user_id)

    if (isLoggedUserInRanking) {
      result.ranking = result.ranking.map(r => {
        if (r.user_id === action.sendHeartsDto.user_id) {
          return { ...r, count: r.count + action.sendHeartsDto.count }
        }
        return r
      }).sort((a, b) => a.count - b.count).reverse()
    } else {
      result.ranking.push(action.sendHeartsDto)
      result.ranking = result.ranking.sort((a, b) => a.count - b.count).reverse()
    }
    result.ranking = result.ranking.slice(0, 5)
  }
  return result
}

const updateCommentsCount = (item, action) => {
  let result = updatePropertyCount(
    item, 'comments_count', action.postId, action.total, true
  )
  return result
}

const incrementcomments_count = (item, action) => updatePropertyCount(
  item, 'comments_count', action.comment.postId, 1
)

const decrementcomments_count = (item, action) => updatePropertyCount(
  item, 'comments_count', action.comment.postId, -1
)

export default timeline = (state = initialState, action) => {
  switch (action.type) {

    case LOADING_TIMELINE:
      return updateStateBySearch(state, {
        loading: true
      }, action.isSearch)

    case SEND_HEARTS_LOADING:
      return { ...state, sendHeartsLoading: true }

    case SUCCESS_TO_SEND_HEARTS:
      return {
        ...state,
        sendHeartsLoading: false,
        items: state.items.map(i => updateHeartsCount(i, action)),
        searchResults: {
          ...state.searchResults,
          items: state.searchResults.items.map(i => updateHeartsCount(i, action))
        }
      }

    case COMMENTS_LOADED:
      return {
        ...state,
        sendHeartsLoading: false,
        items: state.items.map(i => updateCommentsCount(i, action)),
        searchResults: {
          ...state.searchResults,
          items: state.searchResults.items.map(i => updateCommentsCount(i, action))
        }
      }

    case SUCCESS_TO_UPDATE_RANKING:
      return {
        ...state,
        sendHeartsLoading: false,
        items: state.items.map(i => updateRanking(i, action)),
        searchResults: {
          ...state.searchResults,
          items: state.searchResults.items.map(i => updateRanking(i, action))
        }
      }

    case SUCCESS_TO_SAVE_COMMENT:
      return {
        ...state,
        sendHeartsLoading: false,
        items: state.items.map(i => incrementcomments_count(i, action)),
        searchResults: {
          ...state.searchResults,
          items: state.searchResults.items.map(i => incrementcomments_count(i, action))
        }
      }

      case SUCCESS_TO_DELETE_COMMENT:
        return {
          ...state,
          sendHeartsLoading: false,
          items: state.items.map(i => decrementcomments_count(i, action)),
          searchResults: {
            ...state.searchResults,
            items: state.searchResults.items.map(i => decrementcomments_count(i, action))
          }
        }

    case ERROR_TO_SEND_HEARTS:
      return { ...state, sendHeartsLoading: false }

    case SUCCESS_TO_LOAD_TIMELINE:
      return updateStateBySearch(state, {
        loading: false, items: action.items, total: action.total
      }, action.isSearch)

    case SUCCESS_TO_PAGINATE_TIMELINE:
      return updateStateBySearch(state, {
        loading: false, items: state.items.concat(action.items), total: action.total
      }, action.isSearch)

    case TIMELINE_LOADED_WITHOUT_RESULTS:
      return updateStateBySearch(state, {
        loading: false
      }, action.isSearch)

    case ERROR_TO_LOAD_TIMELINE:
      return updateStateBySearch(state, {
        loading: false, errorToLoadTimeline: action.error
      }, action.isSearch)

    default:
      return state
  }
}
