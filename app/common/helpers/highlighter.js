import React from 'react'

const highlighteText = (text, Component, highlitedStyles, notHighlitedStyles) => {
  let splittedTitle = text.split('$highlight$')

  if (splittedTitle.length === 1) {
    return (<Component style={highlitedStyles}>{ splittedTitle[0] }</Component>)
  }

  return (
    <Component>
      {
        splittedTitle.map((t, i) => {
          if (t.slice(-1) === '*' && t[0] === '*') {
            return (<Component key={ i } style={highlitedStyles}>
              { t.slice(1).substring(0, t.length - 2) }
            </Component>)
          } else {
            return (<Component key={ i } style={notHighlitedStyles}>
              { t }
            </Component>)
          }
        })
      }
    </Component>
  )
}

export default highlighteText
