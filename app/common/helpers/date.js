import moment from 'moment/min/moment-with-locales'
import locale from 'moment/locale/pt-br'

moment.locale('pt-br', locale);


export const dateStringToOrrtaLabel = (dateString, upperCase = true) => {

  let result = ''

  if (!dateString) {
    return result
  }

  try {
    let postDate = new Date(dateString)
    result = moment(dateString).fromNow()

    if (upperCase) {
      result = result.toUpperCase()
    }
  } catch (e) {
    result = ''
  } finally {
    return result
  }
}

export const diffDates = (a, b, type = 'days') => {
  return moment(a).diff(moment(b), type)
}
