const appSettings = {
  APP_DOWNLOAD_LINK: 'https://play.google.com/store/apps/details?id=orrta.julianasilveira',
  SENDER_USER_HEARTS_COUNT: 50,
  OWNER_USER_HEARTS_COUNT: 50
}

export default appSettings
