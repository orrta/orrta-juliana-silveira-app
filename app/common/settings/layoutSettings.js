import { Platform, Dimensions } from 'react-native'

const isIphoneX = () => {
  let d = Dimensions.get('window')

  const { height, width } = d

  return (
    // This has to be iOS duh
    Platform.OS === 'ios' &&

    // Accounting for the height in either orientation
    (height === 812 || width === 812)
  );
}

const topByDevice = () => {
  if (isIphoneX()) {
    return 25
  }
  if (Platform.OS === 'ios') {
    return 15
  }
  return 0
}

const bottomByDevice = () => {
  if (isIphoneX()) {
    return 15
  }
  return 0
}

const layoutSettings = {
  colors: {
    PRIMARY: '#fdf5f2',
    SECONDARY: '#ffebe3',
    SECONDARY_TRANSPARENT: 'rgba(255, 235, 227, 0.7)',
    TERTIARY: '#564440',
    TERTIARY_TRANSPARENT: 'rgba(86,68,64, 0.95)',
    LIGHT: '#fff',
    LIGHT_GREY: '#ededed',
    BLACK: '#000',
    LIGHT_TRANSPARENT: 'rgba(255, 255, 255, 0.9)',
    BLACK_TRANSPARENT: 'rgba(0, 0, 0, 0.6)',
    SHADOW_COLOR: '#000',
  },
  defaultRadius: 7,
  bigRadius: 15,
  topByDevice: topByDevice(),
  bottomByDevice: bottomByDevice()
}

export default layoutSettings
