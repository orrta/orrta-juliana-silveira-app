import { StyleSheet } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { typografyObjectStyles } from './typografy'

const headerObjectStyles = {
  container: {
    zIndex: 200,
    borderWidth: 0,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR,
    height: 40
  },
  header: {
    paddingTop: layoutSettings.topByDevice + 10,
    paddingBottom: 5,
    position: 'absolute',
    zIndex: 100,
    left: 0,
    right: 0,
    backgroundColor: layoutSettings.colors.SECONDARY,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  text: {
    ...typografyObjectStyles.title,
    color: layoutSettings.colors.TERTIARY,
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10
  }
}

const headerMenuObjectStyles = {
  icon: {
    paddingHorizontal: 5,
    color: layoutSettings.colors.TERTIARY,
    fontSize: 26,
  },
  iconWithText: {
    marginTop: 8,
    color: layoutSettings.colors.TERTIARY,
    fontSize: 20
  },
  text: {
    ...typografyObjectStyles.defaultText,
    color: layoutSettings.colors.TERTIARY,
    fontSize: 20,
    marginRight: 10
  },
  container: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10
  }
}

const headerStyles = StyleSheet.create(headerObjectStyles)
const headerMenuStyles = StyleSheet.create(headerMenuObjectStyles)

export {
  headerObjectStyles,
  headerStyles,
  headerMenuStyles,
  headerMenuObjectStyles
}
