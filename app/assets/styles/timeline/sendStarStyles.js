import { StyleSheet, Platform } from 'react-native'
import { baseCardObjectStyles } from '../timelineCard'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const defaultTitle = buildText({ fontSize: 24, color: layoutSettings.colors.TERTIARY, extend: {
  textAlign: 'center'
}})

const defaultShadow = {
  shadowColor: layoutSettings.colors.SHADOW_COLOR,
  shadowOffset: { width: 1, height: 1 },
  shadowOpacity: 0.4,
  shadowRadius: 1,
  elevation: 1,
}

const startsSuggestionItem = {
  marginVertical: 5,
  padding: 5,
  borderWidth: 0,
  borderRadius: layoutSettings.defaultRadius,
  backgroundColor: layoutSettings.colors.LIGHT,
  ...defaultShadow
}

const defaultButtonContainer = {
  marginTop: 10,
  padding: 15,
  borderWidth: 0,
  borderRadius: layoutSettings.defaultRadius,
  ...defaultShadow
}

const startsSuggestionStarsNumberIcon = {
  fontSize: 20,
  marginRight: 5,
  color: layoutSettings.colors.TERTIARY,
}

const startsSuggestionStarsNumberText = buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16, extend: {
  marginTop: 1
}})

const sendStarObjectStyles = {
  container: {
    backgroundColor: layoutSettings.colors.TERTIARY,
    flex: 1,
    justifyContent: 'center'
  },
  title: defaultTitle,
  cancelButton: {
    ...defaultTitle,
    marginTop: 10
  },
  card: {
    ...baseCardObjectStyles.card,
    marginHorizontal: 20,
    padding: 20,
    backgroundColor: layoutSettings.colors.LIGHT
  },
  startsNumber: {
    ...defaultTitle,
    color: layoutSettings.colors.TERTIARY,
    fontSize: 18
  },
  startsNumberBold: {
    fontWeight: 'bold'
  },
  startsSuggestions: {
    flexDirection: 'column',
    paddingTop: 15
  },
  startsSuggestionItem,
  startsSuggestionItemSelected: {
    ...startsSuggestionItem,
    backgroundColor: layoutSettings.colors.TERTIARY
  },
  startsSuggestionStarsNumberContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  startsSuggestionStarsNumberText,
  startsSuggestionStarsNumberTextSelected: {
    ...startsSuggestionStarsNumberText,
    color: layoutSettings.colors.TERTIARY
  },
  startsSuggestionStarsNumberIcon,
  startsSuggestionStarsNumberIconSelected: {
    ...startsSuggestionStarsNumberIcon,
    color: layoutSettings.colors.TERTIARY,
  },
  sendStarsButtonContainer: {
    backgroundColor: layoutSettings.colors.TERTIARY,
    ...defaultButtonContainer
  },
  sendStarsButtonText: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 18, extend: {
    textAlign: 'center'
  }}),
  starsSelectorSlider: {
    marginTop: 5
  },
  starsSelectorLimit: {
    flexDirection: 'row',
    marginBottom: 10
  },
  starsSelectorLimitMin: buildTitle({ color: layoutSettings.colors.SECONDARY, fontSize: 12, extend: {
    flex: 1,
    textAlign: 'left'
  }}),
  starsSelectorLimitMax: buildTitle({ color: layoutSettings.colors.SECONDARY, fontSize: 12, extend: {
    flex: 1,
    textAlign: 'right'
  }}),
}

const sendStarStyles = StyleSheet.create(sendStarObjectStyles)

export {
  sendStarStyles,
  sendStarObjectStyles
}
