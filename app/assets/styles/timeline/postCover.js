import { StyleSheet } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText } from '../typografy'


const postInfoObjectStyles = {
  container: {
    position: 'absolute',
    top: 10,
    right: 10
  },
  content: {
    backgroundColor: layoutSettings.colors.SECONDARY,
    paddingHorizontal: 10,
    paddingTop: 3,
    paddingBottom: 2,
    marginBottom: 5,
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  icon: {
    color: layoutSettings.colors.TERTIARY,
    fontSize: 16,
    marginRight: 2,
    marginTop: 1
  },
  iconGame: {
    color: layoutSettings.colors.TERTIARY,
    fontSize: 20,
    marginRight: 1,
    marginTop: -1
  },
  text: buildText({
    color: layoutSettings.colors.TERTIARY,
    fontSize: 14,
    extend: {
      textAlign: 'center',
      marginTop: 1,
      marginHorizontal: 3
    }
  }),
}

const postCoverObjectStyles = {
  sliderText: buildText({
    color: layoutSettings.colors.TERTIARY,
    extend: {
      textAlign: 'center'
    }
  }),
  closeSendHeartsButton: {
    paddingHorizontal: 5,
    paddingTop: 7,
    paddingBottom: 5,
    marginLeft: 10,
    width: 45,
  },
  sendHeartsCheckButton: {
    padding: 5,
    minWidth: 45,
  },
  sendHeartsOpenSliderButton: {
    paddingHorizontal: 5,
    paddingTop: 7,
    paddingBottom: 5,
    marginLeft: 10,
    width: 45,
    backgroundColor: layoutSettings.colors.TERTIARY
  },
  sendHeartsLoading: {
    width: 45,
    height: 45,
    marginLeft: 10,
    marginVertical: 5,
    backgroundColor: layoutSettings.colors.TERTIARY,
    justifyContent: 'space-around',
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR,
    paddingTop: 3,
    paddingLeft: 3
  },
  sendHeartsContainer: {
    height: 280,
    width: 45,
    marginLeft: 10,
    marginVertical: 5,
    backgroundColor: layoutSettings.colors.SECONDARY,
    justifyContent: 'space-around',
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  sendHeartsSliderThumb: {
    backgroundColor: layoutSettings.colors.TERTIARY,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 1,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  sendHeartsSliderTrack: {
    backgroundColor: layoutSettings.colors.TERTIARY,
    width: 200
  },
  sendHeartsSlider: {
    width: 200,
    marginTop: 80,
    marginLeft: -77
  },
  sendHeartsSliderContainer: {
    height: 200
  }
}

const postCoverStyles = StyleSheet.create(postCoverObjectStyles)
const postInfoStyles = StyleSheet.create(postInfoObjectStyles)

export {
  postCoverStyles,
  postCoverObjectStyles,
  postInfoStyles,
  postInfoObjectStyles
}
