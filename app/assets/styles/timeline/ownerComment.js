import { StyleSheet } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const ownerCommentObjectStyles = {
  container: {
    margin: 10
  },
  comment: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'flex-start'
  },
  ownerCommentText: buildText({ fontSize: 20, color: layoutSettings.colors.TERTIARY, extend: {
    textAlign: 'justify'
  }}),
  title: buildTitle({ fontSize: 20, color: layoutSettings.colors.TERTIARY }),
  ownerCommentContent: {
    flex: 1
  }
}

const ownerCommentStyles = StyleSheet.create(ownerCommentObjectStyles)

export {
  ownerCommentStyles,
  ownerCommentObjectStyles
}
