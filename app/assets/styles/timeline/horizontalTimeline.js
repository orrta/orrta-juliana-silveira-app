import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const horizontalTimelineObjectStyles = {
  section: {
    backgroundColor: layoutSettings.colors.PRIMARY
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  sectionTitle: buildText({ bold: true, color: layoutSettings.colors.TERTIARY, fontSize: 18, extend: { padding: 10 } }),
  showMore: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16, extend: { padding: 10 } }),
  iconRight: {
    fontSize: 16
  },
  list: {
    flexDirection: 'row',
    paddingBottom: 30
  },
  post: {
    width: 220,
    backgroundColor: layoutSettings.colors.LIGHT,
    marginHorizontal: 10,
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR,
    backgroundColor: layoutSettings.colors.SECONDARY,
  },
  postTitleConteiner: {
    padding: 5,
    borderWidth: 0,
    borderTopLeftRadius: layoutSettings.defaultRadius,
    borderTopRightRadius: layoutSettings.defaultRadius
  },
  postTitleText: buildText({ fontSize: 16, color: layoutSettings.colors.TERTIARY, extend: {
    textAlign: 'center',
    paddingHorizontal: 5
  }}),
  postImageConteiner: {
    borderWidth: 0,
    borderBottomLeftRadius: layoutSettings.defaultRadius,
    borderBottomRightRadius: layoutSettings.defaultRadius,
    overflow: 'hidden'
  },
  postImage: {
    borderWidth: 0,
    borderBottomLeftRadius: layoutSettings.defaultRadius,
    borderBottomRightRadius: layoutSettings.defaultRadius,
    height: 250,
    width: 220
  },
  locked: {
    position: 'absolute',
    zIndex: 300,
    top: 90,
    left: 0,
    right: 0
  },
  lockedContainer: {
    alignItems: 'center',
  },
  lockedIconContent: {
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    backgroundColor: layoutSettings.colors.LIGHT_TRANSPARENT,
    opacity: 0.6,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 15,
    alignItems: 'center',
  },
  lockedIcon: {
    color: layoutSettings.colors.TERTIARY,
    fontSize: 48,
  },
  lockedText:  buildTitle({ fontSize: 12, color: layoutSettings.colors.TERTIARY }),
}

const horizontalTimelineStyles = StyleSheet.create(horizontalTimelineObjectStyles)

export {
  horizontalTimelineStyles,
  horizontalTimelineObjectStyles
}
