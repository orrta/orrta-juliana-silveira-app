import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const searchObjectStyles = {
  container: {
    backgroundColor: layoutSettings.colors.PRIMARY,
    flex: 1
  },
  searchContainer: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    minHeight: 40,
    marginTop: 15,
    padding: 5,
    borderWidth: 3,
    borderRadius: layoutSettings.defaultRadius,
    borderColor: layoutSettings.colors.TERTIARY
  },
  input: buildText({ fontSize: 20, color: layoutSettings.colors.TERTIARY, extend: {
    backgroundColor: layoutSettings.colors.PRIMARY,
    padding: 5,
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    flex: 1
  } }),
  icon: {
    fontSize: 24,
    paddingTop: 7,
    marginHorizontal: 10,
    color: layoutSettings.colors.TERTIARY
  },
  iconRight: {
    marginHorizontal: 10,
  },
  filterTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
    paddingVertical: 10,
    borderBottomWidth: 0.5,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderColor: layoutSettings.colors.TERTIARY
  },
  filterTitleText: buildText({ bold: true, fontSize: 24, color: layoutSettings.colors.TERTIARY }),
  filterExitContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: layoutSettings.colors.TERTIARY,
    borderWidth: 0,
    borderRadius: 11,
    width: 22,
    height: 22,
  },
  filterExitIcon: {
    color: layoutSettings.colors.PRIMARY,
    height: 17,
    width: 13,
    fontSize: 16
  }
}

const searchStyles = StyleSheet.create(searchObjectStyles)

export {
  searchStyles,
  searchObjectStyles
}
