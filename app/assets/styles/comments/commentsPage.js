import { StyleSheet, Dimensions } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const commentsPageObjectStyles = {
  container: {
    flex: 1,
    backgroundColor: layoutSettings.colors.PRIMARY
  },
  body: {
    marginTop: layoutSettings.topByDevice + 4,
    flex: 1
  },
  noContentBody: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 80
  },
  noContentContainer: {
    marginHorizontal: 20,
    paddingVertical: 60,
    paddingHorizontal: 20,
    borderColor: layoutSettings.colors.SECONDARY,
    borderLeftWidth: 10
  },
  noContentText: buildTitle({ color: layoutSettings.colors.SECONDARY, fontSize: 32 }),
  fixedCommentInput: {
    position: 'absolute',
    left: 0,
    right: 0
  }
}

const commentsPageStyles = StyleSheet.create(commentsPageObjectStyles)

export {
  commentsPageStyles,
  commentsPageObjectStyles
}
