import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { buildText } from './typografy'

const baseCardObjectStyles = {
  card: {
    backgroundColor: layoutSettings.colors.SECONDARY,
    margin: 15,
    borderWidth: 0,
    borderRadius: layoutSettings.bigRadius,
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 4,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  cardImage: {
    height: 450,
    borderWidth: 0,
    borderBottomLeftRadius: layoutSettings.bigRadius,
    borderBottomRightRadius: layoutSettings.bigRadius,
    width: null
  },
  imageContainer: {
    borderWidth: 0,
    borderBottomLeftRadius: layoutSettings.bigRadius,
    borderBottomRightRadius: layoutSettings.bigRadius,
    overflow: 'hidden'
  },
  cardImageTop: {
    height: 450,
    borderWidth: 0,
    borderTopLeftRadius: layoutSettings.bigRadius,
    borderTopRightRadius: layoutSettings.bigRadius,
    width: null
  },
  imageContainerTop: {
    borderWidth: 0,
    borderTopLeftRadius: layoutSettings.bigRadius,
    borderTopRightRadius: layoutSettings.bigRadius,
    overflow: 'hidden'
  },
  titleContainer: {
    paddingHorizontal: 5,
    paddingVertical: 10,
    borderWidth: 0,
    borderTopLeftRadius: layoutSettings.bigRadius,
    borderTopRightRadius: layoutSettings.bigRadius,
    backgroundColor: layoutSettings.colors.SECONDARY
  },
  buttonContainer: {
    alignSelf: 'stretch',
    marginHorizontal: 10,
    backgroundColor: layoutSettings.colors.TERTIARY
  },
  buttonText: {
    color: layoutSettings.colors.LIGHT
  },
  title: buildText({ fontSize: 24, color: layoutSettings.colors.TERTIARY, extend: { textAlign: 'center' } }),
  subTitleContainer: {
    alignItems: 'center',
    borderColor: layoutSettings.colors.TERTIARY,
    borderWidth: 0,
    borderBottomWidth: 2,
    marginRight: 80,
    marginLeft: 80,
    marginBottom: 3,
    paddingBottom: 3
  },
  subTitle: buildText({ fontSize: 14, color: layoutSettings.colors.TERTIARY })
}

const advertisingCardObjectStyles = {
  card: {
    ...baseCardObjectStyles.card,
    backgroundColor: layoutSettings.colors.SECONDARY
  },
  titleContainer: {
    ...baseCardObjectStyles.titleContainer,
    backgroundColor: layoutSettings.colors.SECONDARY,
    paddingBottom: 3
  },
  title: {
    ...baseCardObjectStyles.title,
    color: layoutSettings.colors.TERTIARY
  },
  buttonContainer: baseCardObjectStyles.buttonContainer,
  buttonText: baseCardObjectStyles.buttonText,
  content: buildText({ color: layoutSettings.colors.TERTIARY, extend: {
    textAlign: 'center'
  }}),
  contentContainer: {
    marginHorizontal: 15,
    borderTopWidth: 1,
    borderColor: layoutSettings.colors.TERTIARY,
    paddingVertical: 15
  },
  advertisingCard: {
    backgroundColor: layoutSettings.colors.SECONDARY,
    borderWidth: 0,
    paddingBottom: 10,
    borderBottomLeftRadius: layoutSettings.bigRadius,
    borderBottomRightRadius: layoutSettings.bigRadius,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
}

const inviteFriendsCardObjectStyles = {
  buttonText: baseCardObjectStyles.buttonText,
  card: baseCardObjectStyles.card,
  title: advertisingCardObjectStyles.title,
  titleContainer: advertisingCardObjectStyles.titleContainer,
  content: advertisingCardObjectStyles.content,
  contentContainer: advertisingCardObjectStyles.contentContainer,
  cardContainer: advertisingCardObjectStyles.advertisingCard,
  buttonContainer: baseCardObjectStyles.buttonContainer
}

const postCardObjectStyles = {
  card: baseCardObjectStyles.card,
  image: baseCardObjectStyles.cardImage,
  imageContainer: baseCardObjectStyles.imageContainer,
  imageTop: baseCardObjectStyles.cardImageTop,
  imageContainerTop: baseCardObjectStyles.imageContainerTop,
  titleContainer: baseCardObjectStyles.titleContainer,
  title: baseCardObjectStyles.title,
  subTitle: baseCardObjectStyles.subTitle,
  actions: {
    position: 'absolute',
    flexDirection: 'row',
    bottom: 10,
    right: 10
  },
  ranking: {
    position: 'absolute',
    flexDirection: 'column',
    bottom: 10,
    left: 10,
    width: 63,
  },
  locked: {
    position: 'absolute',
    zIndex: 300,
    top: 180,
    left: 0,
    right: 0
  },
  lockedContainer: {
    alignItems: 'center',
  },
  lockedIconContent: {
    borderWidth: 0,
    borderRadius: 10,
    backgroundColor: layoutSettings.colors.LIGHT_TRANSPARENT,
    opacity: 0.6,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 15,
    alignItems: 'center',
  },
  lockedIcon: {
    color: layoutSettings.colors.TERTIARY,
    fontSize: 48,
  },
  lockedText: buildText({ fontSize: 12, color: layoutSettings.colors.TERTIARY }),
  subTitleContainer: baseCardObjectStyles.subTitleContainer,
  ownerCommentView: {
    position: 'absolute',
    zIndex: 100,
    padding: 10,
    flexDirection: 'row',
    backgroundColor: layoutSettings.colors.LIGHT_TRANSPARENT
  },
  videoContainer: {
    position: 'absolute',
    zIndex: 300,
    top: 0,
    right: 0
  },
  videoAnimation: {
    width: 50,
    height: 50
  }
}

const advertisingCardStyles = StyleSheet.create(advertisingCardObjectStyles)
const inviteFriendsCardStyles = StyleSheet.create(inviteFriendsCardObjectStyles)
const postCardStyles = StyleSheet.create(postCardObjectStyles)
const baseCardStyles = StyleSheet.create(baseCardObjectStyles)

export {
  advertisingCardStyles,
  advertisingCardObjectStyles,
  postCardStyles,
  postCardObjectStyles,
  baseCardStyles,
  baseCardObjectStyles,
  inviteFriendsCardStyles,
  inviteFriendsCardObjectStyles
}
