import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const button = {
  padding: 15,
  position: 'absolute',
  right: 20,
  left: 20,
  borderWidth: 0,
  borderRadius: layoutSettings.defaultRadius,
}

const defaultButtonText = buildText({ fontSize: 18, extend: { textAlign: 'center' } })

const container = {
  position: 'absolute',
  top: 0,
  right: 0,
  left: 0,
  bottom: 0,
}

const loginObjectStyles = {
  loading: container,
  container: container,
  image: {
    flex: 1,
    width: null,
    height: null
  },
  textContainer: {
    flex: 1,
    position: 'absolute',
    top: 40,
    right: 0,
    left: 0
  },
  title: buildTitle({ light: true, color: layoutSettings.colors.TERTIARY, fontSize: 60, extend: {
    textAlign: 'center'
  } }),
  loginFB: {
    ...button,
    bottom: 170
  },
  loginGoogle: {
    ...button,
    bottom: 105
  },
  loginFBText: {
  },
  enterWithoutLogin: {
    ...button,
    backgroundColor: layoutSettings.colors.PRIMARY,
    bottom: 40
  },
  enterWithoutLoginText: {
    color: layoutSettings.colors.TERTIARY,
  }
}

const loginStyles = StyleSheet.create(loginObjectStyles)

export {
  loginStyles,
  loginObjectStyles
}
