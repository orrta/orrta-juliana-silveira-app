import { StyleSheet } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const container = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
}

const quizObjectStyles = {
  container: container,
  contentContainer: {
    ...container,
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: layoutSettings.colors.SECONDARY_TRANSPARENT
  },
  answerContainer: {
    minHeight: 140
  },
  image: {
    flex: 1,
    width: null,
    height: null
  },
  questionTitle: buildTitle({ color: layoutSettings.colors.TERTIARY, fontSize: 64, extend: {
    padding: 10
  }}),
  questionAnswered: buildTitle({ color: layoutSettings.colors.TERTIARY, fontSize: 20, extend: {
    padding: 10
  }}),
  description: buildText({ bold: true, color: layoutSettings.colors.TERTIARY, fontSize: 20, extend: {
    padding: 10,
    backgroundColor: layoutSettings.colors.SECONDARY,
    marginTop: 20
  }}),
  responseTitle: buildText({ bold: true, color: layoutSettings.colors.TERTIARY, fontSize: 30, extend: {
    padding: 10,
    backgroundColor: layoutSettings.colors.SECONDARY,
  }}),
  yourResponseContainer: {
    flexDirection: 'row',
    padding: 10
  },
  yourResponsePrefix: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 20 }),
  yourResponseText: buildText({ bold: true, color: layoutSettings.colors.TERTIARY, fontSize: 20 }),
}

const quizStyles = StyleSheet.create(quizObjectStyles)

export {
  quizStyles,
  quizObjectStyles
}
