import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { buildText, buildTitle } from './typografy'

const timelineActionBaseButtonObjectStyles = {
  background: {
    padding: 8,
    width: 'auto',
    minWidth: 48,
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 3,
    backgroundColor: layoutSettings.colors.SECONDARY,
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  textContainer: {
    marginRight: 3
  },
  text: buildText({ color: layoutSettings.colors.LIGHT })
}

const timelineActionButtonObjectStyles = {
  icon: {
    margin: 0,
    fontSize: 18,
    color: layoutSettings.colors.LIGHT,
  },
  iconHeart: {
    margin: 0,
    fontSize: 14,
    paddingTop: 2,
    color: layoutSettings.colors.LIGHT,
  },
  iconSendStar: {
    margin: 0,
    fontSize: 26,
    color: layoutSettings.colors.LIGHT,
  }
}

const timelineCommentInputObjectStyles = {
  container: {
    margin: 10,
    borderColor: layoutSettings.colors.TERTIARY,
  },
  input: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16, extend: {
    flex: 1,
    textAlign: 'left'
  }}),
  icon: {
    color: layoutSettings.colors.TERTIARY
  }
}

const dateText = buildText({ fontSize: 12, color: layoutSettings.colors.SECONDARY, extend: {
  paddingTop: 2,
  paddingHorizontal: 17
}})

const timelineCommentObjectStylesBase = {
  container: {
    marginHorizontal: 10,
    marginTop: 10
  },
  commentsUserName: buildTitle({ fontSize: 16, color: layoutSettings.colors.TERTIARY, extend: { paddingHorizontal: 5, paddingTop: 5 } }),
  text: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16, extend: { paddingHorizontal: 5, paddingBottom: 5 } }),
  commentsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: layoutSettings.colors.TERTIARY
  },
  commentsUserNameContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  commentsText: {
    flex: 1,
    padding: 5,
    marginHorizontal: 10,
    backgroundColor: layoutSettings.colors.SECONDARY,
    borderRadius: layoutSettings.defaultRadius
  },
  deleteContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: layoutSettings.colors.TERTIARY,
    borderWidth: 0,
    borderRadius: 11,
    width: 22,
    height: 22
  },
  deleteIcon: {
    color: layoutSettings.colors.SECONDARY,
    height: 17,
    width: 13,
    fontSize: 16
  },
  dateTextLeft: {
    ...dateText,
    color: layoutSettings.colors.TERTIARY,
    textAlign: 'left'
  },
  dateTextRight: {
    ...dateText,
    color: layoutSettings.colors.TERTIARY,
    textAlign: 'right'
  },
}

const timelineCommentObjectStyles = {
  ...timelineCommentObjectStylesBase,
  commentsUserNameInverted: {
    ...timelineCommentObjectStylesBase.commentsUserName,
    textAlign: 'right'
  },
  textInverted: {
    ...timelineCommentObjectStylesBase.text,
    textAlign: 'right'
  },
}

const timelineActionBaseButtonStyles = StyleSheet.create(timelineActionBaseButtonObjectStyles)
const timelineActionButtonStyles = StyleSheet.create(timelineActionButtonObjectStyles)
const timelineCommentInputStyles = StyleSheet.create(timelineCommentInputObjectStyles)
const timelineCommentStyles = StyleSheet.create(timelineCommentObjectStyles)

export {
  timelineActionBaseButtonStyles,
  timelineActionBaseButtonObjectStyles,
  timelineActionButtonStyles,
  timelineActionButtonObjectStyles,
  timelineCommentInputStyles,
  timelineCommentInputObjectStyles,
  timelineCommentStyles,
  timelineCommentObjectStyles
}
