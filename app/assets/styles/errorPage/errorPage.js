import { StyleSheet } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText } from '../typografy'

const errorPageObjectStyles = {
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  image: {
    flex: 1,
    width: null,
    height: null
  },
  textContainer: {
    position: 'absolute',
    padding: 20,
    right: 0,
    left: 0,
    top: 0,
  },
  button: {
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 40,
  },
  text: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 20, extend: {
    textAlign: 'center',
    paddingVertical: 10
  }})
}

const errorPageStyles = StyleSheet.create(errorPageObjectStyles)

export {
  errorPageStyles,
  errorPageObjectStyles
}
