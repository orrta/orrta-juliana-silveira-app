import { StyleSheet } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { typografyObjectStyles, buildText, buildTitle } from './typografy'

const timelineObjectStyles = {
  noPosts: {
    alignItems: 'center',
    flex: 1
  },
  noPostsText: {
    ...typografyObjectStyles.defaultText,
    color: layoutSettings.colors.PRIMARY,
    marginTop: 140,
    fontSize: 22
  },
  container: {
    paddingHorizontal: 0,
  },
  body: {
    backgroundColor: layoutSettings.colors.PRIMARY,
    flex: 1
  }
}

const timelineStyles = StyleSheet.create(timelineObjectStyles)

export {
  timelineStyles,
  timelineObjectStyles
}
