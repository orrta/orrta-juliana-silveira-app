import { StyleSheet } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { buildText } from './typografy'


const toastHeartsObjectStyles = {
  iconHeart: {
    color: layoutSettings.colors.PRIMARY,
    fontSize: 22,
    marginHorizontal: 5,
    marginTop: -3
  },
  iconPlus: {
    color: layoutSettings.colors.PRIMARY,
    fontSize: 20,
    marginTop: -3,
    marginHorizontal: 10
  },
  text: buildText({ fontSize: 28, color: layoutSettings.colors.PRIMARY, bold: true }),
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
}

const toastObjectStyles = {
  container: {
    paddingHorizontal: 5,
    paddingTop: 5,
    marginTop: 20,
    paddingBottom: 2,
    backgroundColor: layoutSettings.colors.TERTIARY,
    alignSelf: 'flex-end',
    borderTopLeftRadius: layoutSettings.defaultRadius,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: layoutSettings.defaultRadius,
    borderBottomRightRadius: 0,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  }
}

const toastHeartsStyles = StyleSheet.create(toastHeartsObjectStyles)
const toastStyles = StyleSheet.create(toastObjectStyles)

export {
  toastHeartsObjectStyles,
  toastHeartsStyles,
  toastObjectStyles,
  toastStyles
}
