import { StyleSheet, Dimensions } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const discountCouponObjectStyles = {
  contentContainer: {
    justifyContent: 'space-around',
    flex: 1
  },
  video: {
    height: Dimensions.get('window').width
  },
  primaryText: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 18, extend: {
    textAlign: 'center',
    padding: 10
  }}),
  textInputContainer: {
    margin: 10,
    borderColor: layoutSettings.colors.LIGHT
  },
  textInput: {
    color: layoutSettings.colors.TERTIARY
  },
  divisor: {
    marginVertical: 15,
    backgroundColor: layoutSettings.colors.LIGHT,
    height: 4,
    marginHorizontal: 100,
    borderRadius: 2,
    borderWidth: 0
  }
}

const discountCouponStyles = StyleSheet.create(discountCouponObjectStyles)

export {
  discountCouponStyles,
  discountCouponObjectStyles
}
