import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText } from '../typografy'

const rankingPreviewObjectStyles = {
  listItem: {
    marginVertical: 5
  },
  rankingThumbnailContainer: {
    marginHorizontal: 4
  },
  rankingThumbnailImage: {
    width: 48,
    height: 48,
    borderWidth: 2,
    borderRadius: 24,
    borderColor: layoutSettings.colors.LIGHT,
    backgroundColor: layoutSettings.colors.SHADOW_COLOR,
    shadowColor: layoutSettings.colors.SHADOW_COLOR,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    margin: 3
  },
  rankingThumbnail: {
    width: 44,
    height: 44,
    borderWidth: 0,
    borderRadius: 22
  },
  rankingPositionText: buildText({ fontSize: 11, color: layoutSettings.colors.TERTIARY , extend: {
    paddingTop: 1
  }}),
  rankingStarCountText: buildText({ fontSize: 12, color: layoutSettings.colors.LIGHT }),
  rankingPositionContainer: {
    position: 'absolute',
    left: -2,
    top: -2,
    justifyContent: 'center',
    alignItems: 'center',
    height: 22,
    width: 22,
    backgroundColor: layoutSettings.colors.SECONDARY,
    borderWidth: 0,
    borderRadius: 11,
    zIndex: 300,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  rankingStarCountContainer: {
    zIndex: 300,
    position: 'absolute',
    bottom: -2,
    right: 2,
    left: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: layoutSettings.colors.TERTIARY,
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  rankingStarCountIcon: {
    fontSize: 12,
    marginLeft: 2,
    marginTop: 1.5,
    color: layoutSettings.colors.LIGHT
  }
}

const rankingPreviewStyles = StyleSheet.create(rankingPreviewObjectStyles)

export {
  rankingPreviewStyles,
  rankingPreviewObjectStyles
}
