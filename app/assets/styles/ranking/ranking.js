import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { baseCardObjectStyles } from '../timelineCard'
import { buildText, buildTitle } from '../typografy'

const rankingObjectStyles = {
  container: {
    flex: 1,
    backgroundColor: layoutSettings.colors.LIGHT
  },
  body: {
    marginTop: layoutSettings.topByDevice + 4,
    flex: 1
  },
  noContentBody: {
    flex: 1,
    justifyContent: 'center'
  },
  noContentContainer: {
    marginHorizontal: 20,
    paddingVertical: 60,
    paddingHorizontal: 20,
    borderColor: layoutSettings.colors.SECONDARY,
    borderLeftWidth: 10
  },
  noContentText: buildTitle({ color: layoutSettings.colors.SECONDARY, fontSize: 32 }),
  rankingItem: {
    ...baseCardObjectStyles.card,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    marginBottom: 0
  },
  name: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 26, extend: {
    flex: 1,
    textAlign: 'center'
  } })
}

const rankingStyles = StyleSheet.create(rankingObjectStyles)

export {
  rankingStyles,
  rankingObjectStyles
}
