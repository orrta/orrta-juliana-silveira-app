import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText } from '../typografy'
import { baseCardObjectStyles, advertisingCardObjectStyles } from '../timelineCard'

const profileObjectStyles = {
  termsContainer: {
    flex: 1,
    backgroundColor: layoutSettings.colors.LIGHT
  },
  defaultContainer: {
    paddingHorizontal: 15,
    marginTop: layoutSettings.topByDevice + 4
  },
  defaultTitle: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 22 }),
  defaultText: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16, extend: {
    padding: 5
  }}),
  heartsInfoSection: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  heartsInfoCard: {
    ...advertisingCardObjectStyles.card,
    flex: 1
  },
  heartsInfoCardBody: {
    ...advertisingCardObjectStyles.contentContainer,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  heartsInfoTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 5
  },
  heartsInfoTitle: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16 }),
  heartsInfoText: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 28 }),
  heartsInfoIcon: {
    color: layoutSettings.colors.TERTIARY,
    fontSize: 23,
    marginLeft: 3,
    marginBottom: 3
  },
  userContainer: {
    ...baseCardObjectStyles.card,
    borderTopLeftRadius: layoutSettings.defaultRadius,
    borderTopRightRadius: layoutSettings.defaultRadius,
    borderBottomLeftRadius: layoutSettings.defaultRadius,
    borderBottomRightRadius: layoutSettings.defaultRadius,
    justifyContent: 'center',
    padding: 10,
    backgroundColor: layoutSettings.colors.SECONDARY
  },
  userName: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 24, extend: {
    textAlign: 'center'
  } }),
  thumbnailContainer: {
    alignItems: 'center'
  },
  thumbnail: {
    marginTop: 30,
    marginBottom: 10,
    width: 96,
    height: 96,
    borderRadius: 96 / 2,
    borderWidth: 3,
    borderColor: layoutSettings.colors.LIGHT,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  thumbnailImage: {
    width: 90,
    height: 90,
    borderRadius: 90 / 2
  },
  subTitleContainer: {
    marginVertical: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cardButton: {
    marginHorizontal: 0
  },
  exitButton: {
    marginHorizontal: 0,
    backgroundColor: layoutSettings.colors.PRIMARY
  },
  exitButtonText: {
    color: layoutSettings.colors.TERTIARY
  },
  card: advertisingCardObjectStyles.card,
  titleContainer: advertisingCardObjectStyles.titleContainer,
  title: advertisingCardObjectStyles.title,
  contentContainer: advertisingCardObjectStyles.contentContainer,
  content: {
    ...advertisingCardObjectStyles.content,
    marginBottom: 30
  }
}

const profileStyles = StyleSheet.create(profileObjectStyles)

export {
  profileStyles,
  profileObjectStyles
}
