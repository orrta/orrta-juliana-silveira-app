import { StyleSheet } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const modalObjectStyles = {
  container: {
    backgroundColor: layoutSettings.colors.BLACK_TRANSPARENT,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    justifyContent: 'center'
  },
  cardContainer: {
    alignSelf: 'stretch',
    marginHorizontal: 20,
    paddingVertical: 5,
    backgroundColor: layoutSettings.colors.PRIMARY,
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 4,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  image: {
    alignSelf: 'center',
    marginVertical: 5
  },
  title: buildText({ bold: true, color: layoutSettings.colors.TERTIARY, fontSize: 24, extend: {
    textAlign: 'center',
    marginTop: 10
  }}),
  subtitle: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 20, extend: {
    textAlign: 'center',
    padding: 10
  }})
}

const modalStyles = StyleSheet.create(modalObjectStyles)

export {
  modalStyles,
  modalObjectStyles
}
