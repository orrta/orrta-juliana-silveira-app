import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { buildText } from './typografy'


const safeAreaViewObjectStyles = {
  container: {
    flex: 1,
    backgroundColor: layoutSettings.colors.SECONDARY
  }
}

const videoObjectStyles = {
  playButton: {
    backgroundColor: layoutSettings.colors.SECONDARY
  }
}

const loaderObjectStyles = {
  loader: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 80,
    marginBottom: -2,
    backgroundColor: 'transparent'
  }
}

if (Platform.OS !== 'ios') {
  loaderObjectStyles.loader.marginBottom = 0
  loaderObjectStyles.loader.marginTop = -1
  loaderObjectStyles.loader.marginLeft = -2
}

const thumbnailObjectStyles = {
  thumbnail: {
    borderWidth: 0,
    borderRadius: 27,
    height: 54,
    width: 54
  }
}

const exitButtonObjectStyles = {
  container: {
    position: 'absolute',
    zIndex: 100,
    right: 13,
    top: 20 + layoutSettings.topByDevice,
    zIndex: 100,
    backgroundColor: layoutSettings.colors.TERTIARY,
    borderColor: layoutSettings.colors.SECONDARY,
    width: 32,
    height: 32,
    borderWidth: 2,
    borderRadius: 16,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  icon: {
    marginLeft: -1,
    marginTop: -4,
    fontSize: 37,
    color: layoutSettings.colors.SECONDARY,
    backgroundColor: 'transparent'
  }
}

const textInputObjectStyles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 40,
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
    borderWidth: 3,
    borderRadius: layoutSettings.defaultRadius,
    borderColor: layoutSettings.colors.TERTIARY
  },
  input: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 18, extend: {
    flex: 1,
    margin: 5,
    padding: 0
  }}),
  icon: {
    color: layoutSettings.colors.TERTIARY,
    fontSize: 30,
    height: 30
  }
}

const buttonObjectStyles = {
  container: {
    backgroundColor: layoutSettings.colors.TERTIARY,
    marginHorizontal: 10,
    marginVertical: 5,
    padding: 12,
    borderWidth: 0,
    borderRadius: layoutSettings.defaultRadius,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 2,
    shadowColor: layoutSettings.colors.SHADOW_COLOR
  },
  text: buildText({ color: layoutSettings.colors.LIGHT, fontSize: 18, extend: {
    textAlign: 'center'
  }}),
  icon: {
    color: layoutSettings.colors.LIGHT,
    fontSize: 28,
    textAlign: 'center'
  },
}

buttonObjectStyles.containerOutline = {
  ...buttonObjectStyles.container,
  backgroundColor: layoutSettings.colors.SECONDARY
}

buttonObjectStyles.textOutline = {
  ...buttonObjectStyles.text,
  color: layoutSettings.colors.TERTIARY
}

buttonObjectStyles.iconOutline = {
  ...buttonObjectStyles.icon,
  color: layoutSettings.colors.TERTIARY
}


const loaderStyles = StyleSheet.create(loaderObjectStyles)
const thumbnailStyles = StyleSheet.create(thumbnailObjectStyles)
const exitButtonStyles = StyleSheet.create(exitButtonObjectStyles)
const textInputStyles = StyleSheet.create(textInputObjectStyles)
const buttonStyles = StyleSheet.create(buttonObjectStyles)
const safeAreaViewStyles = StyleSheet.create(safeAreaViewObjectStyles)
const videoStyles = StyleSheet.create(videoObjectStyles)


export {
  loaderObjectStyles,
  loaderStyles,
  thumbnailStyles,
  thumbnailObjectStyles,
  exitButtonStyles,
  exitButtonObjectStyles,
  textInputStyles,
  textInputObjectStyles,
  buttonStyles,
  buttonObjectStyles,
  safeAreaViewStyles,
  safeAreaViewObjectStyles,
  videoStyles,
  videoObjectStyles
}
