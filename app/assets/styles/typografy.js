import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'

const defaultFontSize = 16
const defaultColor = layoutSettings.colors.TERTIARY

const typografyObjectStyles = {
  titleLight: {
    fontFamily: 'AvenirNext-UltraLight',
    fontSize: 34
  },
  title: {
    fontFamily: 'AvenirNextCondensed-Heavy',
    fontSize: 34
  },
  defaultText: {
    fontFamily: 'AvenirNextCondensed-DemiBold',
    fontSize: defaultFontSize
  },
  textBold: {
    fontFamily: 'AvenirNextCondensed-Bold',
    fontSize: defaultFontSize
  }
}

const _build = ({ typografy, color = defaultColor, fontSize = defaultFontSize, extend = {} }) => {
  return { ...typografy, color, fontSize, ...extend }
}

const buildText = ({
  bold = false,
  color = defaultColor,
  fontSize = defaultFontSize,
  extend = {}
} = {}) => _build({
  typografy: bold ? typografyObjectStyles.textBold : typografyObjectStyles.defaultText,
  color,
  fontSize,
  extend
})

const buildTitle = ({
  light = false,
  color = defaultColor,
  fontSize = 34,
  extend = {}
} = {}) => _build({
  typografy: light ? typografyObjectStyles.titleLight : typografyObjectStyles.title,
  color,
  fontSize,
  extend
})

const typografyStyles = StyleSheet.create(typografyObjectStyles)

export {
  typografyObjectStyles,
  typografyStyles,
  buildText,
  buildTitle
}

export default typografyStyles
