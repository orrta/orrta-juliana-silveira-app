import { StyleSheet, Platform } from 'react-native'
import layoutSettings from '../../common/settings/layoutSettings'
import { buildText, buildTitle } from './typografy'

const multiContentObjectStyles = {
  textBody: buildText({ color: layoutSettings.colors.BLACK, fontSize: 20, extend: {
    textAlign: 'justify',
    paddingVertical: 0
  }}),
  creditItemThumbnail: {
    marginRight: 10
  },
  body: {
    padding: 10
  },
  title: buildText({ bold: true, fontSize: 28, color: layoutSettings.colors.TERTIARY, extend: { padding: 10, textAlign: 'center' }}),
  sectionTitle: buildText({ bold: true, fontSize: 22, color: layoutSettings.colors.TERTIARY, extend: { padding: 10 }}),
  viewMoreCommentsButton: {
    marginBottom: 20,
    marginTop: 20
  },
  container: {
    backgroundColor: layoutSettings.colors.PRIMARY,
  },
  postContainer: {
    backgroundColor: layoutSettings.colors.PRIMARY
  },
  scrollToTopContainer: {
    zIndex: 99,
    borderWidth: 0,
    borderTopLeftRadius: layoutSettings.defaultRadius,
    borderBottomLeftRadius: layoutSettings.defaultRadius,
    position: 'absolute',
    bottom: 50,
    right: 0,
    backgroundColor: layoutSettings.colors.TERTIARY,
    paddingVertical: 5,
    paddingHorizontal: 10,
    shadowColor: layoutSettings.colors.SHADOW_COLOR,
    shadowOffset: { width: -1, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
  },
  scrollToTopIcon: {
    color: layoutSettings.colors.LIGHT,
    fontSize: 20
  },
  divisor: {
    height: 1,
    borderRadius: 0.5,
    borderWidth: 0,
    backgroundColor: layoutSettings.colors.TERTIARY,
    marginHorizontal: 10
  },
  creditsMusicText: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 20, extend: {
    paddingHorizontal: 10
  }}),
  creditItem: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    marginBottom: 10
  },
  creditNameText: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 16 }),
  creditLinks: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  creditIconContainer: {
    paddingRight: 10
  },
  creditIcon: {
    fontSize: 32,
    color: layoutSettings.colors.TERTIARY
  }
}

multiContentObjectStyles.divisorWithMargin = {
  ...multiContentObjectStyles.divisor,
  marginTop: 30
}

const multiContentStyles = StyleSheet.create(multiContentObjectStyles)

export {
  multiContentStyles,
  multiContentObjectStyles
}
