import { StyleSheet, Dimensions } from 'react-native'
import layoutSettings from '../../../common/settings/layoutSettings'
import { buildText, buildTitle } from '../typografy'

const defaultTextTerms = buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 14 })

const subscribeObjectStyles = {
  wellcomeImage: {
    flex: 1,
    width: null,
    height: null
  },
  wellcomeTextContainer: {
    backgroundColor: layoutSettings.colors.SECONDARY_TRANSPARENT,
    padding: 10,
    marginBottom: 10
  },
  wellcomeStarName: buildText({ bold: true, color: layoutSettings.colors.TERTIARY, fontSize: 36, extend: { textAlign: 'center' } }),
  wellcomeBeSubscriber: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 26, extend: { textAlign: 'center' } }),
  wellcomeDescription: buildText({ color: layoutSettings.colors.TERTIARY, fontSize: 18, extend: { textAlign: 'center' } }),
  wellcomeContainer: {
    position: 'absolute',
    backgroundColor: layoutSettings.colors.SECONDARY_TRANSPARENT,
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  subscriptionContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 40
  },
  terms: {
    position: 'absolute',
    flexDirection: 'row',
    right: 0,
    left: 0,
    bottom: 0,
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
    padding: 10
  },
  serviceTerms: {
    ...defaultTextTerms
  },
  privacyPolicy: {
    ...defaultTextTerms
  }
}

const subscribeStyles = StyleSheet.create(subscribeObjectStyles)

export {
  subscribeStyles,
  subscribeObjectStyles
}
