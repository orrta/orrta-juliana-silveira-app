import React from 'react'

import Navigation from '../components/navigation/Navigation.js'
import SplashScreen from 'react-native-splash-screen'

export default class App extends React.Component {

  componentDidMount() {
    SplashScreen.hide()
  }

  render() {
    return <Navigation />
  }
}
