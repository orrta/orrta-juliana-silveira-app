import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reduxThunk from 'redux-thunk'
import { View } from 'react-native'
import reducers from './reducers'
import { safeAreaViewStyles } from './assets/styles/common'
import OrrtaModal from './components/modal/Modal'

import App from './containers/App.js'

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore)

const store = createStore(reducers)

class AppResult extends React.Component {

  render() {
    return (
      <Provider store={ createStoreWithMiddleware(reducers) }>
        <View style={{ flex: 1 }}>
          <App />
          <OrrtaModal />
        </View>
      </Provider>
    )
  }
}

export default AppResult
