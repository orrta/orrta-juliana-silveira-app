help:
	@echo 'Makefile for orrta_app                                      '
	@echo '                                                            '
	@echo 'Usage:                                                      '
	@echo '   setup        Install all dependencies to dev             '
	@echo '   run          Run project using local enviroment          '
	@echo '   seed         Setup elasticsearch documents to use the app'

dejavu:
	docker run -p 1358:1358 -d appbaseio/dejavu
